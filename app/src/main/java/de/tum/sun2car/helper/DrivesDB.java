package de.tum.sun2car.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import de.tum.sun2car.classes.Drives;

public class DrivesDB
{
	private static final int VERSION_DrivesDB = 2;
	private static final String NOM_DrivesDB = "sun2car.db";
 
	private static final String TABLE_DRIVES = "table_drives";

	public static final String COL_DRIVES_TRACKID = "TRACK_ID";
	private static final int NUM_COL_TRACKID = 0;
	public static final String COL_DRIVES_USERID = "USER_ID";
	private static final int NUM_COL_USERID = 1;
	public static final String COL_DRIVES_START_LOC = "START_LOC";
	private static final int NUM_COL_START_LOC = 2;
	public static final String COL_DRIVES_STOP_LOC = "STOP_LOC";
	private static final int NUM_COL_STOP_LOC = 3;
	public static final String COL_DRIVES_START_TIME = "START_TIME";
	private static final int NUM_COL_START_TIME = 4;
	public static final String COL_DRIVES_STOP_TIME = "STOP_TIME";
	private static final int NUM_COL_STOP_TIME = 5;
	public static final String COL_DRIVES_LENGTH = "LENGTH";
	private static final int NUM_COL_LENGTH = 6;
	public static final String COL_DRIVES_MOT = "MOT";
	private static final int NUM_COL_MOT = 7;
	public static final String COL_DRIVES_OCCUPATION = "OCCUPATION";
	private static final int NUM_COL_OCC = 8;
	public static final String COL_DRIVES_EMISSION = "EMISSION";
	private static final int NUM_COL_EMISSION = 9;

	private String[] allColumns = {SqliteDB.COL_DRIVES_TRACKID, 
									SqliteDB.COL_DRIVES_USERID,
									SqliteDB.COL_DRIVES_START_LOC,
									SqliteDB.COL_DRIVES_STOP_LOC, 
									SqliteDB.COL_DRIVES_START_TIME,
									SqliteDB.COL_DRIVES_STOP_TIME,
									SqliteDB.COL_DRIVES_LENGTH,
									SqliteDB.COL_DRIVES_MOT,
									SqliteDB.COL_DRIVES_OCCUPATION,
									SqliteDB.COL_DRIVES_EMISSION};
	
	private SQLiteDatabase drivesdb;
 
	private SqliteDB myBaseSQLite;
 
	public DrivesDB(Context context){
		//Erzeugung der DB inkl. Tabellen
		myBaseSQLite = new SqliteDB(context, NOM_DrivesDB, null, VERSION_DrivesDB);
	}
 
	public void open(){
		//DB im Schreib-Modus öffnen
		drivesdb = myBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//Datenbank schliessen
		drivesdb.close();
	}
 
	public SQLiteDatabase getDrivesDB(){
		return drivesdb;
	} 
	
	public long addDrive(Drives d)
	{		
		//Erzeugung einer ContentValues 
		ContentValues values = new ContentValues();
		
		//Einfügen eines Element als key-value-Paar (Spalte-Wert-Paar)
		values.put(COL_DRIVES_TRACKID, d.getTrackID());
		values.put(COL_DRIVES_USERID, d.getUserID());
		values.put(COL_DRIVES_START_LOC, d.getStart_location());
		values.put(COL_DRIVES_STOP_LOC, d.getStop_location());
		values.put(COL_DRIVES_START_TIME, d.getStart_time());
		values.put(COL_DRIVES_STOP_TIME, d.getStop_time());
		values.put(COL_DRIVES_LENGTH, d.getTrip_length());
		values.put(COL_DRIVES_MOT, d.getTrip_mot());
		values.put(COL_DRIVES_OCCUPATION, d.getOccupation());
		values.put(COL_DRIVES_EMISSION, d.getCo2gTotal());
//		values.put(COL_COUNT, s.getCount());

		//Ein Objekt wird in bdd via ContentValues eingefügt
		return drivesdb.insert(TABLE_DRIVES, null, values);
	} 
	
//	public int setDrivesCount(int id, Drives d)
//	{
//		ContentValues values = new ContentValues();
//		values.put(COL_COUNT, s.getCount());
//		return drivesdb.update(TABLE_DRIVES, values, COL_ID + " = " + id, null);
//	}
	
	public int updateDriveCO2(double trackid, Double co2Trip)
	{		
		ContentValues values = new ContentValues();
		values.put(COL_DRIVES_EMISSION, co2Trip);
		
		return drivesdb.update(TABLE_DRIVES, values, COL_DRIVES_TRACKID + " =\"" + trackid + "\"", null);
	}
	
	public int updateDriveOccupation(Drives d, int occupation)
	{
		double trackid = d.getTrackID();
		
		ContentValues values = new ContentValues();
		values.put(COL_DRIVES_OCCUPATION, occupation);
		
		return drivesdb.update(TABLE_DRIVES, values, COL_DRIVES_TRACKID + " =\"" + trackid + "\"", null);
	}


  public double getTrackLength(double trackID) {
    Double length = 0.0;
    Cursor c = drivesdb.rawQuery("SELECT " + COL_DRIVES_LENGTH + " FROM " + TABLE_DRIVES + " WHERE " + COL_DRIVES_TRACKID + " = " + trackID, null);
    if(c.moveToFirst()){
      do{
        length = c.getDouble(0);
      }while(c.moveToNext());
    }
    c.close();
    return length;
  }

	public int updateDriveDistance(Drives d, List<Double> distance)
	{
		double trackid = d.getTrackID();
		double dist = distance.get(distance.size()-1);
		
		ContentValues values = new ContentValues();
		values.put(COL_DRIVES_LENGTH, dist);
		
		return drivesdb.update(TABLE_DRIVES, values, COL_DRIVES_TRACKID + " =\"" + trackid + "\"", null);
	}
	
	public int setCorrectedMOT(String newMOT, Double trackID)
	{
		ContentValues values = new ContentValues();
		values.put(COL_DRIVES_MOT, newMOT);
		
		return drivesdb.update(TABLE_DRIVES, values, COL_DRIVES_TRACKID + " =\"" + trackID + "\"", null);
	}
	
 	public int removeDriveWithTrackID(Context con, Double trackid)
 	{
 		LocationDB locsSource = new LocationDB(con);
		locsSource.open();
		locsSource.removeDriveWithTrackID(trackid);
		
		return drivesdb.delete(TABLE_DRIVES, COL_DRIVES_TRACKID + " = \"" + trackid + "\"", null);
	}
 	
 	public boolean checkForDrive(Double userid, Double trackid)
 	{		
 		Cursor c = drivesdb.query(TABLE_DRIVES, allColumns, COL_DRIVES_USERID + "=\"" + userid + "\" AND " + COL_DRIVES_TRACKID + "=\"" + trackid + "\"", null, null, null, null);
 		
 		boolean result = c.moveToFirst();
 		c.close();
		
 		return result;
		// true if already there
		// false if not there
 		
 		
// 		if ( !getAllDrivesByUser(userid).isEmpty() )
//		{
//			for (int i = 0; i < getAllDrivesByUser(userid).size(); i++)
//			{
//				if ( getAllDrivesByUser(userid).get(i).getTrackID() == trackid )
//				{
//					r = true;
//				}
//			}
//		}
	}
 	
 	public List<Drives> getSimilarTimeDrives(Drives drive)
 	{
		List<Drives> drives = new ArrayList<Drives>();
		
		Cursor c = drivesdb.query(TABLE_DRIVES, allColumns, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			Drives d = cursorToDrives(c);
			if ( Math.abs( d.getStartDate().getTime() - drive.getStartDate().getTime() ) < 5 * 60 * 1000)
			{
				drives.add(d);
			}
			c.moveToNext();
		}

 		c.close();


		return drives;
 	}
 	
 	
	public List<Drives> getAllDrivesByUser(String userid)
	{
		List<Drives> drives = new ArrayList<Drives>();
		
		Cursor c = drivesdb.query(TABLE_DRIVES, allColumns, COL_DRIVES_USERID + "=\"" + userid + "\"", null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			Drives d = cursorToDrives(c);
			drives.add(d);
			c.moveToNext();
		} 		
		
		c.close();
		
		
		return drives;
	} 
	
	private Drives cursorToDrives(Cursor c)
	{		
		if (c.getCount() == 0)
			return null; 
		
		Drives d = new Drives(c.getDouble(NUM_COL_USERID), c.getDouble(NUM_COL_TRACKID), c.getString(NUM_COL_START_LOC), c.getString(NUM_COL_STOP_LOC), c.getString(NUM_COL_START_TIME), 
				c.getString(NUM_COL_STOP_TIME), c.getDouble(NUM_COL_LENGTH), c.getString(NUM_COL_MOT), c.getDouble(NUM_COL_EMISSION));
 
		return d;
	}

}
