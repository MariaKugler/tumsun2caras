package de.tum.sun2car.info;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class InfoAccount extends Activity {

	TextView tv_appversion;
	
	TextView tv_surname;
	TextView tv_username;
	TextView tv_familyID;
	TextView tv_driver;
	
	TextView tv_pv_kWp;
	TextView tv_pv_area;
	TextView tv_pv_orientation;
	TextView tv_pv_angle;
	TextView tv_pv_kWh;
	TextView tv_pv_kWhkWp;
	
	Button btn_UpdateApp;
	Drawable drw_btn_updateApp;
	Context con;

	ServerHelper serverHelper;
	
	private SharedPreferences settings;
	private SharedPreferences prefs;

    private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_info_account);
		
		con = this;
		
		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		
		tv_appversion = (TextView) findViewById(R.id.info_account_appversion);

		tv_surname = (TextView) findViewById(R.id.info_account_surname);
		tv_username = (TextView) findViewById(R.id.info_account_user);
		tv_familyID = (TextView) findViewById(R.id.info_account_familyid);
		tv_driver = (TextView) findViewById(R.id.info_account_driver);
		
		tv_pv_kWp = (TextView) findViewById(R.id.info_account_pv_kwp);
		tv_pv_area = (TextView) findViewById(R.id.info_account_pv_area);
		tv_pv_orientation = (TextView) findViewById(R.id.info_account_pv_orientation);
		tv_pv_angle = (TextView) findViewById(R.id.info_account_pv_angle);
		tv_pv_kWh = (TextView) findViewById(R.id.info_account_pv_kwh);
		tv_pv_kWhkWp = (TextView) findViewById(R.id.info_account_pv_kwhkw);
		
		btn_UpdateApp = (Button) findViewById(R.id.btn_info_account_update);

		tv_surname.setText(prefs.getString("FamilySurname", ""));
		tv_username.setText(settings.getString(Preferences.cUsername, ""));
		tv_familyID.setText(prefs.getString("FamilyID", ""));
		
		if ( prefs.getInt("Driver", 0) == 0 )
			tv_driver.setText("nicht vorhanden");
		else
			tv_driver.setText("vorhanden");
		

		tv_pv_kWp.setText(prefs.getString("PV_kWp", "") + " kW");
		tv_pv_area.setText(prefs.getString("PV_area", "") + " m^2");
		tv_pv_orientation.setText(prefs.getString("PV_orientation", ""));
		tv_pv_angle.setText(prefs.getString("PV_angle", "") + " °");
		tv_pv_kWh.setText(prefs.getString("PV_kWh", "") + " kWh");
		tv_pv_kWhkWp.setText(prefs.getString("PV_kWhkWp", "") + " (kWh/kWp)");
		
    	
    	if( isOnline() )
		{
    		dialog = new ProgressDialog(this);
        	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        	dialog.setTitle("Daten laden...");
        	dialog.setMessage("Daten werden geladen.");
        	
	    	dialog.show();
	    	
				
			new Thread(new Runnable() {
				public void run()
				{
					serverHelper = ServerHelper.getInstance(con);
						serverHelper = new ServerHelper("http://129.187.64.247/", settings.getString(Preferences.cUsername,""), null);
						
						PackageInfo pInfo;
						String version = null;
						try {
							pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
							version = pInfo.versionName;
							
						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}
		
						tv_appversion.setText(version);
						
						ConnectivityManager cm = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
						NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
					    
						if (activeNetwork != null && activeNetwork.isConnected())
						{
							String newVersion = null;
							try {
								newVersion = new checkForUpdateTask().execute(serverHelper).get();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ExecutionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		
					    	dialog.dismiss();
							
					    	
		//			    	DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		////		    		symbols.setDecimalSeparator('.');
		//		    		DecimalFormat df = new DecimalFormat("#" + symbols.getDecimalSeparator() + "############", symbols);      // 12 Stellen
		//		    		Double d_current = Double.valueOf(df.format(version));
		//		    		Double d_new = Double.valueOf(df.format(newVersion));
				    		
							Double d_current = Double.parseDouble(version);
							Double d_new = Double.parseDouble(newVersion);
							if ( d_new > d_current )	// !newVersion.equals(version)
							{
								Handler handler = new Handler(Looper.getMainLooper());
						    	handler.post(new Runnable()
						    	{
						    		@Override
									public void run()
									{
						    			btn_UpdateApp.setBackground(null);
										drw_btn_updateApp = con.getResources().getDrawable(R.drawable.btn_update_app);
										btn_UpdateApp.setBackground(drw_btn_updateApp);
									}
						    	});
							}
					    }
					
						
							
					}}).start();
		}
		else
		{
			Toast.makeText(con, "Keine Verbindung zum Server", Toast.LENGTH_LONG).show();
		}
		
		
		
	}
	
	public void onUpdateApp(View v)
	{
		new Thread(new Runnable() {
			public void run() {
				
//				android.os.Debug.waitForDebugger();
				
	    		serverHelper.getNewAPK(con);
	    		
	    		Intent promptInstall = new Intent(Intent.ACTION_VIEW);
	    		File apk = new File(con.getResources().getString(R.string.downloadPath), "TUMsun2car.apk");
	    		promptInstall.setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive");
		        startActivity(promptInstall); 
		        
				
			}
		}).start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.account, menu);
		return true;
	}
	
	public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

	    if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}

}

class checkForUpdateTask extends AsyncTask<ServerHelper, Void, String>
{
    private Exception exception;
	boolean success;
	ServerHelper serverHelper;

    protected String doInBackground(ServerHelper... param)
    {
    	serverHelper = param[0];
		
    	String newVersion = serverHelper.getAPKVersion();
		
    	return newVersion;
    }
 }
