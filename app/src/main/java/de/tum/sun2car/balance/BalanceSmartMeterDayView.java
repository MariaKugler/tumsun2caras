package de.tum.sun2car.balance;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.SmartmeterValue;
import de.tum.sun2car.helper.SmartHomeDB;
import de.tum.sun2car.helper.SmartLoadingDB;
import de.tum.sun2car.helper.SmartPVDB;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class BalanceSmartMeterDayView extends Activity 
{
	Bundle b;
	int day;
	int month;
	int year;

	String user_token;
	String userName;
	String password;
	
	private SharedPreferences settings;
	private SharedPreferences prefs;

	private GraphicalView graph_smartmetering;
	private GraphicalView graph_smartmetering_delta;
	LinearLayout ll_smartmetering;
	LinearLayout ll_smartmetering_delta;
//	TextView tv_max;
//	TextView tv_mean;

	Context con;
	
    private ProgressDialog dialog;
    
    
    

	String [] smvFiles = {"Home", "Loading", "PV"};
	private ArrayList<ArrayList<SmartmeterValue>> smvData = new ArrayList<ArrayList<SmartmeterValue>>();

	private ArrayList<SmartmeterValue> smvDataHome = new ArrayList<SmartmeterValue>();
	private ArrayList<SmartmeterValue> smvDataLoading = new ArrayList<SmartmeterValue>();
	private ArrayList<SmartmeterValue> smvDataPV = new ArrayList<SmartmeterValue>();

	private SmartHomeDB smarthomeSource;
	private SmartLoadingDB smartloadingSource;
	private SmartPVDB smartpvSource;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance_smartmeter_day_view);

		ll_smartmetering = (LinearLayout) findViewById(R.id.chart_smartmetering);
		ll_smartmetering_delta = (LinearLayout) findViewById(R.id.chart_smartmetering_delta);
//		tv_max = (TextView) findViewById(R.id.balance_sm_day_max);
//		tv_mean = (TextView) findViewById(R.id.balance_sm_day_mean);
		
		Bundle b = new Bundle();
		b = getIntent().getExtras();
		
		day = b.getInt("day");
		month = b.getInt("month");
		year = b.getInt("year");
		
		this.setTitle("Energie-Bilanz am " + day + "." + (month+1) + "." + year);

		con = this;

		smarthomeSource = new SmartHomeDB(this);
		smarthomeSource.open();
		
		smartloadingSource = new SmartLoadingDB(this);
		smartloadingSource.open();
		
		smartpvSource = new SmartPVDB(this);
		smartpvSource.open();

		smvData.add(smvDataHome);
		smvData.add(smvDataLoading);
		smvData.add(smvDataPV);

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Tagesdaten laden...");
    	dialog.setMessage("Daten werden geladen.");
//    	dialog.setCancelable(false);
    	dialog.setCanceledOnTouchOutside(false);
	}
	
	
	public void onResume()
	{
    	super.onResume();
    	
    	dialog.show();
    		
    	new Thread(new Runnable() {
			public void run()
			{								
				Calendar midnight_start = new GregorianCalendar();
	    		midnight_start.set(year, month, day, 0, 0, 0);
	    		


				//3x Durchlauf für 3 CSV
		    	for ( int i = 0; i < smvFiles.length; i++)
		    	{
		    		getSmartHomeDayData(i, midnight_start);
		    	}
	    		
	    		
	    		dialog.dismiss();	
				
				
				Handler handler = new Handler(Looper.getMainLooper());
		    	handler.post(new Runnable()
		    	{

					@Override
					public void run()
					{
						if ( smvData.get(0).isEmpty() &&  smvData.get(1).isEmpty() && smvData.get(2).isEmpty() )
						{
							Toast.makeText(con, "Keine Daten für diesen Tag verfügbar", Toast.LENGTH_LONG).show();
						}
		    			else
		    			{
		    				if (graph_smartmetering == null)
							{
								graph_smartmetering = (GraphicalView) ChartFactory.getTimeChartView(con, getData(0), getRenderer(0), "HH:mm");
								ll_smartmetering.addView(graph_smartmetering);
							}
							else
							{
								graph_smartmetering.repaint(); // use this whenever data has changed and you want to redraw
							}
							
							if (graph_smartmetering_delta == null)
							{
								graph_smartmetering_delta = (GraphicalView) ChartFactory.getTimeChartView(con, getData(1), getRenderer(1), "HH:mm");
								ll_smartmetering_delta.addView(graph_smartmetering_delta);
							}
							else
							{
								graph_smartmetering_delta.repaint(); // use this whenever data has changed and you want to redraw
							}
		    			}
						
						
						
						
//						tv_max.setText( String.format("%.3f km", (sumDistance/1000)) );
//						tv_mean.setText(String.format("%.3f kg", (sumCO2g/1000)) );
						
					} 
		    	});
			
			}}).start();
	
	}
	

    
    @Override
    public void onBackPressed()
    {
    	Intent intent = new Intent(con, Balance.class);

    	ArrayList<Integer> date = new ArrayList<Integer>();
		date.add(year);
		date.add(month);
		date.add(day);
        
		Bundle b = new Bundle();
		b.putBoolean("isSavedDate", true);

		intent.putExtra("savedCal", b);
		intent.putIntegerArrayListExtra("savedDate", date);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		
		startActivity(intent);
    }
	
	private void getSmartHomeDayData(int which, Calendar day)
	{
		String csvName = smvFiles[which];
		
		float sumEnergyHour = 0;
		float sumEnergyDay = 0;
		int prevMinute = 0;
		int prevDay = 0;
	    
	    ArrayList<SmartmeterValue> smv = new ArrayList<SmartmeterValue>();
	    
	    Calendar end = new GregorianCalendar();
		end.setTime(day.getTime());
		end.roll(Calendar.DATE, true);
		end.roll(Calendar.SECOND, true);
	    
	    switch (which)
		{
		case 0:
			smv = smarthomeSource.getIntervalValues(day, end);
			break;
		case 1:
			smv = smartloadingSource.getIntervalValues(day, end);
			break;
		case 2:
			smv = smartpvSource.getIntervalValues(day, end);
			break;
		default:
			break;
		}
		smvData.set(which, smv);
	}
	
	
	public XYMultipleSeriesDataset getData(int which)
	{

        // Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset_smartmetering = new XYMultipleSeriesDataset();

		
		switch (which)
		{
			//alle einzeln
	        case 0:
	        	TimeSeries ts_home = new TimeSeries("Haushalt");
	            for ( SmartmeterValue smv : smvData.get(0) )
	            {
	            	ts_home.add(smv.getDate(), smv.getEnergy());
	            }

	            TimeSeries ts_pv = new TimeSeries("PV");
	            for ( SmartmeterValue smv : smvData.get(2) )
	            {
	            	ts_pv.add(smv.getDate(), smv.getEnergy());
	            }
	            
	    		dataset_smartmetering.addSeries(ts_home);
	    		dataset_smartmetering.addSeries(ts_pv);
	    		
	        	break;
	        	
	        // summativ Haushalt/PV <--> Laden
	        case 1:
	        	TimeSeries ts_homepv = new TimeSeries("Delta Haushalt - PV");
	        	
	        	// look if both home and pv start with same time
	        	Date sm_home_start = smvData.get(0).get(0).getDate();
	        	Date sm_pv_start = smvData.get(2).get(0).getDate();

	        	int moreValues = smvData.get(0).size() >= smvData.get(2).size() ? 0 : 2;
	        	int lessValues = moreValues == 0 ? 2 : 0;
	        	
	        	for ( int i = 0; i < smvData.get(moreValues).size(); i++  )
	            {
        			SmartmeterValue smv1 = smvData.get(moreValues).get(i);
        			
        			SmartmeterValue smv2 = getSMVwithDate(smv1.getDate(), smvData.get(lessValues));     			
        			
        			double energyToAdd = moreValues == 0 ? ( smv1.getEnergy() - smv2.getEnergy() ) : ( smv2.getEnergy() - smv1.getEnergy() );
	            	ts_homepv.add(smv1.getDate(), energyToAdd);
	            }
	            

	    		dataset_smartmetering.addSeries(ts_homepv);
	        	
	        	
	        	break;
	        	
			default:
				break;
		}


        TimeSeries ts_loading = new TimeSeries("Laden");
        if ( !smvData.get(1).isEmpty() )
        {
            for ( SmartmeterValue smv : smvData.get(1) )
            {
            	ts_loading.add(smv.getDate(), smv.getEnergy());
            }
        }
        else
        {
        	Calendar start = new GregorianCalendar();
    		start.set(year, month, day, 0, 0, 0);
    		
    		Calendar end = new GregorianCalendar();
    		end.setTime(start.getTime());
    		end.roll(Calendar.DATE, true);
    		
        	ts_loading.add(start.getTime(), 0.0);
        	ts_loading.add(end.getTime(), 0.0);
        }

		dataset_smartmetering.addSeries(ts_loading);
		
		
		
	    return dataset_smartmetering;
	}
	
	public XYMultipleSeriesRenderer getRenderer(int which)
	{ 
		// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
 
        multiRenderer.setXTitle("Uhrzeit");
        multiRenderer.setYTitle("Energiebilanz in kW");

        
        switch (which)
		{
			//alle einzeln
	        case 0:

	    		// Creating XYSeriesRenderer to customize visitsSeries
	            XYSeriesRenderer houseRenderer = new XYSeriesRenderer();
	            houseRenderer.setColor(Color.BLACK);
	            houseRenderer.setPointStyle(PointStyle.POINT);
	            houseRenderer.setFillPoints(true);
	            houseRenderer.setLineWidth(4);
	            

	            XYSeriesRenderer pvRenderer = new XYSeriesRenderer();
	            pvRenderer.setColor(Color.parseColor("#99cc00"));
	            pvRenderer.setPointStyle(PointStyle.POINT);
	            pvRenderer.setFillPoints(true);
	            pvRenderer.setLineWidth(4);

	            multiRenderer.addSeriesRenderer(houseRenderer);
	            multiRenderer.addSeriesRenderer(pvRenderer);

	        	break;
	        	
	        // summativ Haushalt/PV <--> Laden
	        case 1:

	    		// Creating XYSeriesRenderer to customize visitsSeries
	            XYSeriesRenderer housepvRenderer = new XYSeriesRenderer();
	            housepvRenderer.setColor(Color.parseColor("#b9102d"));
	            housepvRenderer.setPointStyle(PointStyle.POINT);
	            housepvRenderer.setFillPoints(true);
	            housepvRenderer.setLineWidth(4);

	            multiRenderer.addSeriesRenderer(housepvRenderer);
	        	
	        	break;
	        	
			default:
				break;
		}

        XYSeriesRenderer loadingRenderer = new XYSeriesRenderer();
        loadingRenderer.setColor(Color.parseColor("#006ab3"));
        loadingRenderer.setPointStyle(PointStyle.POINT);
        loadingRenderer.setFillPoints(true);
        loadingRenderer.setLineWidth(4);
        
        multiRenderer.addSeriesRenderer(loadingRenderer);
		
        
        
		// set some properties on the main renderer
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.parseColor("#edeff1"));
		multiRenderer.setAxisTitleTextSize(25);
//		renderer_emissions.setChartTitleTextSize(20);
		multiRenderer.setLabelsTextSize(25);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setYLabelsPadding(5);
		multiRenderer.setShowLegend(false);
		multiRenderer.setLegendTextSize(25);
		multiRenderer.setFitLegend(true);
		multiRenderer.setMargins(new int[] { 0, 100, 50, 10 });  // top, left, bottom, right
//		renderer_emissions.setZoomButtonsVisible(true);
//		multiRenderer.setYAxisMax(maxY);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setPointSize(5);
		multiRenderer.setShowLabels(true);
		multiRenderer.setPanEnabled(false,false);
		multiRenderer.setZoomEnabled(false, false);
		
    	
        return multiRenderer;
	}
	
	
	
	public SmartmeterValue getSMVwithDate(Date d, ArrayList<SmartmeterValue> smv_list)
	{
		SmartmeterValue smv = new SmartmeterValue();
		
		for (SmartmeterValue v : smv_list)
		{
	        if ( v.getDate().equals(d) )
	           smv = v;
	    }
		
		if ( smv.getDate() == null )
		{
			smv.setDate(d);
			smv.setEnergy(0.0);
		}
		return smv;
	}


	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance_tracks_day_view, menu);
		return true;
	}

}
