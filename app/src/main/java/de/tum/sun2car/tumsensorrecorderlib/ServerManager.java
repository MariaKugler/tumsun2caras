package de.tum.sun2car.tumsensorrecorderlib;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import de.tum.sun2car.helper.ServerCommunicationException;


public final class ServerManager {

  // TODO
  // Dringend: "Server" in diesem Fall XAMPP muss / darf nur noch HTTPS-Anfragen entgegennehmen
	private final String cServerUrl = "http://129.187.64.247/";
	private Context mContext;
	private HttpClient mHttpclient;
	private TokenState mTokenState;
	private String mTokenString;
	private static String mUserName = "";
	private static String mUserPassword = "";
	private static ServerManager mServerManager;
	
	public enum TokenState {
		NoValidToken,
		ValidToken		
	}

	public enum UploadState {
		Success,
		TokenExpired,
		ServerError,
		OtherError
	}
	
	public enum LoginState {
		Success,
		NoConnectivity,
		WrongLoginOrPassword,
		UnknownError,
		ServerError
	}
	
	private ServerManager(Context con) {
		mContext = con;
		mHttpclient = new DefaultHttpClient();
		mTokenState = TokenState.NoValidToken;
		mTokenString=null;
		
		SharedPreferences settings = con.getSharedPreferences(Preferences.PREF_FILE_NAME, Context.MODE_PRIVATE);
		mUserName = settings.getString(Preferences.cUsername,null);
		mUserPassword = settings.getString(Preferences.cPass,null);
	}

	public synchronized static ServerManager getInstance(Context con) {
		if(mServerManager == null) {
			mServerManager = new ServerManager(con);
			
			Log.v("server-manager","Generated new Server-Instance");
		}
		
		return mServerManager;
	}
	
	public void setLoginData(String userName, String pw) {
			mUserName = userName;
			mUserPassword = pw;
			mTokenState = TokenState.NoValidToken;
	}
	
	public String getUserName() {
		return mUserName;
	}

	public synchronized UploadState uploadFile(String filename) {
		
		try {
			
			File file = new File(filename);	
			
			FileBody fileBody = new FileBody(file, "text/plain");
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			HttpPost httppost = new HttpPost(cServerUrl);
			reqEntity.addPart("sensordata", fileBody);
			reqEntity.addPart("action", new StringBody("uploadSensorData"));
			reqEntity.addPart("token", new StringBody(mTokenString));
			httppost.setEntity(reqEntity);
			
			HttpResponse response = mHttpclient.execute(httppost);
			
			if(response.getStatusLine().getStatusCode()==200) {
				String resp = EntityUtils.toString(response.getEntity());
			
				if(getStatusFromResponse(resp)) {
					return UploadState.Success;
				} else {
					Log.i("upload-error",resp);
					
					String status = getTagContentFromResponse(resp, "status");
					if(status != null && status.equals("session_expired")) {
						mTokenState = TokenState.NoValidToken;
						return UploadState.TokenExpired;
					} else {
						return UploadState.OtherError;
					}
				}
			} else {
				return UploadState.ServerError;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.err.println("Error: " + e.getMessage());
        	Log.i("SensorFileUploader", e.getMessage());
        	return UploadState.OtherError;
		}

	}

	
	
	/***
	 * holt einen token und schreibt ihn in mTokenState
	 * setzt außerdem noch mTokenState je nachdem ob der request erfolgreich war
	 * @return true if request was successful
	 */

	public synchronized LoginState createNewToken(){
		
		try {
			
			if(!isNetworkAvailable()) return LoginState.NoConnectivity;

      JSONObject json_data_token = null;
      try {
        ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
        tokenPair.add(new BasicNameValuePair("action", "token"));
        tokenPair.add(new BasicNameValuePair("username", mUserName));
        tokenPair.add(new BasicNameValuePair("password", mUserPassword));
        json_data_token = getJSONfromServer("php_sun2car/api.php", tokenPair);

        if (json_data_token.has("token")) {
          mTokenString = json_data_token.getString("token");
          if(mTokenString != null) {
            mTokenState = TokenState.ValidToken;
            return LoginState.Success;
          } else {
            Log.i("parse-error", mTokenString);
            return LoginState.UnknownError;
          }
				} else {  // no success
					Log.i("invalid-request-error: wrong password or username", mTokenString);
					return LoginState.WrongLoginOrPassword;
				}
      }
      catch(Exception e) {
        Log.i("Server-Error", mTokenString);
				return LoginState.ServerError;
      }
			
//			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//
//			mTokenState = TokenState.NoValidToken;
//
//			HttpPost httppost = new HttpPost(cServerUrl);
//			reqEntity.addPart("action", new StringBody("token"));
//			reqEntity.addPart("username", new StringBody(mUserName));
//			reqEntity.addPart("password", new StringBody(mUserPassword));
//
//			httppost.setEntity(reqEntity);
//
//			HttpResponse response;
//			try {
//				response = mHttpclient.execute(httppost);
//			} catch(Exception ex) {
//				Log.i("execute request:",ex.getMessage());
//
//				return LoginState.NoConnectivity;
//			}
//
//
//			if(response.getStatusLine().getStatusCode()==200) {
//				String resp = EntityUtils.toString(response.getEntity());
//
//				if(getStatusFromResponse(resp)) { // success
//					mTokenString = getTagContentFromResponse(resp, "token");
//					if(mTokenString != null) {
//						mTokenState = TokenState.ValidToken;
//						return LoginState.Success;
//					} else {
//						Log.i("parse-error",resp);
//						return LoginState.UnknownError;
//					}
//				} else {  // no success
//					Log.i("invalid-request-error",resp);
//					return LoginState.WrongLoginOrPassword;
//				}
//			} else {
//				Log.i("Server-Error: ",response.getStatusLine().toString());
//				return LoginState.ServerError;
//			}
    }

		catch(Exception e) {
			e.printStackTrace();
			System.err.println("Error: " + e.getMessage());
      Log.i("SensorFileUploader", e.getMessage());
      return LoginState.UnknownError;
		}
	}


  public synchronized boolean isNetworkAvailable() {
    ConnectivityManager connectivityManager
            = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null;
  }


  private boolean getStatusFromResponse(String response) throws SAXException, IOException, ParserConfigurationException {
		
		String status = getTagContentFromResponse(response,"status");

		if(status != null && status.equals("success")) {
			return true;
		} else {
			return false;
		}
	
	}
	
	private String getTagContentFromResponse(String response, String tagName) throws SAXException, IOException, ParserConfigurationException {
		
		Element node =  DocumentBuilderFactory
			    .newInstance()
			    .newDocumentBuilder()
			    .parse(new ByteArrayInputStream(response.getBytes()))
			    .getDocumentElement();
		
		NodeList statusNodes = node.getElementsByTagName(tagName);
		if(statusNodes.getLength() == 1) {
			return statusNodes.item(0).getFirstChild().getNodeValue().toString();
		}
		return null;
	}
	
	public synchronized boolean isTokenValid() {
			return (mTokenState == TokenState.ValidToken);
	}
	
	public synchronized String getToken() {
		return mTokenString;
	}

  public JSONObject getJSONfromServer(String page, List<NameValuePair> args) throws ServerCommunicationException {
    try {
      String json = getStringFromServer(page, args);

      int start = json.indexOf("{");
      int end = json.lastIndexOf("}");

      if (json.length() == 0) return null;

      if (start > 0) {
        json = json.substring(start, end + 1);
      }
      return new JSONObject(json);

    } catch (JSONException e) {
      throw new ServerCommunicationException(e.toString());
    }
  }

  public String getStringFromServer(String page, List<NameValuePair> args) throws ServerCommunicationException {
    try {
      InputStream is = this.getContentFromServer(page, args);
      BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);

      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
      reader.close();

      return sb.toString();
    } catch (ClientProtocolException e) {
      throw new ServerCommunicationException(e.toString());
    } catch (IOException e) {
      throw new ServerCommunicationException(e.toString());
    }
  }



    public InputStream getContentFromServer(String page, List<NameValuePair> options) throws ClientProtocolException, IOException {
        InputStream is = null;

        HttpClient httpclient = new DefaultHttpClient();
        String params = "?";
        for (int i = 0; i < options.size(); i++)
        {
            params = params + options.get(i).getName() + "=" + options.get(i).getValue();
            if (i < (options.size() - 1))
                params = params + "&";
        }

        HttpGet httpget = new HttpGet(this.cServerUrl + page + params);

        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        is = entity.getContent();
        return is;
    }



}
