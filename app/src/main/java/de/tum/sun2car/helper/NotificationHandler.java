package de.tum.sun2car.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import de.tum.sun2car.R;
import de.tum.sun2car.newdrive.NewDrive;

public class NotificationHandler {
	
	private static NotificationHandler mNotificationHandler;
	
	private Context context;
	private static NotificationManager mNotificationManager;
	private String cNotificationName = Context.NOTIFICATION_SERVICE;
	private static boolean NotificationStarted;
	private int ServiceCounter;
	
	
	private static Notification mNotification = null;
	
	private NotificationHandler(Context con) {
		context = con;
		NotificationStarted = false;
		ServiceCounter = 0;
		mNotificationManager = (NotificationManager) context.getSystemService(cNotificationName);
		
		
	}
	
	public static NotificationHandler getInstance(Context con) {
		
		if(mNotificationHandler == null) {
			mNotificationHandler = new NotificationHandler(con);
			
			Log.v("notification-handler","Generated new Notification-Instance");
		}
		
		return mNotificationHandler;
	}
	
	public synchronized void NotificationStart() {
		
		ServiceCounter++;
		if(NotificationStarted == false) {
			startNotification();
			NotificationStarted = true;
		}
	}
	
	public synchronized void NotificationStop() {
				
		if(NotificationStarted) {
			ServiceCounter--;
			if(ServiceCounter == 0) {
				stopNotification();
				NotificationStarted = false;
			}

		}
	}
	
	private void startNotification(){
		int icon = android.R.drawable.ic_input_get; // Systems icon
		CharSequence tickerText = "Start Sensor Recorder"; // displayed immediatelly
		mNotification = new Notification(icon, tickerText,0);
		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		CharSequence contentTitle = context.getString(R.string.app_name); // c.f. Notifications
		CharSequence contentText = "Service started"; // c.f. Notifications
		Intent notificationIntent = new Intent(context, NewDrive.class);
		
		// hint found at http://stackoverflow.com/questions/8254055/android-close-app-when-back-button-is-pressed
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		mNotification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

		
		mNotificationManager.notify(1, mNotification); // identifier = 1
	}
	
	private void stopNotification(){
		mNotificationManager.cancel(1);
	}

}
