package de.tum.sun2car.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Drives implements Comparable<Drives>
{
	private String start_location;
	private String stop_location;
	private String start_time;
	private String stop_time;
	private Double trip_length;
//	private String trip_duration;
	private String trip_mot;
	private Double trackID;
	private Double userID;
	private Date start_Date;
	private Date stop_Date;
	private Double co2gTotal;
	
	/**
	 * Occupation rate during trip
	 * default: 1
	 */
	private int occupation = 1;
	
	
	public Drives(Double userID, Double trackID, String start_loc, String stop_loc, String t_start, String t_stop, Double l_trip, String mot, Double co2Total)
	{
		setUserID(userID);
		setTrackID(trackID);
		setStart_location(start_loc);
		setStop_location(stop_loc);
		setStart_time(t_start);
		setStop_time(t_stop);
		setTrip_length(l_trip);
//		setTrip_duration(d_trip);
		setTrip_mot(mot);
		setCo2gTotal(co2Total);
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        try
        {
        	this.start_Date = simpleDateFormat.parse(start_time);
        	this.stop_Date = simpleDateFormat.parse(stop_time);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
	}


	public Drives(JSONObject json) throws JSONException
	{
		this.start_location = json.optString("startlocation");
		this.stop_location = json.optString("stoplocation");
		this.start_time = json.optString("starttime");
		this.stop_time = json.optString("stoptime");
		this.trip_length = json.optDouble("distance") * 1000;	// in meters
//		this.trip_duration = json.optString("surname");
		this.trip_mot = json.optString("type");
		this.trackID = json.optDouble("trackid");
		this.userID = json.optDouble("userid");
		
		//2013-08-06 17:49:46
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        try
        {
        	this.start_Date = simpleDateFormat.parse(start_time);
        	this.stop_Date = simpleDateFormat.parse(stop_time);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }

	}


	public String getStart_location() {
		return start_location;
	}


	public void setStart_location(String start_location) {
		this.start_location = start_location;
	}


	public String getStop_location() {
		return stop_location;
	}


	public void setStop_location(String stop_location) {
		this.stop_location = stop_location;
	}


	public String getStart_time() {
		return start_time;
	}


	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}


	public String getStop_time() {
		return stop_time;
	}


	public void setStop_time(String stop_time) {
		this.stop_time = stop_time;
	}


	public Double getTrip_length() {
		return trip_length;
	}


	public void setTrip_length(Double trip_length) {
		this.trip_length = trip_length;
	}


//	public String getTrip_duration() {
//		return trip_duration;
//	}
//
//
//	public void setTrip_duration(String trip_duration) {
//		this.trip_duration = trip_duration;
//	}


	public String getTrip_mot() {
		return trip_mot;
	}


	public void setTrip_mot(String mot) {
		this.trip_mot = mot;
	}


	public Double getTrackID() {
		return trackID;
	}


	public void setTrackID(Double trackID) {
		this.trackID = trackID;
	}
	

	public Double getUserID() {
		return userID;
	}


	public void setUserID(Double userID) {
		this.userID = userID;
	}
	

	public int getOccupation() {
		return occupation;
	}


	public void setOccupation(int occupation) {
		this.occupation = occupation;
	}
	
	public Date getStartDate() {
		return start_Date;
	}


	public void setStartDate(Date startDate) {
		this.start_Date = startDate;
	}
	
	
	public int compareTo(Drives another) {
		return another.start_Date.compareTo(start_Date);
	}


	public Date getStopDate() {
		return stop_Date;
	}


	public void setStopDate(Date stopDate) {
		this.stop_Date = stopDate;
	}


	public Double getCo2gTotal() {
		return co2gTotal;
	}


	public void setCo2gTotal(Double co2gTotal) {
		this.co2gTotal = co2gTotal;
	}







}
