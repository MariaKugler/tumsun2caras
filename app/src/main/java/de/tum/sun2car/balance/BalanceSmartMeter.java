package de.tum.sun2car.balance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.achartengine.GraphicalView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import de.tum.sun2car.MainPage;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.SmartmeterValue;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.helper.SmartHomeDB;
import de.tum.sun2car.helper.SmartLoadingDB;
import de.tum.sun2car.helper.SmartPVDB;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class BalanceSmartMeter extends FragmentActivity implements OnItemSelectedListener, OnDateSetListener
{
	Context con;

	private SharedPreferences prefs;
	private SharedPreferences settings;
	ServerHelper serverHelper;
	
	Spinner spinner;
	
	FrameLayout frame;
	FragmentManager fm;
	FragmentTransaction ft;
	
	BalanceChartFragment chartFragment;
	BalanceCalendarFragment calFragment;
	
	GraphicalView graph;
	
	private ProgressDialog dialog;

	String [] smvFiles = {"Home", "Loading", "PV"};
	
	private ArrayList<ArrayList<SmartmeterValue>> smvData = new ArrayList<ArrayList<SmartmeterValue>>();
	// 1) Home
	// 2) Loading
	// 3) PV
	
	private ArrayList<SmartmeterValue> smvDataHome = new ArrayList<SmartmeterValue>();
	private ArrayList<SmartmeterValue> smvDataLoading = new ArrayList<SmartmeterValue>();
	private ArrayList<SmartmeterValue> smvDataPV = new ArrayList<SmartmeterValue>();
	
	private SmartHomeDB smarthomeSource;
	private SmartLoadingDB smartloadingSource;
	private SmartPVDB smartpvSource;
	
	
	private GregorianCalendar timestampStartSel;
	private GregorianCalendar timestampEndSel;
	
	
	private static final String TAG_MEANFRAGMENT = "TAG_GraphMeanFragment";
	private static final String TAG_DAYFRAGMENT = "TAG_GraphDayFragment";
	private static final String TAG_CALFRAGMENT = "TAG_CalFragment";
	
	TextView tv_DateViewStart;
	TextView tv_DateViewEnd;
	Calendar calendarDateStart;
	Calendar calendarDateEnd;
	ImageButton btn_date_go;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance_pvhome);
		
//		tv_DateViewStart = (TextView) findViewById(R.id.balance_PVHome_Date_Start);
//		tv_DateViewEnd = (TextView) findViewById(R.id.balance_PVHome_Date_End);
//		calendarDateStart = Calendar.getInstance();
//		calendarDateEnd = Calendar.getInstance();
//		btn_date_go = (ImageButton) findViewById(R.id.btn_date_go);
		
		timestampStartSel = new GregorianCalendar(2014, 1, 1);
		timestampEndSel = new GregorianCalendar(2014, 1, 1);
		
		con = this;
		
		smarthomeSource = new SmartHomeDB(this);
		smarthomeSource.open();
		
		smartloadingSource = new SmartLoadingDB(this);
		smartloadingSource.open();
		
		smartpvSource = new SmartPVDB(this);
		smartpvSource.open();
		
		smvData.add(smvDataHome);
		smvData.add(smvDataLoading);
		smvData.add(smvDataPV);
		
		spinner = (Spinner) findViewById(R.id.balance_stat_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.balance_pv_home_values, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		

		

		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		
		fm = getSupportFragmentManager();
		ft = fm.beginTransaction();

    	
//    	if( isOnline() )
//		{
//    		dialog = new ProgressDialog(this);
//        	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        	dialog.setTitle("Daten laden...");
//        	dialog.setMessage("SmartMetering-Daten werden geladen.");
//        	dialog.setCanceledOnTouchOutside(false);
//        	
//
//	    	dialog.show();
//				
//			new Thread(new Runnable() {
//				public void run()
//				{
//					serverHelper = ServerHelper.getInstance(con);
//						serverHelper = new ServerHelper("http://129.187.64.247/", settings.getString(Preferences.cUsername,""), null);
//						
//						String version = null;
//						
//						File sdcard = Environment.getExternalStorageDirectory();
//						File file = new File(sdcard + "/sun2car/NewCSV/version.txt");	
//						
//						if (file.exists())
//						{
//							try {
//							    BufferedReader br = new BufferedReader(new FileReader(file));
//							    String line;
//
//							    while ((line = br.readLine()) != null)
//							    {
//							        version = line;
//							    }
//							}
//							catch (IOException e) {
//							    //You'll need to add proper error handling here
//							}
//						}
//						else
//						{
//							version = "0";
//						}
//						
//						ConnectivityManager cm = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
//						NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//					    
//						if (activeNetwork != null && activeNetwork.isConnected())
//						{
//							String newVersion = null;
//							try {
//								newVersion = new checkForCSVUpdateTask().execute(serverHelper).get();
//							} catch (InterruptedException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							} catch (ExecutionException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//		
//				    		
//
////							smarthomeSource.removeValues();
////							smartloadingSource.removeValues();
////							smartpvSource.removeValues();
//							
//							Double d_current = Double.parseDouble(version);
//							Double d_new = Double.parseDouble(newVersion);
//							if ( d_new > d_current )	// !newVersion.equals(version)
//							{
//						    	fetchData(newVersion);
//							}
//							else
//							{
////								//3x Durchlauf für 3 CSV
////						    	for ( int i = 0; i < smvFiles.length; i++)
////						    	{
////						    		getSmartHomeData(i);
////						    	}
//								
//						    	
//								dialog.dismiss();
//							}
//					    }
//					
//						
//							
//					}}).start();
//		}
//		else
//		{
//			Toast.makeText(con, "Keine Verbindung zum Server", Toast.LENGTH_LONG).show();
//		}
		
    	
    	
	}
	
//	protected void writeDownloadedVersion(String newVersion)
//	{
//		File sdcard = Environment.getExternalStorageDirectory();
//		File file = new File(sdcard + "/sun2car/NewCSV/version.txt");	
//		
//		try {
//			BufferedWriter buf_writer = new BufferedWriter(new FileWriter(file));
//
//			buf_writer.write(newVersion);
//		   
//			buf_writer.flush();
//			buf_writer.close();
//			toString();
//			
//		} 	
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//		
//	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance_pvhome, menu);
		return true;
	}
	
	@Override
    public void onBackPressed()
    {
    	Intent intent = new Intent(con, MainPage.class);

		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		startActivity(intent);
    }

	

	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
	{
		switch (pos)
		{
			//KalenderAnsicht
	        case 0:
	        	calFragment = (BalanceCalendarFragment) fm.findFragmentByTag(TAG_CALFRAGMENT);
	        	
	        	if (calFragment == null)
	    		{
	        		calFragment = BalanceCalendarFragment.newInstance(0, smvData);

		        	// pass savedCalView, if existing
		        	if ( getIntent().hasExtra("savedCal") )
		        		calFragment.setArguments(getIntent().getExtras());
		        	
	    			ft = fm.beginTransaction();
	    			ft.replace(R.id.balance_fragment_container, calFragment);
	    			ft.commit();
	    		}
  		
	    		
	    		fm.executePendingTransactions();
	    		
    		break;	
	            
            // Tagesansicht
	        case 1:

//	    		chartFragment = (BalanceChartFragment) fm.findFragmentByTag(TAG_DAYFRAGMENT);
//	    		
//	    		float[] hour_count = smvData.get(0).getCSVHourEnergy(timestampStartSel);
//	    		
//	    		if (chartFragment == null)
//	    		{
//	    			chartFragment = BalanceChartFragment.newInstance(0, hour_count);
//	    			ft = fm.beginTransaction();
//	    			ft.replace(R.id.balance_fragment_container, chartFragment);
//	    			ft.commit();
//	    		}
//  		
//	    		
//	    		fm.executePendingTransactions();
    		break;
	    		
	    	
	    		
	        // Zeitverteilung mit PV
	        case 2:  
//	    		chartFragment = (BalanceChartFragment) fm.findFragmentByTag(TAG_CALFRAGMENT);
//	    		if (chartFragment == null)
//	    		{
//	    			//chartFragment = BalanceChartFragment.newInstance(0,hour_count);
//	    			ft = fm.beginTransaction();
//	    			ft.replace(R.id.balance_fragment_container, chartFragment);
//	    			ft.commit();
//	    		}
//	    		fm.executePendingTransactions();
    		break;
	    		
	                
	        default: 
            break;
		}
	}
	
	
	
//	private void fetchData(final String newVersion)
//	{
//		new Thread(new Runnable() {
//			public void run()
//			{
//				
//				//3x Durchlauf für 3 CSV
//		    	for ( int i = 0; i < smvFiles.length; i++)
//		    	{		    				
//    				serverHelper = ServerHelper.getInstance(con);
//    				serverHelper = new ServerHelper("http://129.187.64.247/", settings.getString(Preferences.cUsername,""), null);
//    				
//    				String familyID = prefs.getString("FamilyID", "");
//    		
//    				serverHelper.getNewConsumption(con, "17", i);	//TODO: hier normal familyID reinschreiben!
//		        	
//		    	}
//
//		    	writeDownloadedVersion(newVersion);
//				
//	        	dialog.dismiss();	
//
//			}}).start();
//	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	private void getSmartHomeData(int which)
	{
		String csvName = smvFiles[which];
		
		float sumEnergyHour = 0;
		float sumEnergyDay = 0;
		int prevMinute = 0;
		int prevDay = 0;
	    
	    ArrayList<SmartmeterValue> smv = new ArrayList<SmartmeterValue>();
	    
	    switch (which)
		{
		case 0:
			smv = smarthomeSource.getAllValues();
			break;
		case 1:
			smv = smartloadingSource.getAllValues();
			break;
		case 2:
			smv = smartpvSource.getAllValues();
			break;
		default:
			break;
		}
	    
//		int[] intDate = new int[smvData.size()]; //Konvertierung String zu int-Array
//	    
//	    for ( int i = 0; i < (smvData.size())-1; i++)
//	    {
//	    	//Konvertieren Datum in int
//	    	String dateAndEnergyTotalTrimmed = smvData.get(i);
//	    	intDate[i] = Integer.parseInt(dateAndEnergyTotalTrimmed);
//	    }
//	    
//
//	    
//	    
//	    
//		GregorianCalendar currentDate = new GregorianCalendar(intDate[0], intDate[1], intDate[2], intDate[3], intDate[4], intDate[5]);
//		if (prevDay == 0)
//		{
//			prevDay = intDate[2];
//		}
//		
//	    float fEnergy = Float.parseFloat(dateAndEnergyTotal[(dateAndEnergyTotal.length)-1]);
//	  
//	    //Sum Energy per hour
//	    if ( intDate[4] >= prevMinute )
//	    {
//			sumEnergyHour = sumEnergyHour + fEnergy;
//		}
//	    else
//	    {
//			smvData.get(which).setCSVHour(currentDate, sumEnergyHour, csvName);
//			sumEnergyHour = fEnergy;
//		}
//	    
//	    //Sum Energy per Day
//	    if ( intDate[2] == prevDay)
//	    {
//	    	sumEnergyDay = sumEnergyDay + fEnergy;
//	    }
//	    else
//	    {
//	    	smvData.get(which).setCSVDay(currentDate, sumEnergyDay, csvName);
//			sumEnergyDay = fEnergy;
//	    }
//	    
//	    smvData.get(which).setCSVTotal(currentDate, fEnergy, csvName);
//
//		prevMinute = intDate[4];
//		prevDay = intDate[2];
//		
	}

	

	
	public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

	    if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	
	/**
	 * DatePicker
	 */
	@SuppressLint("ValidFragment")
	public class DatePicker extends DialogFragment{

	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the current date as the default date in the picker
	        final Calendar c = Calendar.getInstance();
	        int year = c.get(Calendar.YEAR);
	        int month = c.get(Calendar.MONTH);
	        int day = c.get(Calendar.DAY_OF_MONTH);

	        DatePickerDialog d = new DatePickerDialog(getActivity(), (BalanceSmartMeter)getActivity(), year, month, day); 
	        android.widget.DatePicker dp = d.getDatePicker(); 
	        
	        return d;
	        
	    }
	}
	
	public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth)
	{
		FragmentManager fragmanager = getSupportFragmentManager();

	    if (fragmanager.findFragmentByTag("startDatePicker") != null)
	    {
	    	tv_DateViewStart.setText(dayOfMonth + "." + (monthOfYear+1) + "." + year);
	    	calendarDateStart.set(year, monthOfYear, dayOfMonth);
	    	timestampStartSel = new GregorianCalendar(year, monthOfYear, dayOfMonth);   	
	    }
	    if (fragmanager.findFragmentByTag("endDatePicker") != null)
	    {
	    	tv_DateViewEnd.setText(dayOfMonth + "." +
					(monthOfYear+1) + "." + 
					year);
	    	calendarDateEnd.set(year, monthOfYear, dayOfMonth);
	    	timestampEndSel = new GregorianCalendar(year, monthOfYear, dayOfMonth);
	    }
	}
	
	public void onDateSetStart(View v)
	{	
		DialogFragment newFragment = new DatePicker();
		newFragment.show(getSupportFragmentManager(), "startDatePicker");	    
	}
	
	

	public void onDateSetEnd(View v)
	{
		//endClicked = true;
		
		DialogFragment newFragment = new DatePicker();
	    newFragment.show(getSupportFragmentManager(), "endDatePicker");
	}

}


//class checkForCSVUpdateTask extends AsyncTask<ServerHelper, Void, String>
//{
//    private Exception exception;
//	boolean success;
//	ServerHelper serverHelper;
//
//    protected String doInBackground(ServerHelper... param)
//    {
//    	serverHelper = param[0];
//		
//    	String newVersion = serverHelper.getCSVVersion();
//		
//    	return newVersion;
//    }
// }

