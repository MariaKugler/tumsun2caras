package de.tum.sun2car.balance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.internal.dr;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import de.tum.sun2car.MainPage;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.FamilyDB;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.SyncSun2CarService;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class BalanceTracks extends Activity
{
	List<Drives> drives;
	List<String> members;
	String userName;
	String password;

	private LocationDB locsSource;
	private DrivesDB drivesSource;
	private FamilyDB familySource;

    private ProgressDialog dialog;
    
	List<Double> co2g_total = new ArrayList<Double>();
	List<List<List<Double>>> locs_details = new ArrayList<List<List<Double>>>();

	private SharedPreferences settings;
	private SharedPreferences prefs;

	GridView calView;
	public GregorianCalendar month, itemmonth;// calendar instances.

	public BalanceTracksCalendarAdapter adapter;// adapter instance
//	public Handler calHandler;// for grabbing some event values for showing the dot
	// marker.
	public ArrayList<String> items; // container to store calendar items which
	// needs showing the event marker
	ArrayList<String> event;
	ArrayList<String> date;
	ArrayList<String> desc;

	Context con;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance_tracks);
		Locale.setDefault(Locale.GERMAN);

		con = this;
		
		drives = new ArrayList<Drives>();
		members = new ArrayList<String>();

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Fahrtdaten laden...");
    	dialog.setMessage("Fahrtdaten werden geladen.");
//    	dialog.setCancelable(false);
    	dialog.setCanceledOnTouchOutside(false);
    	
    	
    	// check whether sync-service is running at the moment
        SyncSun2CarService mService = new SyncSun2CarService();
        boolean running = mService.mIsRunning;
    	
    	boolean serviceRunning = false;
    	ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (SyncSun2CarService.class.getName().equals(service.service.getClassName()))
            {
            	serviceRunning =  true;
            }
        }
    	if ( running )
    	{
    		Toast.makeText(con, "Synchronisierung läuft derzeit.", Toast.LENGTH_LONG).show();
    	}

		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		items = new ArrayList<String>();
		event = new ArrayList<String>();

		calView = (GridView) findViewById(R.id.gridview);

//		calHandler = new Handler();

		TextView title = (TextView) findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		RelativeLayout previous = (RelativeLayout) findViewById(R.id.previous);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
			}
		});

		RelativeLayout next = (RelativeLayout) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();

			}
		});

		
	}
	
	
	public void onResume()
	{
    	super.onResume();
    	
    	if ( drives.size() != 0 )
    	{

    	}
    	else
    	{
    		locsSource = new LocationDB(this);
    		locsSource.open();
    		
    		drivesSource = new DrivesDB(this);
    		drivesSource.open();
    		
    		familySource = new FamilyDB(this);
    		familySource.open();
        	
    		dialog.show();
    		
        	new Thread(new Runnable() {
    			public void run()
    			{
					members = familySource.getAllMembers(prefs.getString("FamilyID", ""));
    				
    				userName = settings.getString(Preferences.cUsername,null);
    		    	password = settings.getString(Preferences.cPass, null);
    		
    		    	drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));
    				
    		    	if ( drives.isEmpty() )
    		    	{
    		    		Handler handler = new Handler(Looper.getMainLooper());
    			    	handler.post(new Runnable()
    			    	{

    						@Override
    						public void run()
    						{
    				    		Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
    		    				dialog.dismiss();	
    						} 
    			    	});
    		    	}
    		    	else
    		    	{
//    					Collections.sort(smvData, Collections.reverseOrder());
//    					for (int i = 0; i < smvData.size(); i++ )
//    					{
//    						co2g_total.add(smvData.get(i).getCo2gTotal());
//    					}
	
        				dialog.dismiss();	
        				
        				
        				Handler handler = new Handler(Looper.getMainLooper());
        		    	handler.post(new Runnable()
        		    	{

        					@Override
        					public void run()
        					{
        						adapter = new BalanceTracksCalendarAdapter(con, month, drives);

//        						calHandler.post(calendarUpdater);
        						
        						// set colors depending on
        						calView.setAdapter(adapter);
        						
        						
        						calView.setOnItemClickListener(new OnItemClickListener() 
        						{
        							public void onItemClick(AdapterView parent, View v, int position, long id) 
        							{     								
        								String selectedGridDate = BalanceTracksCalendarAdapter.dayString
        										.get(position);
        								
        								String[] separatedTime = selectedGridDate.split("-");
        								
        								((BalanceTracksCalendarAdapter) parent.getAdapter()).setSelected(v, ((BalanceTracksCalendarAdapter) parent.getAdapter()).getDayBalance(separatedTime));
        								
        								
        								String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking last part of date. ie; 2 from 2012-12-02.
        								int gridvalue = Integer.parseInt(gridvalueString);
        								// navigate to next or previous month on clicking offdays.
        								if ((gridvalue > 10) && (position < 8)) 
        								{
        									setPreviousMonth();
        									refreshCalendar();
        								} 
        								else if ((gridvalue < 7) && (position > 28)) 
        								{
        									setNextMonth();
        									refreshCalendar();
        								}
        								
        								((BalanceTracksCalendarAdapter) parent.getAdapter()).setSelected(v, ((BalanceTracksCalendarAdapter) parent.getAdapter()).getDayBalance(separatedTime));
        								

        								int yearSel = Integer.parseInt(separatedTime[0]);
        								int monthSel = Integer.parseInt(separatedTime[1])-1;
        								int daySel = Integer.parseInt(separatedTime[2]);
//        								
        								
    				                    Intent intent = new Intent(con, BalanceTracksDayView.class);

    									Bundle b = new Bundle();
//    									b.putDouble("trackID", trackID);
//    									b.putString("trackMOT", trackMOT);
    									b.putInt("year", yearSel);
    						            b.putInt("month", monthSel);
    						            b.putInt("day", daySel);

    									intent.putExtras(b);
    									
    									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    									
    									startActivity(intent);

        							}

        						});
        						
        						
        					} 
        		    	});
    		    	}
    			
    			}}).start();

    	}    	
	}
	
	
	
	
	
	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}

	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}

	}

	protected void showToast(String string) {
		Toast.makeText(this, string, Toast.LENGTH_SHORT).show();

	}

	public void refreshCalendar() {
		TextView title = (TextView) findViewById(R.id.title);

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
//		calHandler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}
	
	@Override
    public void onBackPressed()
    {
    	Intent intent = new Intent(con, MainPage.class);

		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		startActivity(intent);
    }

//	public Runnable calendarUpdater = new Runnable() {
//
//		@Override
//		public void run() {
////			items.clear();
//
//			//	   // Print dates of the current week
//			//	   SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN);
//			//	   String itemvalue;
//			//	   event = Utility.readCalendarEvent(con);
//			//	   Log.d("=====Event====", event.toString());
//			//	   Log.d("=====Date ARRAY====", Utility.startDates.toString());
//			//
//			//	   for (int i = 0; i < Utility.startDates.size(); i++) {
//			//	    itemvalue = df.format(itemmonth.getTime());
//			//	    itemmonth.add(GregorianCalendar.DATE, 1);
//			//	    items.add(Utility.startDates.get(i).toString());
//			//	   }
//			//	   adapter.setItems(items);
//			adapter.notifyDataSetChanged();
//		}
//	};

}
