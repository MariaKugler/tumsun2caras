package de.tum.sun2car.balance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.FamilyDB;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.SyncSun2CarService;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class BalanceTracksDayView extends Activity 
{
	Bundle b;
	int day;
	int month;
	int year;

	List<Drives> drives;
	List<String> members;
	String user_token;
	String userName;
	String password;
	
	private SharedPreferences settings;
	private SharedPreferences prefs;

	private GraphicalView graph_emissions;
	LinearLayout ll_emissionschart;
	TextView tv_distance;
	TextView tv_emissions;
	
	double sumCO2g = 0.0;
	double sumDistance = 0.0;

	Context con;

	HashMap<Long, Double> emissionTrend = new HashMap<Long, Double>();

	private LocationDB locsSource;
	private DrivesDB drivesSource;
	private FamilyDB familySource;
	
    private ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance_tracks_day_view);

		ll_emissionschart = (LinearLayout) findViewById(R.id.chart_emissions);
		tv_distance = (TextView) findViewById(R.id.balance_tracks_day_distance);
		tv_emissions = (TextView) findViewById(R.id.balance_tracks_day_emissions);
		
		Bundle b = new Bundle();
		b = getIntent().getExtras();
		
		day = b.getInt("day");
		month = b.getInt("month");
		year = b.getInt("year");
		
		this.setTitle("Emissions-Bilanz am " + day + "." + (month+1) + "." + year);

		con = this;
		
		drives = new ArrayList<Drives>();
		members = new ArrayList<String>();

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Fahrtdaten laden...");
    	dialog.setMessage("Fahrtdaten werden geladen.");
//    	dialog.setCancelable(false);
    	dialog.setCanceledOnTouchOutside(false);
    	
    	
    	// check whether sync-service is running at the moment
        SyncSun2CarService mService = new SyncSun2CarService();
        boolean running = mService.mIsRunning;
    	
    	boolean serviceRunning = false;
    	ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (SyncSun2CarService.class.getName().equals(service.service.getClassName()))
            {
            	serviceRunning =  true;
            }
        }
    	if ( running )
    	{
    		Toast.makeText(con, "Synchronisierung läuft derzeit.", Toast.LENGTH_LONG).show();
    	}
	}
	
	public void onResume()
	{
    	super.onResume();
    	
    	if ( drives.size() != 0 )
    	{

    	}
    	else
    	{
    		locsSource = new LocationDB(this);
    		locsSource.open();
    		
    		drivesSource = new DrivesDB(this);
    		drivesSource.open();
    		
    		familySource = new FamilyDB(this);
    		familySource.open();
        	
    		dialog.show();
    		
        	new Thread(new Runnable() {
    			public void run()
    			{
					members = familySource.getAllMembers(prefs.getString("FamilyID", ""));
    				
    				userName = settings.getString(Preferences.cUsername,null);
    		    	password = settings.getString(Preferences.cPass, null);
    		
    		    	drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));
    				
    		    	if ( drives.isEmpty() )
    		    	{
    		    		Handler handler = new Handler(Looper.getMainLooper());
    			    	handler.post(new Runnable()
    			    	{

    						@Override
    						public void run()
    						{
    				    		Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
    		    				dialog.dismiss();	
    						} 
    			    	});
    		    	}
    		    	else
    		    	{	
    		    		Collections.sort(drives, Collections.reverseOrder());
    		    		
    		    		  
    		    		Calendar midnight_start = new GregorianCalendar();
    		    		midnight_start.set(year, month, day, 0, 0, 0);
    		    		
    		    		sumCO2g = 0.0;
    		    		sumDistance = 0.0;
    		    		emissionTrend.put(midnight_start.getTimeInMillis(), sumCO2g);
    		    		
    		    		
    		    		for ( Drives d : drives)
    		    		{
    		    			int yearD = d.getStartDate().getYear()+1900;
    		    			int monthD = d.getStartDate().getMonth();
    		    			int dayD = d.getStartDate().getDate();
    		    				
    		    			if ( year == yearD && month == monthD && day == dayD )
    		    			{
    		    				emissionTrend.put(d.getStartDate().getTime(), sumCO2g);
    		    				sumCO2g = sumCO2g + d.getCo2gTotal();
    		    				emissionTrend.put(d.getStopDate().getTime(), sumCO2g);
    		    				sumDistance = sumDistance + d.getTrip_length();
    		    			}
    		    		}
    		    		Calendar midnight_end = new GregorianCalendar();
    		    		midnight_end.set(year, month, day, 23, 59, 59);
    		    		
    		    		emissionTrend.put(midnight_end.getTimeInMillis(), sumCO2g);
    		    		
    		    		dialog.dismiss();	
        				
        				
        				Handler handler = new Handler(Looper.getMainLooper());
        		    	handler.post(new Runnable()
        		    	{

        					@Override
        					public void run()
        					{
        						if (graph_emissions == null)
    							{
        							graph_emissions = (GraphicalView) ChartFactory.getTimeChartView(con, getData(), getRenderer(), "HH:mm");
        							
    								//graph_emissions = ChartFactory.getTimeChartView(con, getData(emissionTrend),getRenderer(), "HH:mm");
    										//getLineChartView(con, getData(emissionTrend),getRenderer());
    								ll_emissionschart.addView(graph_emissions);
    							}
    							else
    							{
    								graph_emissions.repaint(); // use this whenever data has changed and you want to redraw
    							}
        						
        						
        						//String.format( "Value of a: %.2f", a )
        						tv_distance.setText( String.format("%.3f km", (sumDistance/1000)) );
        						tv_emissions.setText(String.format("%.3f kg", (sumCO2g/1000)) );
        						
        					} 
        		    	});
    		    	}
    			
    			}}).start();

    	}    	
	}
	
	
	public XYMultipleSeriesDataset getData()
	{    
		TimeSeries et = new TimeSeries("Emissions");
        for ( Long timeKey : emissionTrend.keySet() )
        {
        	et.add(new Date(timeKey), emissionTrend.get(timeKey)/1000);
        }
        
        // Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset_emissions = new XYMultipleSeriesDataset();
	    
		dataset_emissions.addSeries(et);
		
	    return dataset_emissions;
	}
	
	public XYMultipleSeriesRenderer getRenderer()
	{ 
		double maxY = (sumCO2g/1000) * 1.5;
		// Creating XYSeriesRenderer to customize visitsSeries
        XYSeriesRenderer emissionsRenderer = new XYSeriesRenderer();
        emissionsRenderer.setColor(Color.parseColor("#006ab3"));
        emissionsRenderer.setPointStyle(PointStyle.POINT);
        emissionsRenderer.setFillPoints(true);
        emissionsRenderer.setLineWidth(4);
        emissionsRenderer.setFillBelowLine(true);
        emissionsRenderer.setFillBelowLineColor(Color.parseColor("#006ab3"));
//        emissionsRenderer.setDisplayChartValues(true);
//        emissionsRenderer.setChartValuesTextSize(25);
//        emissionsRenderer.setChartValuesFormat(new DecimalFormat("#.###"));

//        FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_BELOW);
//        fill.setColor(Color.parseColor("#006ab3"));
//        emissionsRenderer.addFillOutsideLine(fill);
 
        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
 
        multiRenderer.setXTitle("Uhrzeit");
        multiRenderer.setYTitle("Emissionen in kg");
 
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        multiRenderer.addSeriesRenderer(emissionsRenderer);
		
		// set some properties on the main renderer
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.parseColor("#edeff1"));
		multiRenderer.setAxisTitleTextSize(35);
//		renderer_emissions.setChartTitleTextSize(20);
		multiRenderer.setLabelsTextSize(30);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setYLabelsPadding(5);
//		renderer_emissions.setLegendTextSize(15);
		multiRenderer.setMargins(new int[] { 0, 100, 50, 10 });  // top, left, bottom, right
//		renderer_emissions.setZoomButtonsVisible(true);
		multiRenderer.setYAxisMax(maxY);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setPointSize(5);
		multiRenderer.setShowLabels(true);
		multiRenderer.setShowLegend(false);
		multiRenderer.setPanEnabled(false,false);
		multiRenderer.setZoomEnabled(false, false);
		
    	
        return multiRenderer;
	}


	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance_tracks_day_view, menu);
		return true;
	}

}
