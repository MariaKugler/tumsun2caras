package de.tum.sun2car.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import de.tum.sun2car.classes.SmartmeterValue;

public class SmartHomeDB
{
	private static final int VERSION_SMARTHOMEDB = 2;
	private static final String NOM_SMARTHOMEDB = "sun2car.db";
 
	private static final String TABLE_SMARTHOME = "table_smarthome";

	public static final String COL_SMARTHOME_ID = "SMARTHOME_ID";
	private static final int NUM_COL_ID = 0;
	public static final String COL_SMARTHOME_TIMESTAMP = "TIMESTAMP";
	private static final int NUM_COL_TIMESTAMP = 1;
	public static final String COL_SMARTHOME_ENERGY = "ENERGY";
	private static final int NUM_COL_ENERGY = 2;

	private String[] allColumns = {SqliteDB.COL_SMARTHOME_ID,
									SqliteDB.COL_SMARTHOME_TIMESTAMP, 
									SqliteDB.COL_SMARTHOME_ENERGY};
	
	private SQLiteDatabase smarthomedb;
 
	private SqliteDB myBaseSQLite;
 
	public SmartHomeDB(Context context){
		//Erzeugung der DB inkl. Tabellen
		myBaseSQLite = new SqliteDB(context, NOM_SMARTHOMEDB, null, VERSION_SMARTHOMEDB);
	}
 
	public void open(){
		//DB im Schreib-Modus öffnen
		smarthomedb = myBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//Datenbank schliessen
		smarthomedb.close();
	}
 
	public SQLiteDatabase getDrivesDB(){
		return smarthomedb;
	} 
	
	public long addValues(SmartmeterValue smv)
	{			
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(smv.getDate());
		
		if ( !checkForValue(smv.getDate()) )
		{
			//Erzeugung einer ContentValues 
			ContentValues values = new ContentValues();
			
			//Einfügen eines Element als key-value-Paar (Spalte-Wert-Paar)
			values.put(COL_SMARTHOME_TIMESTAMP, strDate);
			values.put(COL_SMARTHOME_ENERGY, smv.getEnergy());


			//Ein Objekt wird in bdd via ContentValues eingefügt
			return smarthomedb.insert(TABLE_SMARTHOME, null, values);
		}
		else
			return 0;
		
	} 

 	
 	public boolean checkForValue(Date date)
 	{			
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date);
		
		
 		Cursor c = smarthomedb.query(TABLE_SMARTHOME, allColumns, COL_SMARTHOME_TIMESTAMP + "=\"" + strDate + "\"", null, null, null, null);
 		
 		boolean result = c.moveToFirst();
 		c.close();
 		
		return result;
		// true if already there
		// false if not there
	}
 	 	
 	
	public ArrayList<SmartmeterValue> getAllValues()
	{
		ArrayList<SmartmeterValue> smv_list = new ArrayList<SmartmeterValue>();
		
		Cursor c = smarthomedb.query(TABLE_SMARTHOME, allColumns, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			SmartmeterValue smv = cursorToSMV(c);
			
			smv_list.add(smv);
			c.moveToNext();
		}
		
		c.close();
		
		return smv_list;
	}
	
	public ArrayList<SmartmeterValue> getIntervalValues(Calendar start, Calendar end)
	{
		ArrayList<SmartmeterValue> smv_list = new ArrayList<SmartmeterValue>();
		
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDateStart = sdf.format(start.getTime());
		String strDateEnd = sdf.format(end.getTime());
		
		
		Cursor c = smarthomedb.query(TABLE_SMARTHOME, allColumns, COL_SMARTHOME_TIMESTAMP + " between \"" + strDateStart + "\" and \"" + strDateEnd + "\" order by " + COL_SMARTHOME_TIMESTAMP + " ASC;", 
				null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			SmartmeterValue smv = cursorToSMV(c);
			
			smv_list.add(smv);
			c.moveToNext();
		}
		
		c.close();
		
		return smv_list;
	}
	
	public int removeValues()
 	{
		return smarthomedb.delete(TABLE_SMARTHOME, null, null);
	}

	
	private SmartmeterValue cursorToSMV(Cursor c)
	{		
		if (c.getCount() == 0)
			return null;
		
		SmartmeterValue smv = new SmartmeterValue();
		
		smv.setTime(c.getString(NUM_COL_TIMESTAMP));
		smv.setEnergy(c.getDouble(NUM_COL_ENERGY));
		
 
		return smv;
	}
}
