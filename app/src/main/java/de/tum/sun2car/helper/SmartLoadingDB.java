package de.tum.sun2car.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import de.tum.sun2car.classes.SmartmeterValue;

public class SmartLoadingDB
{
	private static final int VERSION_SMARTLOADINGDB = 2;
	private static final String NOM_SMARTLOADINGDB = "sun2car.db";
 
	private static final String TABLE_SMARTLOADING = "table_smartloading";

	public static final String COL_SMARTLOADING_ID = "SMARTLOADING_ID";
	private static final int NUM_COL_ID = 0;
	public static final String COL_SMARTLOADING_TIMESTAMP = "TIMESTAMP";
	private static final int NUM_COL_TIMESTAMP = 1;
	public static final String COL_SMARTLOADING_ENERGY = "ENERGY";
	private static final int NUM_COL_ENERGY = 2;

	private String[] allColumns = {SqliteDB.COL_SMARTLOADING_ID,
									SqliteDB.COL_SMARTLOADING_TIMESTAMP, 
									SqliteDB.COL_SMARTLOADING_ENERGY};
	
	private SQLiteDatabase smartloadingdb;
 
	private SqliteDB myBaseSQLite;
 
	public SmartLoadingDB(Context context){
		//Erzeugung der DB inkl. Tabellen
		myBaseSQLite = new SqliteDB(context, NOM_SMARTLOADINGDB, null, VERSION_SMARTLOADINGDB);
	}
 
	public void open(){
		//DB im Schreib-Modus öffnen
		smartloadingdb = myBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//Datenbank schliessen
		smartloadingdb.close();
	}
 
	public SQLiteDatabase getDrivesDB(){
		return smartloadingdb;
	} 
	
	public long addValues(SmartmeterValue smv)
	{		
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(smv.getDate());
		
		if ( !checkForValue(smv.getDate()) )
		{
			//Erzeugung einer ContentValues 
			ContentValues values = new ContentValues();
			
			//Einfügen eines Element als key-value-Paar (Spalte-Wert-Paar)
			values.put(COL_SMARTLOADING_TIMESTAMP, strDate);
			values.put(COL_SMARTLOADING_ENERGY, smv.getEnergy());


			//Ein Objekt wird in bdd via ContentValues eingefügt
			return smartloadingdb.insert(TABLE_SMARTLOADING, null, values);
		}
		else
			return 0;
	} 

 	
 	public boolean checkForValue(Date date)
 	{		
 		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date);
		
		
 		Cursor c = smartloadingdb.query(TABLE_SMARTLOADING, allColumns, COL_SMARTLOADING_TIMESTAMP + "=\"" + strDate + "\"", null, null, null, null);
 		
 		boolean result = c.moveToFirst();
 		c.close();
 		
		return result;
		// true if already there
		// false if not there
	}
 	 	
 	
	public ArrayList<SmartmeterValue> getAllValues()
	{
		ArrayList<SmartmeterValue> smv_list = new ArrayList<SmartmeterValue>();
		
		Cursor c = smartloadingdb.query(TABLE_SMARTLOADING, allColumns, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			SmartmeterValue smv = cursorToSMV(c);
			
			smv_list.add(smv);
			c.moveToNext();
		}
		
		c.close();
		
		return smv_list;
	}
	
	public ArrayList<SmartmeterValue> getIntervalValues(Calendar start, Calendar end)
	{
		ArrayList<SmartmeterValue> smv_list = new ArrayList<SmartmeterValue>();
		
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDateStart = sdf.format(start.getTime());
		String strDateEnd = sdf.format(end.getTime());
		
		
		Cursor c = smartloadingdb.query(TABLE_SMARTLOADING, allColumns, COL_SMARTLOADING_TIMESTAMP + " between \"" + strDateStart + "\" and \"" + strDateEnd + "\" order by " + COL_SMARTLOADING_TIMESTAMP + " ASC;", 
				null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			SmartmeterValue smv = cursorToSMV(c);
			
			smv_list.add(smv);
			c.moveToNext();
		}
		
		c.close();
		
		return smv_list;
	}
	
	public int removeValues()
 	{
		return smartloadingdb.delete(TABLE_SMARTLOADING, null, null);
	}

	
	private SmartmeterValue cursorToSMV(Cursor c)
	{		
		if (c.getCount() == 0)
			return null;
		
		SmartmeterValue smv = new SmartmeterValue();
		
		smv.setTime(c.getString(NUM_COL_TIMESTAMP));
		smv.setEnergy(c.getDouble(NUM_COL_ENERGY));
		
 
		return smv;
	}
}
