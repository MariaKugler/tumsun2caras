package de.tum.sun2car.logbook;

import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.LinearLayout;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class LogbookDrivesMap extends FragmentActivity {

  Context con;

  LinearLayout ll_speedchart;
  private SharedPreferences settings;

  private GraphicalView graph_speed;

  GoogleMap map;
  LatLngBounds.Builder bc;
  List<MyLocation> locations;
  Double trackID;
  PolylineOptions poly;

  private ProgressDialog dialog;
  private LocationDB locsSource;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_logbook_drives_map);

    con = this;

    // Getting Google Play availability status
    int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

    map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_singledrive)).getMap();
    map.setMyLocationEnabled(true);

    locsSource = new LocationDB(this);
    locsSource.open();

    Bundle b = new Bundle();
    b = getIntent().getExtras();
    trackID = b.getDouble("trackID");

    ll_speedchart = (LinearLayout) findViewById(R.id.chart_speed);

    locations = new ArrayList<MyLocation>();
    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);

    dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setTitle("Daten laden...");
    dialog.setMessage("Track-Daten werden geladen.");
    dialog.show();

    new Thread(new Runnable() {
			public void run() {

        locations = locsSource.getAllLocationsByTrack(trackID);

				if ( !locations.isEmpty() )
				{
					poly = new PolylineOptions()
			        .color(Color.parseColor("#006ab3"));

			        bc = new LatLngBounds.Builder();
			        
			        //Add LatLngs to a polyline
			        for(MyLocation loc : locations)
			        {
			            LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());
			        	poly.add(ll);
			        	bc.include(ll);
			        }

			        
			    	dialog.dismiss();
			    	
			    	Handler handler = new Handler(Looper.getMainLooper());
			    	handler.post(new Runnable()
			    	{

						@Override
						public void run()
						{
							map.addPolyline(poly);
							map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
							
							if (graph_speed == null)
							{
								graph_speed = ChartFactory.getLineChartView(con, getData(locations),getRenderer());
								ll_speedchart.addView(graph_speed);
							}
							else
							{
								graph_speed.repaint(); // use this whenever data has changed and you want to redraw
							}
						    
						 
						} 
			    	});
				}
				else
				{
					dialog.dismiss();
					
					Handler handler = new Handler(Looper.getMainLooper());
			    	handler.post(new Runnable()
			    	{

						@Override
						public void run()
						{
							new AlertDialog.Builder(con)
							.setTitle(R.string.logbook_drives_no_locs)
							.setMessage(R.string.logbook_drives_no_locs_msg)
							.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener()
									{
										public void onClick(DialogInterface dialoginterface,int i)
										{								
											Intent intent = new Intent(con, Logbook.class);
											intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
											startActivity(intent);
											
											((Activity) con).finish();
										}
									})
							.show();
						} 
			    	});
				}
				
				
			}
		}).start();   
	    
	}


  public XYMultipleSeriesDataset getData(List<MyLocation> locations) {
    XYMultipleSeriesDataset dataset_speed = new XYMultipleSeriesDataset();
    XYSeries dataSeries = new XYSeries("speed_series");

    int i = 0;
    for (MyLocation loc : locations) {
//			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//			java.util.Date parsedDate = dateFormat.parse("2006-05-22 14:04:59:612");
//			java.sql.Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

      dataSeries.add(i, loc.getSpeed());
      i = i + 1;
    }

    dataset_speed.addSeries(dataSeries);

    return dataset_speed;
  }

  public XYMultipleSeriesRenderer getRenderer() {
    XYSeriesRenderer r = new XYSeriesRenderer();
    r.setColor(Color.parseColor("#006ab3"));
    r.setLineWidth(2);

//        r.setPointStyle(PointStyle.SQUARE); // CIRCLE, DIAMOND , POINT, TRIANGLE, X
//        r.setFillPoints(true); // not for point or x
    // don't know how to set point size or point color
//        r.setFillBelowLine(true);
//        r.setFillBelowLineColor(Color.WHITE);

    XYMultipleSeriesRenderer renderer_speed = new XYMultipleSeriesRenderer();
    renderer_speed.addSeriesRenderer(r);

    // set some properties on the main renderer
    renderer_speed.setApplyBackgroundColor(true);
    renderer_speed.setBackgroundColor(Color.WHITE);
//		renderer_speed.setAxisTitleTextSize(16);
//		renderer_speed.setChartTitleTextSize(20);
//		renderer_speed.setLabelsTextSize(15);
//		renderer_speed.setLegendTextSize(15);
    renderer_speed.setMargins(new int[]{0, 0, 0, 0});
//		renderer_speed.setZoomButtonsVisible(true);
    renderer_speed.setPointSize(5);
    renderer_speed.setShowLabels(false);
    renderer_speed.setShowLegend(false);
    renderer_speed.setPanEnabled(false, false);
    renderer_speed.setZoomEnabled(false, false);


    return renderer_speed;
  }

  protected void onResume() {
    super.onResume();
    if (graph_speed != null) {
      graph_speed.repaint();
    }
  }

	@Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.logbook_drives_map, menu);
    return true;
  }

}
