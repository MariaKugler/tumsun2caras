package de.tum.sun2car.tumsensorrecorderlib;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SensorService extends Service {
	
	private static boolean mIsRunning = false;
	
	private EventListener mSensorListener;
	
	//private static LinkedBlockingDeque<String> mFileSendQueue = null;
 
    private static final String TAG = "SensorService";
    
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate() {
		Log.i(TAG, "onCreate");
		
        mSensorListener = new EventListener(this);
        
        startProcessing();
    }
    
    public static boolean getIsRunning() {
    	return mIsRunning;
    }
    
	@Override
	public void onDestroy() {
		mSensorListener.stoplogging();
		
		try {
			// wait 1 second for the SensorThread
			mSensorListener.join(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mIsRunning = false;
		
		stopForeground(true);
		
		//Toast.makeText(this, "Sensor Service stopped", Toast.LENGTH_LONG).show();
		Log.i(TAG, "onDestroy");
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		startProcessing();
		
		//Toast.makeText(this, "Sensor Service started", Toast.LENGTH_LONG).show();
		Log.i(TAG, "onStart");
		
		return START_STICKY;
	}
	
	private void startProcessing() {
		if(!mIsRunning) {
			mIsRunning = true;
			mSensorListener.start();
	    	
	    	Notification n = new Notification();
	    	startForeground(3, n);
		}
	}
	

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}