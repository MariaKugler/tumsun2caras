package de.tum.sun2car.balance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.R.string;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;

public class BalanceTracksCalendarAdapter extends BaseAdapter 
{  
	private Context mContext;  

	private java.util.Calendar month;  
	public GregorianCalendar pmonth; // calendar instance for previous month  
	/** 
	 * calendar instance for previous month for getting complete view 
	 */  
	public GregorianCalendar pmonthmaxset;  
	private GregorianCalendar selectedDate;  
	int firstDay;  
	int maxWeeknumber;  
	int maxP;  
	int calMaxP;  
	int lastWeekDay;  
	int leftDays;  
	int mnthlength;  
	String itemvalue, curentDateString;  
	SimpleDateFormat df;  
 
	public static  List<String> dayString;  
	private View previousView;  
	private int previousState;

	List<Drives> drives;

	public BalanceTracksCalendarAdapter(Context c, GregorianCalendar monthCalendar, List<Drives> d) 
	{  
		BalanceTracksCalendarAdapter.dayString = new ArrayList<String>();  
		Locale.setDefault(Locale.GERMAN);  
		month = monthCalendar;  
		selectedDate = (GregorianCalendar) monthCalendar.clone();  
		mContext = c;  
		month.set(GregorianCalendar.DAY_OF_MONTH, 1);  
		df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN);  
		curentDateString = df.format(selectedDate.getTime());  
		refreshDays();  
		
		// get smvData
		drives = new ArrayList<Drives>();
		drives = d;
	}  
 

	public int getCount() 
	{  
		return dayString.size();  
	}  

	public Object getItem(int position) 
	{  
		return dayString.get(position);  
	}  

	public long getItemId(int position) 
	{  
		return 0;  
	}  

	// create a new view for each item referenced by the Adapter  
	public View getView(int position, View convertView, ViewGroup parent) 
	{  
		View v = convertView;  
		TextView dayView;  
		if (convertView == null) 
		{ // if it's not recycled, initialize some  
			// attributes  
			LayoutInflater vi = (LayoutInflater) mContext  
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
			v = vi.inflate(R.layout.calendar_item, null);  

		}  
		dayView = (TextView) v.findViewById(R.id.date);  
		// separates daystring into parts.  
		String[] separatedTime = dayString.get(position).split("-");  
		// taking last part of date. ie; 2 from 2012-12-02  
		String gridvalue = separatedTime[2].replaceFirst("^0*", "");  
		
		// checking whether the day is in current month or not.  
		if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay))
		{  
			// setting offdays to grey color.  
			dayView.setTextColor(Color.parseColor("#d4d4d4"));  
			dayView.setClickable(false);  
			dayView.setFocusable(false);  
		} 
		else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) 
		{  
			dayView.setTextColor(Color.parseColor("#d4d4d4"));  
			dayView.setClickable(false);  
			dayView.setFocusable(false);  
		} 
		else 
		{  
			// setting curent month's days in black color.  
			dayView.setTextColor(Color.BLACK);  
		}  

		/*
		 *  set Colors
		 *  green:	#99cc00 --> type 0
		 *  orange:	#ffbb33 --> type 1
		 *  red:	#b9102d --> type 2
		 */
		int type = getDayBalance(separatedTime);
		
		if (dayString.get(position).equals(curentDateString))
		{  
			setSelected(v, type);  
			previousView = v;
			previousState = type;
		}
		else
		{  
			switch(type)
			{
			case 0: // good
				v.setBackgroundColor(Color.parseColor("#99cc00"));  
				break;
				
			case 1: // middle
				v.setBackgroundColor(Color.parseColor("#ffbb33"));    
				break;
				
			case 2: // bad
				v.setBackgroundColor(Color.parseColor("#b9102d"));  
				break;
				
			default:
				break;
			}
		}  
		
		dayView.setText(gridvalue);  

		// create date string for comparison  
		String date = dayString.get(position);  

		if (date.length() == 1) 
		{  
			date = "0" + date;  
		}  
		
		String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);  
		if (monthStr.length() == 1) 
		{  
			monthStr = "0" + monthStr;  
		}   
		return v;  
	}  

	double co2_avg_km = 130;
	public int getDayBalance(String[] separatedTime)
	{
		int yearSel = Integer.parseInt(separatedTime[0]);
		int monthSel = Integer.parseInt(separatedTime[1]);
		int daySel = Integer.parseInt(separatedTime[2]);

		double sumCO2g = 0.0;
		double sumDistance = 0.0;
		
		for ( Drives d : drives)
		{
			int yearD = d.getStartDate().getYear()+1900;
			int monthD = d.getStartDate().getMonth();
			int dayD = d.getStartDate().getDate();
			
				
			if ( yearSel == yearD && monthSel == (monthD+1) && daySel == dayD )
			{
				sumCO2g = sumCO2g + d.getCo2gTotal();
				sumDistance = sumDistance + ( d.getTrip_length() / 1000 );
			}
		}

		double sumCO2kg = sumCO2g / 1000;
		double avgCO2gPerKM = sumCO2g / sumDistance;
		
		if ( sumDistance != 0 )
		{
			double ratio = avgCO2gPerKM / co2_avg_km;
			// bad #b9102d
			if ( ratio > 1 )
			{
				return 2;
			}
			// middle #ffbb33
			else if ( ratio > 0.75 )
			{
				return 1;
			}
			// good #99cc00
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
		
	}

	public View setSelected(View view, int t) 
	{  
		if (previousView != null) 
		{  
			switch(previousState)
			{
			case 0: // good
				previousView.setBackgroundColor(Color.parseColor("#99cc00"));  
				break;
				
			case 1: // middle
				previousView.setBackgroundColor(Color.parseColor("#ffbb33"));    
				break;
				
			case 2: // bad
				previousView.setBackgroundColor(Color.parseColor("#b9102d"));  
				break;
				
			default:
				break;
			}
		}  
		
		previousView = view;
		previousState = t;
		switch(t)
		{
		case 0: // good
			view.setBackgroundResource(R.drawable.balance_cal_sel_day_good);  
			break;
			
		case 1: // middle
			view.setBackgroundResource(R.drawable.balance_cal_sel_day_middle);  
			break;
			
		case 2: // bad
			view.setBackgroundResource(R.drawable.balance_cal_sel_day_bad);  
			break;
			
		default:
			break;
		}
		return view;  
	}  

	public void refreshDays() 
	{    
		dayString.clear();  
		Locale.setDefault(Locale.GERMAN);  
		pmonth = (GregorianCalendar) month.clone();  
		// month start day. ie; sun, mon, etc  
		firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);  
		// finding number of weeks in current month.  
		maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);  
		// allocating maximum row number for the calView.  
		mnthlength = maxWeeknumber * 7;  
		
		if ( firstDay == 1 )
		{
			mnthlength = (maxWeeknumber * 6) - 1;  
		}
		
		
		maxP = getMaxP(); // previous month maximum day 31,30....  
		calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...  
		/** 
		 * Calendar instance for getting a complete calView including the three 
		 * month's (previous,current,next) dates. 
		 */  
		pmonthmaxset = (GregorianCalendar) pmonth.clone();  
		/** 
		 * setting the start date as previous month's required date. 
		 */  
		pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);  

		/** 
		 * filling calendar calView. 
		 */  
		for (int n = 0; n < mnthlength; n++) {  

			itemvalue = df.format(pmonthmaxset.getTime());  
			pmonthmaxset.add(GregorianCalendar.DATE, 1);  
			dayString.add(itemvalue);  

		}  
	}  

	private int getMaxP() {  
		int maxP;  
		if (month.get(GregorianCalendar.MONTH) == month  
				.getActualMinimum(GregorianCalendar.MONTH)) {  
			pmonth.set((month.get(GregorianCalendar.YEAR) - 1),  
					month.getActualMaximum(GregorianCalendar.MONTH), 1);  
		} else {  
			pmonth.set(GregorianCalendar.MONTH,  
					month.get(GregorianCalendar.MONTH) - 1);  
		}  
		maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);  

		return maxP;  
	}  

}  
