package de.tum.sun2car.prediction;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.CombinedXYChart;
import org.achartengine.chart.CubicLineChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.classes.SmartmeterValue;
import de.tum.sun2car.classes.WeatherValue;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.SmartHomeDB;

public class Prediction extends Activity implements OnItemSelectedListener
{
	Context con;
	private SharedPreferences prefs;
	Spinner spinner;
	ArrayAdapter<String> adapter;
	
	private ArrayList<WeatherValue> weatherForecastData = new ArrayList<WeatherValue>();
	private ArrayList<String> forecastDates = new ArrayList<String>();
	
	private GraphicalView graph_prediction;
	LinearLayout ll_prediction;
	
	private GraphicalView graph_prediction_balance;
	LinearLayout ll_prediction_balance;
	
	List<Drives> drives;    

	int[] hour_count = new int[24];
	int[] day_count = new int[7];
	int[][] mobility_count = new int[2][24];

	private ProgressDialog dialog;
	
	private DrivesDB drivesSource;
	private SmartHomeDB smarthomeSource;
	
	ArrayList<ArrayList<ArrayList<Drives>>> mobilityBehavior = new ArrayList<ArrayList<ArrayList<Drives>>>();
	// 1. Ebene: Wochentags - Wochenende
	// 2. Ebene: Tagesstunden 0-23
	// 3. Ebene: Fahrten

	ArrayList<ArrayList<ArrayList<SmartmeterValue>>> smartHomeBehavior = new ArrayList<ArrayList<ArrayList<SmartmeterValue>>>();
	// 1. Ebene: Wochentags - Wochenende
	// 2. Ebene: Tag
	// 3. Ebene: Viertelstunde (00:00-23:45)
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_prediction);

		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);

		ll_prediction = (LinearLayout) findViewById(R.id.chart_prediction);
		ll_prediction_balance = (LinearLayout) findViewById(R.id.chart_prediction_balance);
		
		con = this;
		

		drives = new ArrayList<Drives>();
    	
    	mobilityBehavior.add(new ArrayList<ArrayList<Drives>>());
		mobilityBehavior.add(new ArrayList<ArrayList<Drives>>());
		
		for (int i = 0; i < 2; i++)
		{
			ArrayList<ArrayList<Drives>> dayMobility = new ArrayList<ArrayList<Drives>>();
			for (int j = 0; j < 25; j++)
			{
				ArrayList<Drives> hourMobility = new ArrayList<Drives>();
				dayMobility.add(j, hourMobility);
			}
			mobilityBehavior.add(i, dayMobility);
		}
		
		for (int i = 0; i < 2; i++)
		{
			ArrayList<ArrayList<SmartmeterValue>> daySmartHome = new ArrayList<ArrayList<SmartmeterValue>>();
			for (int j = 0; j < 24*4; j++)
			{
				ArrayList<SmartmeterValue> hourSmartHome = new ArrayList<SmartmeterValue>();
				daySmartHome.add(j, hourSmartHome);
			}
			smartHomeBehavior.add(i, daySmartHome);
		}
		
		
		drivesSource = new DrivesDB(this);
		drivesSource.open();

		smarthomeSource = new SmartHomeDB(this);
		smarthomeSource.open();
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Daten laden...");
    	dialog.setMessage("Prädiktion wird erstellt.");
    	dialog.setCanceledOnTouchOutside(false);
		
		spinner = (Spinner) findViewById(R.id.prediction_day_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		adapter = new ArrayAdapter<String>(this,
			    android.R.layout.simple_spinner_item, new ArrayList<String>());
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
	 
	}
	
	public void onResume()
	{
    	super.onResume();
    	
    	if( isOnline() )
    	{
        		
	    	dialog.show();
			
			new Thread(new Runnable() {
				public void run()
				{				
					String s_data = ( (new WeatherHttpClient()).getWeatherData());
					
					if ( s_data != null )
					{
						JSONObject jObj;
						try {
							jObj = new JSONObject(s_data);
							JSONArray jArr = jObj.getJSONArray("list");
							
							for (int i = 0; i < jArr.length()-1; i++)
							{
								try {
									JSONObject jsonWeather = jArr.getJSONObject(i);
									
									WeatherValue weather = new WeatherValue(jsonWeather);
									weatherForecastData.add(weather);
									
									SimpleDateFormat df1 = new SimpleDateFormat("E, dd. MMM yyyy");
									
									if ( !forecastDates.contains(df1.format(weather.getDate())) )
									{
										forecastDates.add(df1.format(weather.getDate()));
									}
									
									
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						boolean checkMobility = fetchMobilityData();
						boolean checkSmartHome = fetchSmartHomeDayData();
						
						if ( !checkMobility || !checkSmartHome )
						{
							Handler handler = new Handler(Looper.getMainLooper());
					    	handler.post(new Runnable()
					    	{
			
								@Override
								public void run()
								{
									dialog.dismiss();
									Toast.makeText(con, "Keine Daten vorhanden.", Toast.LENGTH_LONG).show();
									
								} 
					    	});
						}
						else
						{
							Handler handler = new Handler(Looper.getMainLooper());
					    	handler.post(new Runnable()
					    	{
			
								@Override
								public void run()
								{
									dialog.dismiss();
									
									adapter.clear();
									
									for (String date : forecastDates)
									{
										adapter.add(date);
									}
									
								    adapter.notifyDataSetChanged();			
									
								} 
					    	});
						}
						
						
					}
					else
					{
						Handler handler = new Handler(Looper.getMainLooper());
				    	handler.post(new Runnable()
				    	{
		
							@Override
							public void run()
							{
								dialog.dismiss();
								Toast.makeText(con, "Wetter-Vorhersage konnte nicht geladen werden.", Toast.LENGTH_LONG).show();
								
							} 
				    	});
					}
					
					
					
					
					
					
				}}).start();
    	}
    	else
		{
			Toast.makeText(con, "Keine Internetverbindung", Toast.LENGTH_LONG).show();
		}
    	
	}
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
	{
		SimpleDateFormat df = new SimpleDateFormat("E, dd. MMM yyyy");
		ArrayList<WeatherValue> selWeather = new ArrayList<WeatherValue>();
		
		for (WeatherValue wv : weatherForecastData)
		{            
            if ( forecastDates.get(position).equals(df.format(wv.getDate())) )
            {
            	selWeather.add(wv);
            }
		}

        CombinedXYChart.XYCombinedChartDef[] types1 = new CombinedXYChart.XYCombinedChartDef[] { new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 0),
                new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 1),
                new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 2),
                new CombinedXYChart.XYCombinedChartDef(BarChart.TYPE, 3)}; //
		
//		graph_prediction = (GraphicalView) ChartFactory.getTimeChartView(con, getData(selWeather), getRenderer(), "HH:mm");
		graph_prediction = ChartFactory.getCombinedXYChartView(con, getData(selWeather), getRenderer(),  types1);
		ll_prediction.removeAllViews();
		ll_prediction.addView(graph_prediction);

        CombinedXYChart.XYCombinedChartDef[] types2= new CombinedXYChart.XYCombinedChartDef[] { new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 0),
                new CombinedXYChart.XYCombinedChartDef(LineChart.TYPE, 1)}; //
		
		graph_prediction_balance = ChartFactory.getCombinedXYChartView(con, getDataBalance(selWeather), getRendererBalance(),  types2);
		ll_prediction_balance.removeAllViews();
		ll_prediction_balance.addView(graph_prediction_balance);
		
		
		
//		
//		if (graph_prediction == null)
//		{
//			graph_prediction = (GraphicalView) ChartFactory.getTimeChartView(con, getData(selWeather), getRenderer(), "HH:mm");
//			ll_prediction.addView(graph_prediction);
//		}
//		else
//		{
//			graph_prediction.repaint(); // use this whenever data has changed and you want to redraw
//		}
	}
	
	/*
	 * Graphische Darstellung Prädiktion
	 */
	public XYMultipleSeriesDataset getData(ArrayList<WeatherValue> selWeather)
	{
        // Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset_prediction = new XYMultipleSeriesDataset();
		
	    
		Calendar selCal = Calendar.getInstance();
		selCal.setTime(selWeather.get(0).getDate());
        
        int diffDay = 1;
		// Unterscheidung Wochentags-Wochenende
    	if ( selCal.get(Calendar.DAY_OF_WEEK) == 1 || selCal.get(Calendar.DAY_OF_WEEK) == 7 )
    	{
    		diffDay = 0;
    	}
    	
		
		XYSeries xys_mobilityEE_prediction = new XYSeries("Mobilitätsprädiktion", 0);
        ArrayList<ArrayList<Drives>> dayMobility = mobilityBehavior.get(diffDay);	// // 0: Wochenende; 1: Wochentags
		
		
        for (int i = 0; i < dayMobility.size(); i++)
        {
        	ArrayList<Drives> hourMobility = dayMobility.get(i);
        	
        	double dist = 0.0;
        	double co2 = 0.0;
        	double duration = 0.0;
        	for (int j = 0; j < hourMobility.size(); j++)
        	{
        		dist += hourMobility.get(j).getTrip_length();
        		co2 += hourMobility.get(j).getCo2gTotal();                
                duration += (hourMobility.get(j).getStopDate().getTime() - hourMobility.get(j).getStartDate().getTime());	// duration in [h]
        	}
        	
        	if ( hourMobility.size() > 0)
        	{
            	dist /= hourMobility.size();
        		co2 /= hourMobility.size();
        		duration /= hourMobility.size();
        	}
        	
        	duration =  duration / 1000 / 60 / 60;
        	//System.out.println(duration + " Stunde");
        	
        	double emissionsEnergy = 0.0;
        	double co2_kWh = 550;
        	if ( dist > 0.0 )
        	{
//        		emissionsEnergy = (co2 / dist) * 100 / co2_kWh;
        		emissionsEnergy = dist / 1000 * 14 / 100 / duration;

        	}
        	
        	xys_mobilityEE_prediction.add(i, emissionsEnergy);
        }

        
		XYSeries xys_smarthome_prediction = new XYSeries("Haushalts-Prädiktion", 0);
        ArrayList<ArrayList<SmartmeterValue>> daySmartHome = smartHomeBehavior.get(diffDay);	// // 0: Wochenende; 1: Wochentags

        for (int i = 0; i < daySmartHome.size(); i++)
        {
        	ArrayList<SmartmeterValue> quarterSmarthome = daySmartHome.get(i);
        	
        	Date d = quarterSmarthome.get(0).getDate();
        	
        	Calendar cal = Calendar.getInstance();
            cal.setTime(d);
        	
        	int hour = i / 4;
        	double quarter = (i % 4) * 0.25;
        	double time = hour + quarter;
        	
        	double energy = 0.0;
        	for (SmartmeterValue smv :  quarterSmarthome)
        	{
        		energy += smv.getEnergy();
        	}
        	
        	if ( energy > 0.0 )
        	{
        		energy /= quarterSmarthome.size();
        	}
        	
        	xys_smarthome_prediction.add(time, energy);
        }
	    
		


		XYSeries xys_PV_prediction = new XYSeries("PV-Prädiktion", 0);
		
        for ( WeatherValue weather : selWeather )
        {        	
        	Date d = weather.getDate();
        	
        	Calendar cal = Calendar.getInstance();
            cal.setTime(d);
        	double radiation;
        	
        	if ( cal.get(Calendar.HOUR_OF_DAY) < 9 || cal.get(Calendar.HOUR_OF_DAY) > 15 )
        	{
        		radiation = 0.0;
        	}
        	else
        	{
        		radiation = weather.getRadiation() * Double.parseDouble(prefs.getString("PV_area", "")) * 0.2 / 1000;
        	}
        	
        	
        	xys_PV_prediction.add(cal.get(Calendar.HOUR_OF_DAY), radiation );
        }

		dataset_prediction.addSeries(xys_PV_prediction);
		dataset_prediction.addSeries(xys_smarthome_prediction);
		dataset_prediction.addSeries(xys_PV_prediction);
		dataset_prediction.addSeries(xys_mobilityEE_prediction);

		
	    return dataset_prediction;
	}
	
	public XYMultipleSeriesRenderer getRenderer()
	{ 
		// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
 
        multiRenderer.setXTitle("Uhrzeit");
        multiRenderer.setYTitle("Energieprädiktion in kW");

        // PV Potential (Filled)
        XYSeriesRenderer pvPotentialPredictionRenderer = new XYSeriesRenderer();
        pvPotentialPredictionRenderer.setColor(Color.parseColor("#99cc00"));
        pvPotentialPredictionRenderer.setPointStyle(PointStyle.POINT);
        pvPotentialPredictionRenderer.setFillPoints(true);
        pvPotentialPredictionRenderer.setLineWidth(4);
        pvPotentialPredictionRenderer.setFillBelowLine(true);
        pvPotentialPredictionRenderer.setFillBelowLineColor(Color.argb(175, 153, 204, 0));        
        
        // SmartHome
        XYSeriesRenderer smarthomePredictionRenderer = new XYSeriesRenderer();
        smarthomePredictionRenderer.setColor(Color.BLACK);
        smarthomePredictionRenderer.setPointStyle(PointStyle.POINT);
        smarthomePredictionRenderer.setFillPoints(true);
        smarthomePredictionRenderer.setLineWidth(4);
        smarthomePredictionRenderer.setFillBelowLine(true);
        smarthomePredictionRenderer.setFillBelowLineColor(Color.WHITE);
        
        // Mobility
        XYSeriesRenderer mobilityEEPredictionRenderer = new XYSeriesRenderer();
        mobilityEEPredictionRenderer.setColor(Color.argb(175, 0, 106, 179));
        mobilityEEPredictionRenderer.setPointStyle(PointStyle.POINT);
        mobilityEEPredictionRenderer.setFillPoints(true);
        mobilityEEPredictionRenderer.setLineWidth(4);
        
        
        // PV Line
        XYSeriesRenderer pvLinePredictionRenderer = new XYSeriesRenderer();
        pvLinePredictionRenderer.setColor(Color.parseColor("#99cc00"));
        pvLinePredictionRenderer.setPointStyle(PointStyle.POINT);
        pvLinePredictionRenderer.setFillPoints(true);
        pvLinePredictionRenderer.setLineWidth(4);
        
        

        multiRenderer.addSeriesRenderer(pvPotentialPredictionRenderer);
        multiRenderer.addSeriesRenderer(smarthomePredictionRenderer);
        multiRenderer.addSeriesRenderer(pvLinePredictionRenderer);
        multiRenderer.addSeriesRenderer(mobilityEEPredictionRenderer);
        
		// set some properties on the main renderer
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.parseColor("#edeff1"));
		multiRenderer.setAxisTitleTextSize(25);
		multiRenderer.setLabelsTextSize(25);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsPadding(5);
		multiRenderer.setShowLegend(false);
		multiRenderer.setLegendTextSize(25);
		multiRenderer.setFitLegend(true);
		multiRenderer.setMargins(new int[] { 20, 75, 50, 15 });  // top, left, bottom, right
//		multiRenderer.setYAxisAlign(Align.RIGHT, 1); 
		multiRenderer.setYLabelsAlign(Align.RIGHT);
		multiRenderer.setYLabels(10);

		
//		multiRenderer.setYLabelsColor(0, Color.parseColor("#006ab3"));
//		multiRenderer.setYLabelsColor(1, Color.parseColor("#99cc00"));

		multiRenderer.setYLabelsColor(0, Color.BLACK);
		
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setPointSize(5);
		multiRenderer.setShowLabels(true);
		multiRenderer.setPanEnabled(false,false);
		multiRenderer.setZoomEnabled(false, false);

		multiRenderer.setBarSpacing(0.5);
		
		for (int i = 0; i < 24; i++) 
	    { 
			if ( i % 2 == 0 )
				multiRenderer.addXTextLabel(i, "");
			else
				multiRenderer.addXTextLabel(i, "" + i + ":00");
				
	    }
		multiRenderer.setXLabelsAlign(Align.CENTER);
		multiRenderer.setXLabelsAngle(45);
		multiRenderer.setXLabels(0);
		multiRenderer.setXLabelsPadding(25);
		
		multiRenderer.setXAxisMax(24);
//		multiRenderer.setXAxisMin(0);
		
		
    	
        return multiRenderer;
	}
	
	
	
	/*
	 * Grafische Darstellung Potential
	 */
	public XYMultipleSeriesDataset getDataBalance(ArrayList<WeatherValue> selWeather)
	{
        // Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset_prediction = new XYMultipleSeriesDataset();
		
	    
		Calendar selCal = Calendar.getInstance();
		selCal.setTime(selWeather.get(0).getDate());
        
        int diffDay = 1;
		// Unterscheidung Wochentags-Wochenende
    	if ( selCal.get(Calendar.DAY_OF_WEEK) == 1 || selCal.get(Calendar.DAY_OF_WEEK) == 7 )
    	{
    		diffDay = 0;
    	}
    	
		
        ArrayList<ArrayList<Drives>> dayMobility = mobilityBehavior.get(diffDay);	// // 0: Wochenende; 1: Wochentags
		
    	ArrayList<Double [][]> energyBalance = new ArrayList<Double[][]>();
    	
        for (int i = 0; i < dayMobility.size(); i++)
        {
        	ArrayList<Drives> hourMobility = dayMobility.get(i);
        	
        	double dist = 0.0;
        	double co2 = 0.0;
        	double duration = 0.0;
        	for (int j = 0; j < hourMobility.size(); j++)
        	{
        		dist += hourMobility.get(j).getTrip_length();
        		co2 += hourMobility.get(j).getCo2gTotal();                
                duration += (hourMobility.get(j).getStopDate().getTime() - hourMobility.get(j).getStartDate().getTime());	// duration in [h]
        	}
        	
        	if ( hourMobility.size() > 0)
        	{
            	dist /= hourMobility.size();
        		co2 /= hourMobility.size();
        		duration /= hourMobility.size();
        	}
        	
        	duration =  duration / 1000 / 60 / 60;
        	
        	double emissionsEnergy =0.0;
        	if ( dist > 0.0 )
        	{
//        		emissionsEnergy = (co2 / dist) * 100 / co2_kWh;
        		emissionsEnergy = dist / 1000 * 14 / 100 / duration;

        	}
        	
        	Double[][] en_start = new Double[1][2];
        	// Start
            en_start[0][0] = (double) i;					// Startzeit
            en_start[0][1] = -emissionsEnergy;	// Wert

            
        	energyBalance.add(en_start);
//        	emissions.add(em_end);
        	
        }
        
        ArrayList<ArrayList<SmartmeterValue>> daySmartHome = smartHomeBehavior.get(diffDay);	// // 0: Wochenende; 1: Wochentags

        for (int i = 0; i < daySmartHome.size(); i++)
        {
        	ArrayList<SmartmeterValue> quarterSmarthome = daySmartHome.get(i);
        	
        	Date d = quarterSmarthome.get(0).getDate();
        	
        	Calendar cal = Calendar.getInstance();
            cal.setTime(d);
        	
        	int hour = i / 4;
        	double quarter = (i % 4) * 0.25;
        	double time = hour + quarter;
        	
        	double energy = 0.0;
        	for (SmartmeterValue smv :  quarterSmarthome)
        	{
        		energy += smv.getEnergy();
        	}
        	
        	if ( energy > 0.0 )
        	{
        		energy /= quarterSmarthome.size();
        	}
        	
        	Double[][] en_start = new Double[1][2];
        	// Start
            en_start[0][0] = time;					// Startzeit
            en_start[0][1] = -energy;	// Wert

            
//        	energyBalance.add(en_start);
        	
//        	xys_smarthome_prediction.add(time, energy);
        }
	    
		
        for ( WeatherValue weather : selWeather )
        {        	
        	Date d = weather.getDate();
        	
        	Calendar cal = Calendar.getInstance();
            cal.setTime(d);
        	double radiation;
        	
        	if ( cal.get(Calendar.HOUR_OF_DAY) < 9 || cal.get(Calendar.HOUR_OF_DAY) > 15 )
        	{
        		radiation = 0.0;
        	}
        	else
        	{
        		radiation = weather.getRadiation() * Double.parseDouble(prefs.getString("PV_area", "")) * 0.2 / 1000;
        	}
        	
        	Double[][] en_start = new Double[1][2];
        	// Start
            en_start[0][0] = (double) cal.get(Calendar.HOUR_OF_DAY);					// Startzeit
            en_start[0][1] = radiation;	// Wert

            
        	energyBalance.add(en_start);
        	
        	
//        	xys_PV_prediction.add(cal.get(Calendar.HOUR_OF_DAY), radiation );
        }
        
        Collections.sort(energyBalance, new Comparator<Double[][]>() {
            @Override
            public int compare(Double[][] v1, Double[][] v2)
            {

                return  v1[0][0].compareTo(v2[0][0]);
            }
        });

		XYSeries xys_Balance_prediction = new XYSeries("Bilanz-Prädiktion", 0);

        double lastValue = 0.0;
        for (int i = 0; i < energyBalance.size(); i++)
        {
        	double currentValue = lastValue + energyBalance.get(i)[0][1];
        	xys_Balance_prediction.add(energyBalance.get(i)[0][0], currentValue);
        	

        	lastValue = currentValue;
        }


		XYSeries xys_Zero = new XYSeries("Nulllinie", 0);
		xys_Zero.add(0, 0);
		xys_Zero.add(energyBalance.get(energyBalance.size()-1)[0][0], 0);

		dataset_prediction.addSeries(xys_Balance_prediction);
//		dataset_prediction.addSeries(xys_smarthome_prediction);
//		dataset_prediction.addSeries(xys_PV_prediction);
		dataset_prediction.addSeries(xys_Zero);
		
	    return dataset_prediction;
	}
	
	public XYMultipleSeriesRenderer getRendererBalance()
	{ 
		// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
 
        multiRenderer.setXTitle("Uhrzeit");
        multiRenderer.setYTitle("Bilanzverlauf");

        XYSeriesRenderer balancePredictionRenderer = new XYSeriesRenderer();
        balancePredictionRenderer.setColor(Color.parseColor("#99cc00"));
        balancePredictionRenderer.setPointStyle(PointStyle.POINT);
        balancePredictionRenderer.setFillPoints(true);
        balancePredictionRenderer.setLineWidth(4);
        balancePredictionRenderer.setFillBelowLine(true);
        balancePredictionRenderer.setFillBelowLineColor(Color.argb(175, 153, 204, 0));        

        
        // Nulllinie
        XYSeriesRenderer baselineRenderer = new XYSeriesRenderer();
        baselineRenderer.setColor(Color.parseColor("#b9102d"));
        baselineRenderer.setPointStyle(PointStyle.POINT);
        baselineRenderer.setFillPoints(true);
        baselineRenderer.setLineWidth(4);
        
        

        multiRenderer.addSeriesRenderer(balancePredictionRenderer);
//        multiRenderer.addSeriesRenderer(smarthomePredictionRenderer);
//        multiRenderer.addSeriesRenderer(pvLinePredictionRenderer);
        multiRenderer.addSeriesRenderer(baselineRenderer);
        
		// set some properties on the main renderer
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.WHITE);
		multiRenderer.setMarginsColor(Color.parseColor("#edeff1"));
		multiRenderer.setAxisTitleTextSize(25);
		multiRenderer.setLabelsTextSize(25);
		multiRenderer.setLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsPadding(5);
		multiRenderer.setShowLegend(false);
		multiRenderer.setLegendTextSize(25);
		multiRenderer.setFitLegend(true);
		multiRenderer.setMargins(new int[] { 20, 75, 50, 15 });  // top, left, bottom, right
//		multiRenderer.setYAxisAlign(Align.RIGHT, 1); 
		multiRenderer.setYLabelsAlign(Align.RIGHT);

		
//		multiRenderer.setYLabelsColor(0, Color.parseColor("#006ab3"));
//		multiRenderer.setYLabelsColor(1, Color.parseColor("#99cc00"));

		multiRenderer.setYLabelsColor(0, Color.BLACK);
		
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setPointSize(5);
		multiRenderer.setShowLabels(true);
		multiRenderer.setPanEnabled(false,false);
		multiRenderer.setZoomEnabled(false, false);

		multiRenderer.setBarSpacing(0.5);
		
		for (int i = 0; i < 24; i++) 
	    { 
			if ( i % 2 == 0 )
				multiRenderer.addXTextLabel(i, "");
			else
				multiRenderer.addXTextLabel(i, "" + i + ":00");
				
	    }
		multiRenderer.setXLabelsAlign(Align.CENTER);
		multiRenderer.setXLabelsAngle(45);
		multiRenderer.setXLabels(0);
		multiRenderer.setXLabelsPadding(25);
		
		multiRenderer.setXAxisMax(24);
//		multiRenderer.setXAxisMin(0);
		
		
    	
        return multiRenderer;
	}
	
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.survey, menu);
		return true;
	}
	
	private boolean fetchSmartHomeDayData()
	{	    	    
		ArrayList<SmartmeterValue> smv_list = smarthomeSource.getAllValues();
		
		for (SmartmeterValue smv: smv_list)
		{
			Date d = smv.getDate();
        	
        	// fetch data for time-distribution
        	Calendar cal = Calendar.getInstance();
        	cal.setTime(d);
        	
        	int hour = cal.get(Calendar.HOUR_OF_DAY);
        	int quarter = cal.get(Calendar.MINUTE) / 15;
        	
        	int idx = hour * 4 + quarter * 1;
        	
        	int diffDay = 1;
    		// Unterscheidung Wochentags-Wochenende
        	if ( cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7 )
        	{
        		diffDay = 0;
        	}

            // 0: Wochenende; 1: Wochentags
        	
        	smartHomeBehavior.get(diffDay).get(idx).add(smv);
			
		}
		
		if ( smv_list.isEmpty() )
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}


	
	
	private boolean fetchMobilityData()
	{
		drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));
		
    	if ( drives.isEmpty() )
    	{
    		Handler handler = new Handler(Looper.getMainLooper());
	    	handler.post(new Runnable()
	    	{

				@Override
				public void run()
				{
		    		Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
				} 
	    	});
	    	return false;
    	}
    	else
    	{
    		Collections.sort(drives, Collections.reverseOrder());
        	
    		for (Drives d : drives)
    		{	
    			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
    			Date start_Date = new Date();;
    	        try
    	        {
    	        	start_Date = simpleDateFormat.parse(d.getStart_time());
    	        }
    	        catch (ParseException ex)
    	        {
    	            System.out.println("Exception "+ex);
    	        }
	        	
	        	// fetch data for time-distribution
	        	Calendar cal = Calendar.getInstance();
	        	cal.setTime(start_Date);
	        	
	        	int hour = cal.get(Calendar.HOUR_OF_DAY);
	        	hour_count[hour]++;
	        	
	        	int day = cal.get(Calendar.DAY_OF_WEEK); //(1 = Sonntag, 7 = Samstag)
	        	day_count[day-1]++;
	        	
	        	// Unterscheidung Wochentags-Wochenende
	        	if ( cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7 )
	        	{
	        		ArrayList<ArrayList<Drives>> dayMobility = mobilityBehavior.get(0);	// 0: Wochenende
	        		ArrayList<Drives> hourMobility = dayMobility.get(hour);
	        		hourMobility.add(d);
	        		dayMobility.set(hour, hourMobility);
		        	mobilityBehavior.set(0, dayMobility);
		        	

		        	mobility_count[0][hour]++;
	        	}
	        	else
	        	{
	        		ArrayList<ArrayList<Drives>> dayMobility = mobilityBehavior.get(1);	// 1: Wochentags
	        		ArrayList<Drives> hourMobility = dayMobility.get(hour);
	        		hourMobility.add(d);
	        		dayMobility.set(hour, hourMobility);
		        	mobilityBehavior.set(1, dayMobility);
		        	

		        	mobility_count[1][hour]++;
	        	}

//    			ArrayList<Drives> dayMobility = mobilityBehavior.get(day-1);
//    			dayMobility.add(d);
//	        	mobilityBehavior.set(day-1, dayMobility);
	        	

    		}
    		
    		return true;
    	}			  
	}

	public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

	    if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	

}

class WeatherHttpClient {
	// http://openweathermap.org
    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?id=2922530";
 
    
     
    public String getWeatherData() {
        HttpURLConnection con = null ;
        InputStream is = null;
 
        try {
            con = (HttpURLConnection) ( new URL(BASE_URL)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();
             
            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");
             
            is.close();
            con.disconnect();
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
 
        return null;
                 
    }
}


