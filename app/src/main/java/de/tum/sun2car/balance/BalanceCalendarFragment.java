package de.tum.sun2car.balance;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.SmartmeterValue;

public class BalanceCalendarFragment extends Fragment
{
	LinearLayout ll_chart;
	ArrayList<ArrayList<SmartmeterValue>> smvData;
	
	// type = 0: nur Zeitverteiluing
	// type = 1: Zeitverteilung mit PV
	int type = 0;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.layout_balance_tracks, container, false);
    }
    
    public static BalanceCalendarFragment newInstance(int t) 
    {
        BalanceCalendarFragment cf = new BalanceCalendarFragment();
        cf.type = t;
        return cf;
    }
    
    public static BalanceCalendarFragment newInstance(int t, ArrayList<ArrayList<SmartmeterValue>> smvData) 
    {
        BalanceCalendarFragment cf = new BalanceCalendarFragment();
        cf.type = t;
        cf.smvData = smvData;
        return cf;
    }

	/**
	 * 
	 */
    int savedYear;
    int savedMonth;
    int savedDay;
    boolean isSavedDate = false;
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) 
	{
		super.onActivityCreated(savedInstanceState);
	}

	
	GridView calView;
	public GregorianCalendar month;// calendar instances.

	public BalanceSmartMeterCalendarAdapter cal_adapter;// adapter instance
	public ArrayList<String> items; // container to store calendar items which
	// needs showing the event marker
	ArrayList<String> event;
	ArrayList<String> date;
	ArrayList<String> desc;

	@Override
	public void onResume() {
		super.onResume();
		
		// get savedCalView, if existing
    	if ( getActivity().getIntent().hasExtra("savedCal") )
    	{
    		ArrayList<Integer> date = new ArrayList<Integer>();
    		
    		date = getActivity().getIntent().getIntegerArrayListExtra("savedDate");
    		
//    		if ( b.containsKey("isSavedDate") )
//    		{
//    			
//    		}
    		
    		savedYear = date.get(0);
		    savedMonth = date.get(1);
		    savedDay = date.get(2);
		    
		    month = new GregorianCalendar(savedYear, savedMonth, savedDay);
    	}
		else
		{
			month = (GregorianCalendar) GregorianCalendar.getInstance();			
		}

		items = new ArrayList<String>();
		event = new ArrayList<String>();

		calView = (GridView) getActivity().findViewById(R.id.gridview);

//		calHandler = new Handler();

		TextView title = (TextView) getActivity().findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		RelativeLayout previous = (RelativeLayout) getActivity().findViewById(R.id.previous);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
			}
		});

		RelativeLayout next = (RelativeLayout) getActivity().findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();

			}
		});
		
		cal_adapter = new BalanceSmartMeterCalendarAdapter(getActivity(), month, smvData);
		
		calView.setAdapter(cal_adapter);
		
		
		calView.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView parent, View v, int position, long id) 
			{     								
				String selectedGridDate = BalanceSmartMeterCalendarAdapter.dayString
						.get(position);
				
				String[] separatedTime = selectedGridDate.split("-");
				
				((BalanceSmartMeterCalendarAdapter) parent.getAdapter()).setSelected(v);
				
				
				String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking last part of date. ie; 2 from 2012-12-02.
				int gridvalue = Integer.parseInt(gridvalueString);
				// navigate to next or previous month on clicking offdays.
				if ((gridvalue > 10) && (position < 8)) 
				{
					setPreviousMonth();
					refreshCalendar();
				} 
				else if ((gridvalue < 7) && (position > 28)) 
				{
					setNextMonth();
					refreshCalendar();
				}
				
				((BalanceSmartMeterCalendarAdapter) parent.getAdapter()).setSelected(v);
				

				int yearSel = Integer.parseInt(separatedTime[0]);
				int monthSel = Integer.parseInt(separatedTime[1])-1;
				int daySel = Integer.parseInt(separatedTime[2]);
				
				
                Intent intent = new Intent(getActivity(), BalanceSmartMeterDayView.class);

				Bundle b = new Bundle();
				b.putInt("year", yearSel);
	            b.putInt("month", monthSel);
	            b.putInt("day", daySel);

				intent.putExtras(b);
//				
				
				startActivity(intent);

			}

		});
	}
	

	public void cleanUpAfterTaskExecution() {
		// TODO Auto-generated method stub

	}
	
	
	/**
	 * Calendar
	 */
	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}

	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}

	}
	
	public void refreshCalendar() {
		TextView title = (TextView) getActivity().findViewById(R.id.title);

		cal_adapter.refreshDays();
		cal_adapter.notifyDataSetChanged();
//		calHandler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}



}
