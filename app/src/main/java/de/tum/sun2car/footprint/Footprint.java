package de.tum.sun2car.footprint;

import de.tum.sun2car.R;
import de.tum.sun2car.R.layout;
import de.tum.sun2car.R.menu;
import de.tum.sun2car.R.string;
import de.tum.sun2car.balance.BalancePurposes;
import de.tum.sun2car.balance.BalanceTracks;
import de.tum.sun2car.helper.SyncSun2CarService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

public class Footprint extends TabActivity
{
	private TabHost tabHost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_footprint);
		
		
		tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    /**
	     * Tab FootprintPerson
	     */
	    // set Tab Text
		final TextView tvPerson = new TextView(this);
		tvPerson.setText(R.string.footprint_person);
		tvPerson.setTextColor(Color.WHITE);
		tvPerson.setTextSize(18);
		tvPerson.setPadding(2, 20, 2, 20);
		tvPerson.setGravity(25);

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, FootprintPerson.class);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("person").setIndicator(tvPerson)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    /**
	     * Tab FootprintFamily
	     */
	    // set Tab Text
	    final TextView tvFamily = new TextView(this);
	    tvFamily.setText(R.string.footprint_family);
	    tvFamily.setTextColor(Color.parseColor("#006ab3"));
	    tvFamily.setTextSize(18);
	    tvFamily.setPadding(2, 20, 2, 20);
	    tvFamily.setGravity(25);
		
	    intent = new Intent().setClass(this, FootprintFamily.class);
	    spec = tabHost.newTabSpec("family").setIndicator(tvFamily)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(0);
	    
	    // current tab: blue, other: white
	    for(int j = 0; j < tabHost.getTabWidget().getChildCount(); j++)
	    {
	        tabHost.getTabWidget().getChildAt(j).setBackgroundColor(Color.WHITE);
	    }
	    tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
    
	    
	    
	    tabHost.setOnTabChangedListener(new OnTabChangeListener()
			{
			  public void onTabChanged(String arg0)
			   	{
				   for(int i = 0; i < tabHost.getTabWidget().getChildCount(); i++)
				    {
				        tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
				    }
		        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
		        if (tabHost.getCurrentTab()==0)
		        	{
		        		tvPerson.setTextColor(Color.WHITE);
		        		tvFamily.setTextColor(Color.parseColor("#006ab3"));
		        	}
		        else
		        {
	        		tvPerson.setTextColor(Color.parseColor("#006ab3"));
	        		tvFamily.setTextColor(Color.WHITE);
	        	}
			   	}     
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.footprint, menu);
		return true;
	}
	


}
