package de.tum.sun2car.tumsensorrecorderlib;

import java.io.File;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class UploadService extends Service {
	
	private static Boolean mIsRunning = false;
	private static QueueManager mQueueManager;
	private static ServerManager mServerManager;
	private static Thread mUploadThread;
	
	private  Context con = this;
	
	@Override
	public void onCreate() {
		mServerManager = ServerManager.getInstance(this);
		mQueueManager = QueueManager.getInstance();
		mIsRunning = false;
		
		// also start here, because of an android-bug (at least in android 2.3) onStartCommand is not triggered after onCreate if service is restarted
		startProcessing();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
    
    public static boolean getIsRunning() {
    		return mIsRunning;
    }
    
	@Override
	public void onDestroy() {
		//Toast.makeText(this, "Upload Service stopped", Toast.LENGTH_LONG).show();
		mIsRunning = false;
		
		stopForeground(true);
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Toast.makeText(this, "Upload Service started", Toast.LENGTH_LONG).show();
		
		startProcessing();
		
		
    	
		return START_STICKY;
	}
	
	private void startProcessing() {
		if(!mIsRunning) {
			mIsRunning = true;
			
			processUploadQueue();
	    	
	    	Notification n = new Notification();
	    	startForeground(2, n);
		}
	}
	
	
	private void processUploadQueue() {
		
		mUploadThread = new Thread (new Runnable() {
			public void run() {
				
				while(mIsRunning) {
					try {
						boolean continueWithUpload = true;
						
						mServerManager = ServerManager.getInstance(con);
						
						if(mServerManager.isNetworkAvailable()) {
							//String filename = mFileSendQueue.pollLast(5000,TimeUnit.MILLISECONDS);
							if (mQueueManager == null) {
								mQueueManager = QueueManager.getInstance();
							}
							String filename = mQueueManager.getLastQueueElement();
							if(filename != null) {
								
								if(new File(filename).exists()) {
																	
									while(!mServerManager.isTokenValid()) {
										
										ServerManager.LoginState loginState = mServerManager.createNewToken();
										if(loginState == ServerManager.LoginState.NoConnectivity || loginState == ServerManager.LoginState.ServerError || loginState == ServerManager.LoginState.UnknownError ) {
											
											if ((!mServerManager.isNetworkAvailable()) || (mIsRunning == false)) { // stop infinite-loop if network goes away
												continueWithUpload = false;
												break;
											}
											
											Thread.sleep(1000); // if request fails wait a bit until next request
										} else if(loginState == ServerManager.LoginState.WrongLoginOrPassword) {
											Log.i("UploadService","invalid-request-error");
											//TODO: what todo if password is wrong? makes no sence to send request again
										}
									}
									
									if(continueWithUpload && mIsRunning) {
										Log.i("start-upload",filename);
										uploadFile(filename);
										Log.i("finished-upload",filename);

									} else {
										// put file back to queue if it was not uploaded
										mQueueManager.putFileBackToQueue(filename);
									}
								} else {
									Log.i("file not found","file doesn't exist: "+filename);
									
								}
							} else {
								Thread.sleep(5000);  // wait 5 secs until checking for new files again
							}
								
							
						} else {
							Thread.sleep(5000);  // wait 5 secs until checking for network-connection again
						}
								
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		mUploadThread.start();
	}
	
	
	private void uploadFile(String fileName) {
		File file = new File(fileName);
		
		ServerManager.UploadState uploadState = mServerManager.uploadFile(fileName);
		if(uploadState == ServerManager.UploadState.Success) {
			file.delete();
		} else {
			if (mQueueManager == null) {
				mQueueManager = QueueManager.getInstance();
			}
			mQueueManager.putFileBackToQueue(fileName);
		}
	}
	
}
