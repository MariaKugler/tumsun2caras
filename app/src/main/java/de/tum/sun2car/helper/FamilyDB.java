package de.tum.sun2car.helper;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FamilyDB
{
	private static final int VERSION_FamilyDB = 2;
	private static final String NOM_FamilyDB = "sun2car.db";
 
	private static final String TABLE_FAMILY = "table_familymembers";

	public static final String COL_FAMILY_ID = "FAMILY_ID";
	private static final int NUM_COL_ID = 0;
	public static final String COL_FAMILY_USERID = "USER_ID";
	private static final int NUM_COL_USERID = 1;
	public static final String COL_FAMILY_DRIVER = "DRIVER";
	private static final int NUM_COL_DRIVER = 2;

	private String[] allColumns = {SqliteDB.COL_FAMILY_ID,
									SqliteDB.COL_FAMILY_USERID, 
									SqliteDB.COL_FAMILY_DRIVER};
	
	private SQLiteDatabase familydb;
 
	private SqliteDB myBaseSQLite;
 
	public FamilyDB(Context context){
		//Erzeugung der DB inkl. Tabellen
		myBaseSQLite = new SqliteDB(context, NOM_FamilyDB, null, VERSION_FamilyDB);
	}
 
	public void open(){
		//DB im Schreib-Modus öffnen
		familydb = myBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//Datenbank schliessen
		familydb.close();
	}
 
	public SQLiteDatabase getDrivesDB(){
		return familydb;
	} 
	
	public long addMember(String userid, int isDriver, String familyid)
	{		
		//Erzeugung einer ContentValues 
		ContentValues values = new ContentValues();
		
		//Einfügen eines Element als key-value-Paar (Spalte-Wert-Paar)
		values.put(COL_FAMILY_USERID, userid);
		values.put(COL_FAMILY_DRIVER, isDriver);
		values.put(COL_FAMILY_ID, familyid);
//		values.put(COL_COUNT, s.getCount());

		//Ein Objekt wird in bdd via ContentValues eingefügt
		return familydb.insert(TABLE_FAMILY, null, values);
	} 
		
	
 	public int removeMember(double userid)
 	{		
		return familydb.delete(TABLE_FAMILY, COL_FAMILY_USERID + " = \"" + userid + "\"", null);
	}
 	
 	public boolean checkForMember(String userid)
 	{		
 		Cursor c = familydb.query(TABLE_FAMILY, allColumns, COL_FAMILY_USERID + "=\"" + userid + "\"", null, null, null, null);
 		
 		boolean result = c.moveToFirst();
 		c.close();
 		
		return result;
		// true if already there
		// false if not there
	}
 	 	
 	
	public List<String> getAllMembers(String familyid)
	{
		List<String> members = new ArrayList<String>();
		
		Cursor c = familydb.query(TABLE_FAMILY, allColumns, COL_FAMILY_ID + "=\"" + familyid + "\"", null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast())
		{
			String d = String.format("%.0f", cursorToUser(c));
			
			members.add(d);
			c.moveToNext();
		}
		
		c.close();
		
		return members;
	}
	
	public int isDriver(double userid)
	{
		Cursor c = familydb.query(TABLE_FAMILY, allColumns, COL_FAMILY_USERID + "=\"" + userid + "\"", null, null, null, null);
		c.moveToFirst();

		int result = c.getInt(NUM_COL_DRIVER);
 		c.close();
		
		return result;
	}
	
	private Double cursorToUser(Cursor c)
	{		
		if (c.getCount() == 0)
			return null;
		
		Double result = c.getDouble(NUM_COL_USERID);
 
		return result;
	}
}
