package de.tum.sun2car.balance;

import java.util.GregorianCalendar;

public class CSVBase {
	GregorianCalendar date;
	float energy;
	String csvName;
	
	public CSVBase(GregorianCalendar date, float energy, String csvName){
		this.date = date;
		this.energy = energy;
		this.csvName = csvName;
	}
	
	public GregorianCalendar getDate() {
		return date;
	}

	public float getEnergy() {
		return energy;
	}

	public String getCsvName() {
		return csvName;
	}
}


