package de.tum.sun2car.info;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import de.tum.sun2car.LoginActivity;
import de.tum.sun2car.R;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;
import de.tum.sun2car.tumsensorrecorderlib.SensorService;
import de.tum.sun2car.tumsensorrecorderlib.ServerManager;
import de.tum.sun2car.tumsensorrecorderlib.UploadService;

@SuppressWarnings("deprecation")
public class Info extends TabActivity {
	
	private ImageButton btn_logout;
	private TabHost tabHost;
	
    private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_info);

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		
		btn_logout = (ImageButton) findViewById(R.id.btn_logout);
		
	    tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    /**
	     * Tab InfoAccount
	     */
	    // set Tab Text
		final TextView tvAccount = new TextView(this);
		tvAccount.setText(R.string.info_account);
		tvAccount.setTextColor(Color.WHITE);
		tvAccount.setTextSize(18);
		tvAccount.setGravity(25);

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, InfoAccount.class);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("account").setIndicator(tvAccount)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    /**
	     * Tab InfoProject
	     */
	    // set Tab Text
	    final TextView tvProject = new TextView(this);
	    tvProject.setText(R.string.info_project);
	    tvProject.setTextColor(Color.parseColor("#006ab3"));
	    tvProject.setTextSize(18);
	    tvProject.setGravity(25);
		
	    intent = new Intent().setClass(this, InfoProject.class);
	    spec = tabHost.newTabSpec("project").setIndicator(tvProject)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(0);
	    
	    // current tab: blue, other: white
	    for(int j = 0; j < tabHost.getTabWidget().getChildCount(); j++)
	    {
	        tabHost.getTabWidget().getChildAt(j).setBackgroundColor(Color.WHITE);
	    }
	    tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
    
	    
	    
	    tabHost.setOnTabChangedListener(new OnTabChangeListener()
			{
			  public void onTabChanged(String arg0)
			   	{
				   for(int i = 0; i < tabHost.getTabWidget().getChildCount(); i++)
				    {
				        tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
				    }
		        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
		        if (tabHost.getCurrentTab()==0)
		        	{
		        		tvAccount.setTextColor(Color.WHITE);
		        		tvProject.setTextColor(Color.parseColor("#006ab3"));
		        	}
		        else
		        {
	        		tvAccount.setTextColor(Color.parseColor("#006ab3"));
	        		tvProject.setTextColor(Color.WHITE);
	        	}
			   	}     
			});
	    
	    
	}
	
	public void onLogout(View v)
	{
		logout();
	}
	
	private void logout() {
		clearSharedPreferences();
    	
    	if(SensorService.getIsRunning()) {
    		stopService(new Intent(this, SensorService.class));
    	}
    	
    	if(UploadService.getIsRunning()) {
    		stopService(new Intent(this, UploadService.class));
    	}
    	
    	ServerManager.getInstance(this).setLoginData("","");
    	
   		Intent intent = new Intent(this, LoginActivity.class);
   		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
   		
    	startActivity(intent);
    	this.finish();
	}
	
	public void clearSharedPreferences(){
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(Preferences.cLastLoginStatus);
		editor.remove(Preferences.cPass);
        editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

}
