package de.tum.sun2car.logbook;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import de.tum.sun2car.R;

public class LogbookChartFragment extends Fragment
{
	LinearLayout ll_chart;
	int[] hour_count = new int[24];
	int[] day_count = new int[7];
	
	// type = 0: nur Zeitverteiluing
	// type = 1: Zeitverteilung mit PV
	int type = 0;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
    	return inflater.inflate(R.layout.layout_logbook_stat_graph, container, false);
    }
    
    public static LogbookChartFragment newInstance(int[] h_count, int[] d_count, int t) 
    {
        LogbookChartFragment cf = new LogbookChartFragment();
        cf.hour_count = h_count;
        cf.day_count = d_count;
        cf.type = t;
        return cf;
    }

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Intent intent = getActivity().getIntent();
	}

	@Override
	public void onResume() {
		super.onResume();

		
		if ( type == 0 )
		{
			drawBarTimeChart();
		}
		else if ( type == 1 )
		{
			drawBarTimePVChart();
		}
	}
	
	public void drawBarTimeChart()
	{
	    GraphicalView chartView;
	    
		XYMultipleSeriesRenderer renderer = getXYMultRenderer(hour_count);
		//graph = ChartFactory.getPieChartView(con, getCatSeries(list, which), getDefaultRenderer(which));
		chartView = ChartFactory.getBarChartView(getActivity(), getXYMultData(hour_count), renderer, Type.DEFAULT);
		
		LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.logbook_stat_chart);	    
	    //layout.removeAllViews();
	    layout.addView(chartView);
	}
	
	public void drawBarTimePVChart()
	{
		GraphicalView chartView;
	}
	
	// Helper for Graphics
	public XYMultipleSeriesDataset getXYMultData(int[] h_c)
	{
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
	    XYSeries dataSeries = new XYSeries("series");
	    
		for (int i = 0; i < h_c.length; i++)
		{
			dataSeries.add(i, h_c[i]);
		}
	    
		dataset.addSeries(dataSeries);
		
	    return dataset;
	}
	
	public XYMultipleSeriesRenderer getXYMultRenderer(int[] h_c)
	{    
	    XYSeriesRenderer r = new XYSeriesRenderer();
	    r.setColor(Color.parseColor("#006ab3"));
        r.setLineWidth(1);        
        
//	        r.setPointStyle(PointStyle.SQUARE); // CIRCLE, DIAMOND , POINT, TRIANGLE, X
//	        r.setFillPoints(true); // not for point or x
        // don't know how to set point size or point color
//	        r.setFillBelowLine(true);
//	        r.setFillBelowLineColor(Color.WHITE);

    	XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer(); 
    	renderer.addSeriesRenderer(r);

		// set some properties on the main renderer
		renderer.setApplyBackgroundColor(false);
		renderer.setBackgroundColor(Color.WHITE);
//			renderer_speed.setChartTitleTextSize(20);
		renderer.setBarSpacing(0.5);
		renderer.setXAxisMin(0);
		renderer.setXAxisMax(24);
//			renderer_speed.setLegendTextSize(15);
		renderer.setMargins(new int[] { 0, 10, 25, 10 });
//			renderer_speed.setZoomButtonsVisible(true);
		renderer.setPointSize(5);
		renderer.setShowLabels(true);
		renderer.setLabelsTextSize(30);
		renderer.setLabelsColor(Color.parseColor("#006ab3"));
		renderer.setYLabels(0);
		renderer.setShowLegend(false);
		renderer.setPanEnabled(false, false);
		renderer.setZoomEnabled(false, false);
		renderer.setXTitle("\n\n Tageszeit");
		renderer.setXLabelsColor(Color.parseColor("#006ab3"));
		renderer.setAxisTitleTextSize(35);
		renderer.setMarginsColor(Color.parseColor("#edeff1"));
		
		for (int i = 0; i < 24; i++) 
	    { 
			if ( i % 2 == 0 )
				renderer.addXTextLabel(i, "");
			else
				renderer.addXTextLabel(i, "" + i + ":00");
				
	    }
		    renderer.setXLabelsAlign(Align.CENTER);
		    renderer.setXLabelsAngle(45);
		    renderer.setXLabels(0);
		    renderer.setXLabelsPadding(5);
		
		
//		for (int i = 0; i < h_c.length; i++)
//		{
//			renderer.addXTextLabel(h_c[i], "" + i);
//		}
    	
        return renderer;
	}


	public void cleanUpAfterTaskExecution() {
		// TODO Auto-generated method stub

	}


}
