package de.tum.sun2car.classes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SmartmeterValue implements Comparable<SmartmeterValue>
{
	private Date date;
	private String time;
	private Double energy;
	
	public SmartmeterValue()
	{
		
	}
	
	public SmartmeterValue(Date date, Double energy)
	{
		this.date = date;
		this.energy = energy;
		
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date);
		
		this.time = strDate;
        
	}


	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date) 
	{
		this.date = date;
		
		DateFormat sdf;
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date);
		
		this.time = strDate;
		
	}
	
	public String getTime() 
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // Sun Nov 04 17:26:49 MEZ 2012 --> EEE MMM dd HH:mm:ss zzz yyyy
        try
        {
        	this.date = simpleDateFormat.parse(time);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
	}

	public Double getEnergy() 
	{
		return energy;
	}

	public void setEnergy(Double energy) 
	{
		this.energy = energy;
	}


	@Override
	public int compareTo(SmartmeterValue another)
	{
		return  date.compareTo(another.date);
	}


}
