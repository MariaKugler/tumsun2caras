package de.tum.sun2car;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.de;

import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class LoginActivity extends Activity implements OnClickListener {

  private ServerHelper ServerHelper;

  private EditText email;
  private EditText password;
  private ImageButton reg;

  private ProgressDialog dialog;
  private AlertDialog alert;
  private Handler mHandler;

  private boolean mLoginStatus = false;

  public Context con;
  private SharedPreferences settings;
  private SharedPreferences prefs;

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_login);

    reg = (ImageButton) findViewById(R.id.button_reg);
    email = (EditText) findViewById(R.id.editText1_Email);
    password = (EditText) findViewById(R.id.editText2_Password);

    con = this;

    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);

    dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setTitle("Anmeldung");
    dialog.setMessage("Anmelden am Server...");

    mLoginStatus = settings.getBoolean(Preferences.cLastLoginStatus, false);
    ServerHelper = new ServerHelper("http://129.187.64.247/", null, null);        // initialize connection

    if (mLoginStatus) {
      String defValue = "";
      String storedUsername = settings.getString(Preferences.cUsername, defValue);
      String storedPassword = settings.getString(Preferences.cPass, defValue);
      String storedToken = settings.getString(Preferences.cToken, defValue);
      ServerHelper.SetUserName(storedUsername);
      ServerHelper.SetToken(storedToken);

      Log.d("TOKEN", storedToken);

      Intent intent = new Intent(con, MainPage.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
    }


    reg.setOnClickListener(this);

    password.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          String user = email.getText().toString();
          String pw = password.getText().toString();
          login(user, pw);
        }
        return false;
      }
    });
  }

  public void onResume() {
    // TODO: Passwörter nicht im Klartext
    // Passwort sollte als Hash gespeichert werden
    // => ein auslesen aus einer Textdatei und anschließend einfügen in einen TextView funktioniert nicht mehr
    super.onResume();
    if (settings.contains(Preferences.cUsername)) {
      String defValue = "";
      String storedUsername = settings.getString(Preferences.cUsername, defValue);
      String storedPassword = settings.getString(Preferences.cPass, defValue);
      email.setText(storedUsername);
      password.setText(storedPassword);
    }

  }

  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.button_reg:
        String user = email.getText().toString();
        String pw = password.getText().toString();
        if (user.matches("")) {
          Toast.makeText(con, R.string.login_activity_empty_username, Toast.LENGTH_SHORT).show();
          return;
        }
        if (pw.matches("")) {
          Toast.makeText(con, R.string.login_activity_empty_password, Toast.LENGTH_SHORT).show();
          return;
        }
        login(user, pw);
        break;
    }
  }

  public void login(final String username, final String password) {

    mHandler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        String mString = (String) msg.obj;
        Toast.makeText(con, mString, Toast.LENGTH_SHORT).show();
      }
    };

    dialog.show();
    ServerHelper.SetUserName(username);
    new Thread(new Runnable() {
      public void run() {
        String response = ServerHelper.createNewToken(password);              // create new token for user
        if(response.equals("Wrong username or password")) {                   // check whether token or error message
          Message msg = new Message();                                        // show Toast message
          msg.obj = getResources().getString(R.string.login_activity_wrong_login);          //R.string.login_activity_wrong_login;
          mHandler.sendMessage(msg);
          try {
            Thread.sleep(100);
          }
          catch (InterruptedException e) {
            e.printStackTrace();
          }
          clearSharedPreferences();                                           // bisherige Einstellungen löschen
          dialog.dismiss();                                                   // Dialog ausblenden
          return;
        }
        // if login is correct
        else {
          String tempID[] = ServerHelper.getIDs(password);
          int driver = ServerHelper.getDriver(tempID[0]);
          String surname = ServerHelper.getSurname(tempID[1]);
          String tempPVSpecs[] = ServerHelper.getPVSpecs(tempID[1]);

          prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
          SharedPreferences.Editor prefEditor = prefs.edit();
          prefEditor.putString("UserID", tempID[0]);
          prefEditor.putString("FamilyID", tempID[1]);
          prefEditor.putInt("Driver", driver);
          prefEditor.putString("FamilySurname", surname);

          prefEditor.putString("PV_angle", tempPVSpecs[0]);
          prefEditor.putString("PV_area", tempPVSpecs[1]);
          prefEditor.putString("PV_orientation", tempPVSpecs[2]);
          prefEditor.putString("PV_kWp", tempPVSpecs[3]);
          prefEditor.putString("PV_kWh", tempPVSpecs[4]);
          prefEditor.putString("PV_kWhkWp", tempPVSpecs[5]);
          prefEditor.apply();                                                 // apply instead of commit (apply => runs in background)

          setSharedPreferences(username, password, response);                 // store information in preferences
          Intent intent = new Intent(con, MainPage.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          startActivity(intent);
        }

      }
    }).start();
  }

  /*
   * handles Prefs of Servermanaber-Library
   */
  public void setSharedPreferences(String user, String pass, String token) {
    SharedPreferences.Editor editor = settings.edit();
    editor.putBoolean(Preferences.cLastLoginStatus, true);
    editor.putString(Preferences.cUsername, user);
    editor.putString(Preferences.cPass, pass);
    editor.putString(Preferences.cToken, token);                    // Token auch speichern
    editor.apply();
  }

  public void clearSharedPreferences() {
    SharedPreferences.Editor editor = settings.edit();
    editor.remove(Preferences.cLastLoginStatus);
    editor.remove(Preferences.cPass);
    editor.remove(Preferences.cToken);
    editor.apply();
  }

  public void onBackPressed() {
//		this.finish();
  }

}
