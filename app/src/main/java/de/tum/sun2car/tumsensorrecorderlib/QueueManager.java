package de.tum.sun2car.tumsensorrecorderlib;

import java.io.File;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import android.util.Log;


public final class QueueManager {

	private static QueueManager mQueueManager;
	private static LinkedBlockingDeque<String> mFileSendQueue = null;
	
	private QueueManager(){
		initQueue();
	}
	
	public synchronized static QueueManager getInstance(){
		
		if(mQueueManager == null) {
			Log.v("queue-manager","Generated new Queue-Instance");
			mQueueManager = new QueueManager();
		}
		
		return mQueueManager;
	}
	
	private void initQueue(){
		mFileSendQueue = new LinkedBlockingDeque<String>();
		enqueueOldFiles();
	}
	
	public void enqueueFiletoSend(String filename) {
		try {
			
			if(mFileSendQueue == null) {
				Log.w("bug","send-queue is null");
				initQueue();
			}
			
			mFileSendQueue.putFirst(filename);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void putFileBackToQueue(String fileName) {
		if(mFileSendQueue == null) {
			Log.w("bug","send-queue is null");
			initQueue();
		}
		
		mFileSendQueue.addLast(fileName);
	}
	
	public String getLastQueueElement() {
		String Element = null;
		if(mFileSendQueue == null) {
			Log.w("bug","send-queue is null");
			initQueue();
		}
		try {
			Element = mFileSendQueue.pollLast(5000,TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Element;
	}
	
	public int getQueueSize() {
		if(mFileSendQueue == null) {
			Log.w("bug","send-queue is null");
			initQueue();
		}
		
		return mFileSendQueue.size();
	}
	
	/***
	 * checks all files in the folder for files that hasn't been uploaded and attaches all files to the upload-queue
	 * @return
	 */
	private void enqueueOldFiles() {
		
		new Thread (new Runnable() {
			public void run() {
				if (Preferences.getFileDirectory().isDirectory()) {
			
					for (File file: Preferences.getFileDirectory().listFiles()) {
						if(file.getName().endsWith(".recfile.gz") || file.getName().endsWith(".recfile")) {   //delete .recfile in new version
							try {
								mFileSendQueue.putFirst(file.getAbsolutePath());
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}	
						}
					}
				}
			}
		}).start();
	}
}
