package de.tum.sun2car.logbook;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.tum.sun2car.R;

public class MOTsAdapter extends ArrayAdapter<String>
{
		List<String> mots;		
		Context context;

		public MOTsAdapter(Context context,
				int textViewResourceId, List<String> mots) {
			super(context, textViewResourceId, mots);
			
			this.context = context;
			this.mots = mots;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.item_logbook_mots, null);
			}

			ImageView iv_mot = (ImageView) v.findViewById(R.id.logbook_mot);
			TextView tv_possiblemot = (TextView) v.findViewById(R.id.logbook_mot_correct);
			
			String mot = mots.get(position);
			

//			bike x
//			Bus x
//			car x
//			citytrain 
//			foot x
//			other 
//			SBahn x
//			subway x
//			train x
//			Tram x
//			Walk x
			
			if (mot.equals("bike"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_bike);
					tv_possiblemot.setText("Fahrrad");
			}
			else if (mot.equals("bus"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_bus);
					tv_possiblemot.setText("Bus");
			}
			else if (mot.equals("car"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_car);
					tv_possiblemot.setText("Auto");
			}
			else if (mot.equals("sbahn") || mot.equals("citytrain"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_sbahn);
					tv_possiblemot.setText("S-Bahn");
			}
			else if (mot.equals("subway"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_subway);
					tv_possiblemot.setText("U-Bahn");
			}
			else if (mot.equals("train"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_train);
					tv_possiblemot.setText("Zug");
			}
			else if (mot.equals("tram"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_tram);
					tv_possiblemot.setText("Tram");
			}
			else if (mot.equals("walk") || mot.equals("foot"))
			{
					iv_mot.setBackgroundResource(R.drawable.mot_walk);
					tv_possiblemot.setText("Fußgänger");
			}
			else
			{
					iv_mot.setBackgroundResource(R.drawable.mot_other);
					tv_possiblemot.setText("Anderes");
			}
			
			return v;
		}
}



