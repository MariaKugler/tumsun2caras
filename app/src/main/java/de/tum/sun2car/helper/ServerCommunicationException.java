package de.tum.sun2car.helper;

public class ServerCommunicationException extends Exception {

	public String message;
	
	public ServerCommunicationException(String message){
		this.message = message;
	}
}
