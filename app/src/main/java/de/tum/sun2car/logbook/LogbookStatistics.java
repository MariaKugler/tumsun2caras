package de.tum.sun2car.logbook;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import org.achartengine.GraphicalView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class LogbookStatistics extends FragmentActivity implements OnItemSelectedListener {
  Context con;
    Boolean alreadyLoaded = false;
    int posSpinner = 0;

  private SharedPreferences settings;
  private SharedPreferences prefs;

  Spinner spinner;
    Spinner spinnerRange;
    int iSpinnerRange = -1;
    int lastSpinnerRangeState = 0;

  FrameLayout frame;
  FragmentManager fm;
  FragmentTransaction ft;
  SupportMapFragment mapFragment;
  LogbookChartFragment chartFragment;

  GoogleMap map;
  GoogleMapOptions gmOptions;

  private HeatmapTileProvider mProvider;
  private TileOverlay heatOverlay;

  GraphicalView graph;
    fetchDataClass fetching;

  LatLngBounds.Builder bc;
    List<Drives> drives;
    List<Drives> drivesOrdered;

    List<PolylineOptions> plOpts;
    List<PolylineOptions> plOptsShow;
  List<Polyline> polylines;
  ArrayList<LatLng> latlon;
  ArrayList<Long> timestamp;
  ArrayList<Double> speed;


  int[] hour_count = new int[24];
  int[] day_count = new int[7];

  private ProgressDialog dialog;

  private LocationDB locsSource;
  private DrivesDB drivesSource;


  private static final String TAG_MAPFRAGMENT = "TAG_MapFragment";
  private static final String TAG_GRAPHFRAGMENT = "TAG_GraphFragment";

  double co2_avg_km = 130;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_logbook_statistics);

    con = this;

    int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

//		frame = (FrameLayout) findViewById(R.id.logbook_fragment_container);

    spinner = (Spinner) findViewById(R.id.logbook_stat_spinner);
    // Create an ArrayAdapter using the string array and a default spinner layout
    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
            R.array.logbook_stat_values, android.R.layout.simple_spinner_item);
    // Specify the layout to use when the list of choices appears
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // Apply the adapter to the spinner
    spinner.setAdapter(adapter);
    spinner.setOnItemSelectedListener(this);



    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
    prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);

    drives = new ArrayList<Drives>();
      drivesOrdered = new ArrayList<Drives>();
    latlon = new ArrayList<LatLng>();
    timestamp = new ArrayList<Long>();
    speed = new ArrayList<Double>();

    plOpts = new ArrayList<PolylineOptions>();
    polylines = new ArrayList<Polyline>();

    fm = getSupportFragmentManager();
    ft = fm.beginTransaction();


    locsSource = new LocationDB(this);
    locsSource.open();

    drivesSource = new DrivesDB(this);
    drivesSource.open();


    dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setTitle("Daten laden...");
    dialog.setMessage("Fahrten werden geladen.");
    dialog.setCanceledOnTouchOutside(false);
      dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
              fetching.cancel(true);
              alreadyLoaded = true;
              spinnerRange.setSelection(lastSpinnerRangeState);
          }
      });

      spinnerRange = (Spinner) findViewById(R.id.logbook_stat_range_spinner);
      ArrayAdapter<CharSequence> adapterRange = ArrayAdapter.createFromResource(this,
              R.array.logbook_stat_range_values, android.R.layout.simple_spinner_item);
      // Specify the layout to use when the list of choices appears
      adapterRange.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      // Apply the adapter to the spinner
      spinnerRange.setAdapter(adapterRange);
      spinnerRange.setOnItemSelectedListener(new OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              iSpinnerRange = position;
              if (!alreadyLoaded){
                  fetchData();
              }
              alreadyLoaded = false;
          }

          @Override
          public void onNothingSelected(AdapterView<?> parent) {
              iSpinnerRange = -1;
          }
      });
  }

  public void onResume() {
    super.onResume();

    gmOptions = new GoogleMapOptions();
    gmOptions.mapType(GoogleMap.MAP_TYPE_NORMAL)
            .compassEnabled(false)
            .rotateGesturesEnabled(false)
            .tiltGesturesEnabled(false)
            .zoomControlsEnabled(false);
  }


  /*private void fetchData() {
    fetchThread = new Thread(new Runnable() {
      public void run() {

        drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));
          Log.i("userID", prefs.getString("UserID", ""));
        bc = new LatLngBounds.Builder();

        if (drives.isEmpty()) {
          Handler handler = new Handler(Looper.getMainLooper());
          handler.post(new Runnable() {

            @Override
            public void run() {
              Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
              dialog.dismiss();
            }
          });
        } else {
          Collections.sort(drives, Collections.reverseOrder());

          for (Drives d : drives) {

              Log.i("driveTime",d.getStart_time());

            List<MyLocation> locations = locsSource.getAllLocationsByTrack(d.getTrackID());

            if (!locations.isEmpty()) {
//		    				double ratio = ( d.getCo2gTotal() / (d.getTrip_length()/1000) ) / co2_avg_km;
//		    				// bad
//		    				if ( ratio > 1 )
//		    				{
//		    					poly = new PolylineOptions()
//						        .color(Color.RED);
//		    				}
//		    				// middle
//		    				else if ( ratio > 0.75 )
//		    				{
//		    					poly = new PolylineOptions()
//						        .color(Color.YELLOW);
//		    				}
//		    				// good
//		    				else
//		    				{
//		    					poly = new PolylineOptions()
//						        .color(Color.GREEN);
//		    				}
              PolylineOptions poly = new PolylineOptions()
                      .color(Color.parseColor("#006ab3"))
                      .width(3);

              //Add LatLngs to a polyline
              for (MyLocation loc : locations) {
                // fetch data for map
                LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());
                latlon.add(ll);

                timestamp.add(loc.getDate().getTime());
                speed.add(loc.getSpeed());

                poly.add(ll);

                bc.include(ll);

                // fetch data for time-distribution
                Calendar cal = Calendar.getInstance();
                cal.setTime(loc.getDate());

                int hour = cal.get(Calendar.HOUR_OF_DAY);
                hour_count[hour]++;

                int day = cal.get(Calendar.DAY_OF_WEEK);
                day_count[day - 1]++;

              }
              plOpts.add(poly);

            } else {
              // TODO
              // Bei dieser Schleife wird jeder verfügbare Track geladen
              // leider existieren nicht zu allen Tracks Daten, was sehr viele Meldungen
              // was sehr viele Meldungen produzieren würde
              // => Auskommentieren
//              Handler handler = new Handler(Looper.getMainLooper());
//              handler.post(new Runnable() {
//
//                @Override
//                public void run() {
//                  new AlertDialog.Builder(con)
//                          .setTitle(R.string.logbook_drives_no_locs)
//                          .setMessage(R.string.logbook_drives_no_locs_msg)
//                          .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialoginterface, int i) {
//                              Intent intent = new Intent(con, Logbook.class);
//                              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                              startActivity(intent);
//
//                              ((Activity) con).finish();
//                            }
//                          })
//                          .show();
//                }
//              });
            }


            dialog.dismiss();


          }
          // TODO
          // Verursacht einen StackOverflowError in Zusammenhang mit der Google Maps Karte
//          Handler handler = new Handler(Looper.getMainLooper());
//          handler.post(new Runnable() {
//
//            @Override
//            public void run() {
//              dialog.dismiss();
//
//              for (PolylineOptions po : plOpts) {
//                polylines.add(map.addPolyline(po));
//              }
//
//              map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
//            }
//          });
        }

      }
    });
      fetchThread.start();
  }*/

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.logbook_statistics, menu);
    return true;
  }

    private void fetchData(){
        dialog.show();
        drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));
        //Log.i("userID", prefs.getString("UserID", ""));
        bc = new LatLngBounds.Builder();

        if (drives.isEmpty()) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            });
        } else {
            for (int i = 1; i<=drives.size(); i++) {
                drivesOrdered.add(drives.get(drives.size()-i));
            }
            drives = drivesOrdered;
            fetching = (fetchDataClass) new fetchDataClass().execute(drives);
        }
    }

    private class fetchDataClass extends AsyncTask<List<Drives>, List<PolylineOptions>, List<PolylineOptions>> {

        @Override
        protected List<PolylineOptions> doInBackground(List<Drives>... params){

            int count = params[0].size();
            switch (iSpinnerRange){
                case 0:
                    if (count > 10) {
                        count = 10;
                    }
                    break;
                case 1:
                    if (count > 20) {
                        count = 20;
                    }
                    break;
                case 2:
                    if (count > 50) {
                        count = 50;
                    }
                    break;
            }
            /*Log.i("length0", String.valueOf(params.length));
            Log.i("length1", String.valueOf(params[0].size()));
            Log.i("length2", String.valueOf(params[0].get(0).getStart_time()));
            Log.i("length3", String.valueOf(params[0].get(1).getStart_time()));
            Log.i("length4", String.valueOf(params[0].get(2).getStart_time()));*/

            for (int i = 0; i < count; i ++) {
            //for (Drives d : params) {

                Drives d = params[0].get(i);
                Log.i("driveTime", d.getStart_time());

                List<MyLocation> locations = locsSource.getAllLocationsByTrack(d.getTrackID());

                if (!locations.isEmpty()) {
                    PolylineOptions poly = new PolylineOptions()
                            .color(Color.parseColor("#006ab3"))
                            .width(3);

                    //Add LatLngs to a polyline
                    for (MyLocation loc : locations) {
                        // fetch data for map
                        LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());
                        latlon.add(ll);

                        timestamp.add(loc.getDate().getTime());
                        speed.add(loc.getSpeed());

                        poly.add(ll);

                        bc.include(ll);

                        // fetch data for time-distribution
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(loc.getDate());

                        int hour = cal.get(Calendar.HOUR_OF_DAY);
                        hour_count[hour]++;

                        int day = cal.get(Calendar.DAY_OF_WEEK);
                        day_count[day - 1]++;

                    }
                    plOpts.add(poly);
                    publishProgress(plOpts);
                }
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }


            return plOpts;
        }

        @Override
        protected void onProgressUpdate(List<PolylineOptions>... values) {
            plOptsShow = values[0];
            //Log.i("progress",plOptsShow.get(plOptsShow.size()-1).getPoints().get(0).toString());
        }

        @Override
        protected void onPostExecute(List<PolylineOptions> polylineOptionses) {
            plOptsShow = polylineOptionses;
            lastSpinnerRangeState = iSpinnerRange;
            switchSelected(posSpinner);
            if (dialog.isShowing()){
                dialog.dismiss();
            }
        }
    }

    private void switchSelected (int pos) {
        ArrayList<LatLng> filtered = new ArrayList<LatLng>();
        posSpinner = pos;
        switch (pos) {
            // Kartenansicht
            case 0:
                if (heatOverlay != null) {
                    heatOverlay.remove();
                }

                /**
                 *  Kartenfragment erstellen
                 */

                mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_MAPFRAGMENT);

                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, mapFragment, TAG_MAPFRAGMENT);
                    ft.commit();
                }

                fm.executePendingTransactions();


                if (drives.isEmpty()) {
                    map = mapFragment.getMap();
                    if (map != null) {
                        map.setMyLocationEnabled(true);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "cannot getMap!",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    //TODO: Hier async task + show dialog
                    map = mapFragment.getMap();
                    if (map != null) {
                        map.setMyLocationEnabled(true);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        if (plOptsShow != null) {
                            for (PolylineOptions po : plOptsShow) {
                                polylines.add(map.addPolyline(po));
                            }
                            // new drawPolyLines().execute(plOptsShow);
                        }
                        try {
                            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                  /*map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                      @Override
                      public void onCameraChange(CameraPosition arg0) {
                          // Move camera.
                          map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                          // Remove listener to prevent position reset on camera move.
                          map.setOnCameraChangeListener(null);
                      }
                  });*/
                        } catch (IllegalStateException ex){
                            //map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.265354, 11.66918)));
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.265354, 11.66918), 10));
                            ex.printStackTrace();
                        }

//	    		    	map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "cannot getMap!",
                                Toast.LENGTH_LONG).show();
                    }
                }

                break;

            // Kartenansicht Häufigkeit allgemein
            case 1:
                if (heatOverlay != null) {
                    heatOverlay.remove();
                }

                for (Polyline line : polylines) {
                    line.remove();
                }
                /**
                 *  Kartenfragment erstellen
                 */

                mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_MAPFRAGMENT);

                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, mapFragment, TAG_MAPFRAGMENT);
                    ft.commit();
                }

                fm.executePendingTransactions();


                map = mapFragment.getMap();

                if (map != null) {
                    map.setMyLocationEnabled(true);
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    // checken ob array-liste leer ist
                    // => Meldung ausgeben und Ausführung beenden, da sonst ein Fehler auftritt
                    if (latlon.isEmpty()) {
                        Toast.makeText(getApplicationContext(),
                                "Keine Daten zum anzeigen vorhanden",
                                Toast.LENGTH_LONG).show();
                        break;
                    }

                    // Check if need to instantiate (avoid setData etc twice)
            /*if (new drawHeatMap().execute(latlon).getStatus().equals(AsyncTask.Status.FINISHED)) {
                heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            }*/
                    if (mProvider == null) {
                        mProvider = new HeatmapTileProvider.Builder().data(latlon).build();
                        mProvider.setRadius(10);
                        mProvider.setOpacity(1);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    } else {
                        mProvider.setData(latlon);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    }

                    try {
                        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                  /*map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                      @Override
                      public void onCameraChange(CameraPosition arg0) {
                          // Move camera.
                          map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                          // Remove listener to prevent position reset on camera move.
                          map.setOnCameraChangeListener(null);
                      }
                  });*/
                    } catch (IllegalStateException ex){
                        //map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.265354, 11.66918)));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.265354, 11.66918), 10));
                        ex.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "cannot getMap!",
                            Toast.LENGTH_LONG).show();
                }


                break;

            // Kartenansicht Häufigkeit 0-30
            case 2:

                for (int i = 0; i < speed.size(); i++) {
                    if (speed.get(i) < 30 / 3.6 && speed.get(i) >= 0) {
                        filtered.add(latlon.get(i));
                    }
                }

                if (heatOverlay != null) {
                    heatOverlay.remove();
                }

                for (Polyline line : polylines) {
                    line.remove();
                }
                /**
                 *  Kartenfragment erstellen
                 */

                mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_MAPFRAGMENT);

                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, mapFragment, TAG_MAPFRAGMENT);
                    ft.commit();
                }

                fm.executePendingTransactions();


                map = mapFragment.getMap();

                if (map != null) {
                    map.setMyLocationEnabled(true);
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    // checken ob array-liste leer ist
                    // => Meldung ausgeben und Ausführung beenden, da sonst ein Fehler auftritt
                    if (filtered.isEmpty()) {
                        Toast.makeText(getApplicationContext(),
                                "Keine Daten zum anzeigen vorhanden",
                                Toast.LENGTH_LONG).show();
                        break;
                    }

                    // Check if need to instantiate (avoid setData etc twice)
            /*if (new drawHeatMap().execute(latlon).getStatus().equals(AsyncTask.Status.FINISHED)) {
                heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            }*/
                    if (mProvider == null) {
                        mProvider = new HeatmapTileProvider.Builder().data(filtered).build();
                        mProvider.setRadius(10);
                        mProvider.setOpacity(1);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    } else {
                        mProvider.setData(filtered);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    }

                    //map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));

                    try {
                        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                  /*map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                      @Override
                      public void onCameraChange(CameraPosition arg0) {
                          // Move camera.
                          map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                          // Remove listener to prevent position reset on camera move.
                          map.setOnCameraChangeListener(null);
                      }
                  });*/
                    } catch (IllegalStateException ex){
                        //map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.265354, 11.66918)));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.265354, 11.66918), 10));
                        ex.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "cannot getMap!",
                            Toast.LENGTH_LONG).show();
                }


                break;

            // Kartenansicht Häufigkeit 30-65
            case 3:

                for (int i = 0; i < speed.size(); i++) {
                    if (speed.get(i) > 30 / 3.6 && speed.get(i) < 65 / 3.6) {
                        filtered.add(latlon.get(i));
                    }
                }

                if (heatOverlay != null) {
                    heatOverlay.remove();
                }

                for (Polyline line : polylines) {
                    line.remove();
                }
                /**
                 *  Kartenfragment erstellen
                 */

                mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_MAPFRAGMENT);

                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, mapFragment, TAG_MAPFRAGMENT);
                    ft.commit();
                }

                fm.executePendingTransactions();


                map = mapFragment.getMap();

                if (map != null) {
                    map.setMyLocationEnabled(true);
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    // checken ob array-liste leer ist
                    // => Meldung ausgeben und Ausführung beenden, da sonst ein Fehler auftritt
                    if (filtered.isEmpty()) {
                        Toast.makeText(getApplicationContext(),
                                "Keine Daten zum anzeigen vorhanden",
                                Toast.LENGTH_LONG).show();
                        break;
                    }

                    // Check if need to instantiate (avoid setData etc twice)
           /*if (new drawHeatMap().execute(latlon).getStatus().equals(AsyncTask.Status.FINISHED)) {
                heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            }*/
                    if (mProvider == null) {
                        mProvider = new HeatmapTileProvider.Builder().data(filtered).build();
                        mProvider.setRadius(10);
                        mProvider.setOpacity(1);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    } else {
                        mProvider.setData(filtered);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    }

                    try {
                        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                  /*map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                      @Override
                      public void onCameraChange(CameraPosition arg0) {
                          // Move camera.
                          map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                          // Remove listener to prevent position reset on camera move.
                          map.setOnCameraChangeListener(null);
                      }
                  });*/
                    } catch (IllegalStateException ex){
                        //map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.265354, 11.66918)));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.265354, 11.66918), 10));
                        ex.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "cannot getMap!",
                            Toast.LENGTH_LONG).show();
                }


                break;

            // Kartenansicht Häufigkeit >65
            case 4:

                for (int i = 0; i < speed.size(); i++) {
                    if (speed.get(i) >= 65 / 3.6) {
                        filtered.add(latlon.get(i));
                    }
                }

                if (heatOverlay != null) {
                    heatOverlay.remove();
                }

                for (Polyline line : polylines) {
                    line.remove();
                }
                /**
                 *  Kartenfragment erstellen
                 */
                mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_MAPFRAGMENT);

                if (mapFragment == null) {
                    mapFragment = SupportMapFragment.newInstance();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, mapFragment, TAG_MAPFRAGMENT);
                    ft.commit();
                }

                fm.executePendingTransactions();


                map = mapFragment.getMap();

                if (map != null) {
                    map.setMyLocationEnabled(true);
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    // checken ob array-liste leer ist
                    // => Meldung ausgeben und Ausführung beenden, da sonst ein Fehler auftritt
                    if (filtered.isEmpty()) {
                        Toast.makeText(getApplicationContext(),
                                "Keine Daten zum anzeigen vorhanden",
                                Toast.LENGTH_LONG).show();
                        break;
                    }
                    // Check if need to instantiate (avoid setData etc twice)
            /*if (new drawHeatMap().execute(latlon).getStatus().equals(AsyncTask.Status.FINISHED)) {
                heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            }*/
                    if (mProvider == null) {
                        mProvider = new HeatmapTileProvider.Builder().data(filtered).build();
                        mProvider.setRadius(10);
                        mProvider.setOpacity(1);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    } else {
                        mProvider.setData(filtered);
                        heatOverlay = map.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                    }

                    try {
                        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                  /*map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                      @Override
                      public void onCameraChange(CameraPosition arg0) {
                          // Move camera.
                          map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
                          // Remove listener to prevent position reset on camera move.
                          map.setOnCameraChangeListener(null);
                      }
                  });*/
                    } catch (IllegalStateException ex){
                        //map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.265354, 11.66918)));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(48.265354, 11.66918), 10));
                        ex.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "cannot getMap!",
                            Toast.LENGTH_LONG).show();
                }


                break;

            // Zeitverteilung
            case 5:
                /**
                 * Graphfragment erstellen
                 */

                chartFragment = (LogbookChartFragment) fm.findFragmentByTag(TAG_GRAPHFRAGMENT);
                if (chartFragment == null) {
                    chartFragment = LogbookChartFragment.newInstance(hour_count, day_count, 0);
                    ft = fm.beginTransaction();
                    ft.replace(R.id.logbook_fragment_container, chartFragment);
                    ft.commit();
                }

                fm.executePendingTransactions();
                break;

            // Zeitverteilung mit PV
            case 6:
                break;
            default:
                break;
        }
    }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
    switchSelected(pos);
  }

  @Override
  public void onNothingSelected(AdapterView<?> arg0) {
    // TODO Auto-generated method stub

  }

}
