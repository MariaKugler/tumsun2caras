package de.tum.sun2car.logbook;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.FamilyDB;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.SyncSun2CarService;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class LogbookDrives extends Activity {

  List<Drives> drives;
  Context con;

  Parcelable state;

  private LocationDB locsSource;
  private DrivesDB drivesSource;
  private FamilyDB familySource;

  ListView drivesListView;
  LinearLayout ll;

  private SharedPreferences settings;
  private SharedPreferences prefs;

  private ProgressDialog dialog;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_logbook_drives);

    con = this;
    ll = (LinearLayout) findViewById(R.id.drives_layout);

    drives = new ArrayList<Drives>();

    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
    prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);


    dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setTitle("Tracks laden...");
    dialog.setMessage("Tracks werden geladen.");

    // check whether sync-service is running at the moment
    SyncSun2CarService mService = new SyncSun2CarService();
    boolean running = mService.mIsRunning;

    boolean serviceRunning = false;
    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
      if (SyncSun2CarService.class.getName().equals(service.service.getClassName())) {
        serviceRunning = true;
      }
    }
    if (running) {
      Toast.makeText(con, "Synchronisierung läuft derzeit.", Toast.LENGTH_LONG).show();
    }
  }

  public void onResume() {
    super.onResume();

    Bundle b = new Bundle();
    b = getIntent().getExtras();

    if (b != null) {
      state = b.getParcelable("lv_state");
    }

    locsSource = new LocationDB(this);
    locsSource.open();

    drivesSource = new DrivesDB(this);
    drivesSource.open();

    familySource = new FamilyDB(this);
    familySource.open();

    dialog.show();

    new Thread(new Runnable() {
      public void run() {

        drives = drivesSource.getAllDrivesByUser(prefs.getString("UserID", ""));

        if (drives.isEmpty()) {
          Handler handler = new Handler(Looper.getMainLooper());
          handler.post(new Runnable() {

            @Override
            public void run() {
              Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
              dialog.dismiss();
            }
          });
        } else {
          Collections.sort(drives);

          Handler handler = new Handler(Looper.getMainLooper());
          handler.post(new Runnable() {

            @Override
            public void run() {

              drivesListView = (ListView) findViewById(R.id.drives_listview);
              drivesListView.setFastScrollEnabled(true);
              DrivesAdapter adapter = new DrivesAdapter(con, R.layout.item_logbook_drives, drives);


//					        int index = drivesListView.getFirstVisiblePosition();
//					        drivesListView.setSelectionFromTop(index, 0);

              if (state == null) {
                state = drivesListView.onSaveInstanceState();
//					            drivesListView.setAdapter(adapter);
              }
//					        else
//					        {
//					            ((DrivesAdapter)drivesListView.getAdapter()).reload(smvData);
//					        }

              drivesListView.setAdapter(adapter);
              // Restore previous state (including selected item index and scroll position)
              drivesListView.onRestoreInstanceState(state);


              drivesListView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                  final Drives sel_drive = drives.get(position);
                  final Double trackID = sel_drive.getTrackID();
                  final String trackMOT = sel_drive.getTrip_mot();

                  AlertDialog alert;
                  AlertDialog.Builder builder = new AlertDialog.Builder(con);
                  builder.setTitle("Fahrt-Details")
                          .setItems(R.array.drive_details, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                              Intent intent;
                              Bundle b = new Bundle();

                              switch (which) {
                                case 0:
                                  // Anzeige
                                  intent = new Intent(con, LogbookDrivesMap.class);
                                  b.putDouble("trackID", trackID);
                                  intent.putExtras(b);

                                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                  startActivity(intent);
                                  break;
                                case 1:
                                  // Korrektur
                                  intent = new Intent(con, LogbookDrivesCorrect.class);
                                  b.putDouble("trackID", trackID);
                                  b.putString("trackMOT", trackMOT);

                                  // Save ListView state
                                  state = drivesListView.onSaveInstanceState();
                                  b.putParcelable("lv_state", state);
                                  intent.putExtras(b);
                                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                  startActivity(intent);
                                  break;
                                default:
                                  break;
                              }
                            }
                          })
                          .setCancelable(true);
                  alert = builder.create();
                  alert.show();
                }
              });

              dialog.dismiss();
            }
          });
        }


      }
    }).start();

  }


  public boolean isOnline() {
    ConnectivityManager connectivityManager
            = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
  }


  @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logbook_drives, menu);
		return true;
	}
	
	
}
