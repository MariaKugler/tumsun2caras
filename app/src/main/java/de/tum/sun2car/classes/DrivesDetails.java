package de.tum.sun2car.classes;

import org.json.JSONException;
import org.json.JSONObject;


public class DrivesDetails
{
	private int purpose;
	private int planning;
	private int electric;
	private Long duration_rec;
	private Double distance;
	private Long start_rec;
	
	public DrivesDetails(int purpose, int planning, int electric, Long duration_rec, Double distance, Long start_rec)
	{
		setPurpose(purpose);
		setPlanning(planning);
		setElectric(electric);
		setDuration_rec(duration_rec);
		setDistance(distance);
		setStart_rec(start_rec);
	}


	public DrivesDetails(JSONObject json) throws JSONException
	{
		this.purpose = json.optInt("purpose");
		this.planning = json.optInt("planned");
		this.electric = json.optInt("electric");
		this.duration_rec = json.optLong("duration");
		this.distance = json.optDouble("distance");
		this.start_rec = json.optLong("start_time");
	}


	public int getPurpose() {
		return purpose;
	}


	public void setPurpose(int purpose) {
		this.purpose = purpose;
	}


	public int getPlanning() {
		return planning;
	}


	public void setPlanning(int planning) {
		this.planning = planning;
	}


	public Long getDuration_rec() {
		return duration_rec;
	}


	public void setDuration_rec(Long duration_rec) {
		this.duration_rec = duration_rec;
	}


	public Double getDistance() {
		return distance;
	}


	public void setDistance(Double distance) {
		this.distance = distance;
	}


	public Long getStart_rec() {
		return start_rec;
	}


	public void setStart_rec(Long start_rec) {
		this.start_rec = start_rec;
	}


	public int getElectric() {
		return electric;
	}


	public void setElectric(int electric) {
		this.electric = electric;
	}

}
