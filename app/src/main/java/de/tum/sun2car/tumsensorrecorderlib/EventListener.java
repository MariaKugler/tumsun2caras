package de.tum.sun2car.tumsensorrecorderlib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream; // zip files
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import com.google.protobuf.GeneratedMessage;
import de.tum.sun2car.protocol.SensorMessageProtos.GpsLocationMessage;
import de.tum.sun2car.protocol.SensorMessageProtos.RawSensorMessage;
import de.tum.sun2car.protocol.SensorMessageProtos.RawSensorMessage.RawSensorType;
import de.tum.sun2car.protocol.SensorMessageProtos.SensorMessage;


public class EventListener extends Thread implements SensorEventListener, LocationListener{
	
	private SensorManager mSensorManager;
	private LocationManager mLocManager;
	private QueueManager mQueueManager;
	
    private Sensor mAccSensor;
    private Sensor mGyroSensor;
    private Sensor mMagSensor;
    
    private String s;
    
    private int accCounter = 0;
    private long lastAccTime = 0;
    
    private int gyroCounter = 0;
    private long lastGyroTime = 0;
 
    private int magCounter = 0;
    private long lastMagTime = 0;
  
    private int measuresPerInterval = 1000;
    
    private static final String TAG = "EventListener";
        
    private FileOutputStream fos;
        
    // name of the file, that is currently written
    private String currFileName = "";
    
    // size of file currently written
    private long currFileSize = 0;
    
    private final int MAX_FILE_SIZE = 128*1024; //512*1024;
    
    
    // for adding messages
    private SensorMessage.Builder mSensorBuilder = null;
    
    private String mFileNameExtension;
    private String mAppVersion = "1.0";
    private String mUploadFileSuffix = "recfile.gz";
    
    private Context context;
    
    private Handler handler;   
    
    /***
     * the unix-timstamp when the smartphone was started (when sensor-timestamp was 0)
     */
    private double mStartTimeAbsoluteS;
    
    public EventListener(Context con) {
    	context = con;
    	mQueueManager = QueueManager.getInstance();
    }
    
    
    public void run() {
		Log.i(TAG, "onCreate");

    	try
    	{
    		long currTimeRelativeToBootMs = SystemClock.uptimeMillis();
    		long currTimeAbsoluteMs = System.currentTimeMillis();
    		
    		mStartTimeAbsoluteS = ((double)(currTimeAbsoluteMs - currTimeRelativeToBootMs))/(double)1000.0;
    		
    		fos = null;
			
			String androidVersion = Build.VERSION.RELEASE;
			String phoneModel = Build.MANUFACTURER+"-"+Build.MODEL;
			phoneModel= phoneModel.replace(" ", "-");
			mFileNameExtension = mAppVersion+"_"+androidVersion+"_"+phoneModel+"."+mUploadFileSuffix;
			
			// create global senor-builder for adding all messages
			mSensorBuilder = SensorMessage.newBuilder();			
			        
	        //get GLS LocationManager
	        mLocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	        //mLastLocation = null;
			
	        //if GPS is disabled launch GPS settings...
			if(!mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER )) {
				Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS ); 
				myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(myIntent);
			}
	        
	        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	        mAccSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	        mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	        mMagSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	        
			int delay = SensorManager.SENSOR_DELAY_NORMAL;
			
			Looper.prepare();
			handler = new Handler();
			
			mSensorManager.registerListener(this, mAccSensor, delay, handler);
			mSensorManager.registerListener(this, mGyroSensor, delay, handler);
			mSensorManager.registerListener(this, mMagSensor, delay, handler);
			mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this, Looper.myLooper());
			
			Looper.loop();
			
        }
        catch (Exception e)
        {//Catch exception if any
        	System.err.println("Error: " + e.getMessage());
        	Log.i(TAG, e.getMessage());
        } 
    }   
    
    
	public void stoplogging()  {
		Log.i(TAG, "onDestroy");
		unRegisterSensorListener();
		
		handler.getLooper().quit();
		forceWritingFile();
		
		this.interrupt();
	}
	
	
	public void unRegisterSensorListener() {
		mSensorManager.unregisterListener(this, mAccSensor);
		mSensorManager.unregisterListener(this, mGyroSensor);
		mSensorManager.unregisterListener(this, mMagSensor);
		mLocManager.removeUpdates(this);
	}

	
	/***
	 * writes cached data to file when sensorservice stopps (only the data that hasn't been written yet)
	 */
	private synchronized void forceWritingFile() {
		
		if(mSensorBuilder != null && (mSensorBuilder.getRawMessageCount() > 0 || mSensorBuilder.getGpsLocationMessageCount() > 0)) {
			try {
				writeCacheToFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	/**
	 * creates the directory for writing the log-file
	 * @param path name of directory that shoul be created
	 * @return
	 */
	public static boolean createDirIfNotExists(String path) {
		boolean ret = true;

		File file = new File(Environment.getExternalStorageDirectory(), path);
		if (!file.exists()) {
			if (!file.mkdirs()) {
				Log.e("Export Activity ", "Problem creating folder");
				ret = false;
			}
		}
		return ret;
	}
    

	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
	 */
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
	 */
	public void onSensorChanged(SensorEvent event) {
        
		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		{
			try {
			
				if(accCounter == 0 || accCounter == measuresPerInterval)
				{
					s  = String.valueOf("acc: "+(event.timestamp-lastAccTime)/1000000000.0);
					lastAccTime = event.timestamp;
					Log.i(TAG, s);
					accCounter = 0;				
				}
				
				writeData(
						createSensorMessage(RawSensorType.Accelerometer,event.timestamp,event.values)
				);
				accCounter++;				
			}
			catch (Exception e) {
				e.printStackTrace();
				Log.i(TAG, e.getMessage());
			}
		}
		else if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE)
		{
			try {
				
				if(gyroCounter == 0 || gyroCounter == measuresPerInterval)
				{
					s = String.valueOf("gyro: "+(event.timestamp-lastGyroTime)/1000000000.0);
					lastGyroTime = event.timestamp;
					Log.i(TAG, s);
					gyroCounter = 0;
				}
				
				writeData(
						createSensorMessage(RawSensorType.Gyro,event.timestamp,event.values)
				);
				
				gyroCounter++;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Log.i(TAG, e.getMessage());
			}
			
		}
		else if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
		{
			
			try {
				if(magCounter == 0 || magCounter == measuresPerInterval)
				{
					s = String.valueOf("mag: "+(event.timestamp-lastMagTime)/1000000000.0);
					lastMagTime = event.timestamp;
					Log.i(TAG, s);
					magCounter = 0;
				}

				writeData(
						createSensorMessage(RawSensorType.Magnetometer,event.timestamp,event.values)
				);
				
				magCounter++;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Log.i(TAG, e.getMessage());
			}
			
		}	
	}
	
	public void onLocationChanged(Location location) {
		
		writeData(
				createGPSMessage(location)
		);
		
	}
	

	/**
	 * stores a single sensor reading into a SensorMessage (ProtocolBuffer)
	 * @param type: type of sensor message, can be acc, gyro or mag
	 * @param timestampRelativeInNs: timestamp of the sensor reading
	 * @param values: x-, y-, and z-values of the sensor reading
	 * @return
	 */
	private RawSensorMessage createSensorMessage(RawSensorType type,long timestampRelativeInNs, float[] values) {
		RawSensorMessage.Builder rawBuilder = RawSensorMessage.newBuilder();
		rawBuilder.setRawSensorType(type);

		String nexus = "Nexus";
		String sony = "Sony";
		
		double SensorEventTimeInS;
		
		if((Build.MODEL).contains(nexus) || (Build.MODEL).contains(sony)) {
			SensorEventTimeInS = timestampRelativeInNs/1000000000.0;
		}
		else {
			SensorEventTimeInS = mStartTimeAbsoluteS+((double)timestampRelativeInNs)/1000000000.0;
		}
		
		rawBuilder.setTimestamp(SensorEventTimeInS);
		rawBuilder.setX(values[0]);
		rawBuilder.setY(values[1]);
		rawBuilder.setZ(values[2]);
		
		return rawBuilder.build();
	}
	
	/**
	 * stores a single sensor reading into a SensorMessage (ProtocolBuffer)
	 * @param loc: type of sensor message, can be acc, gyro or mag
	 * @return
	 */
	private GpsLocationMessage createGPSMessage(Location loc) {
		GpsLocationMessage.Builder gpsBuilder = GpsLocationMessage.newBuilder();
		
		gpsBuilder.setTimestamp(((double)loc.getTime())/1000.0);
		gpsBuilder.setLatitude(loc.getLatitude());
		gpsBuilder.setLongitude(loc.getLongitude());
		gpsBuilder.setAltitude(loc.getAltitude());
		gpsBuilder.setHorizontalAccuracy(loc.getAccuracy());
		gpsBuilder.setVerticalAccuracy(loc.getAccuracy());
		gpsBuilder.setCourse(loc.getBearing());
		gpsBuilder.setSpeed(loc.getSpeed());
		
		return gpsBuilder.build();
		
	}
	
	
	/**
	 * appends a SensorMessage to a file and sends it if file is full
	 * @param msg: a SensorMessage that should be sent out
	 */
	private synchronized void writeData(GeneratedMessage msg) {
		
		try {
			
			if (msg instanceof RawSensorMessage) {
				mSensorBuilder.addRawMessage((RawSensorMessage)msg);
				
			}
			else if (msg instanceof GpsLocationMessage)
			{
				mSensorBuilder.addGpsLocationMessage((GpsLocationMessage)msg);	
			}
			else
			{
				throw new Exception("unknown type in writeData: "+msg.getClass().toString());
			}
			
			currFileSize += msg.getSerializedSize();
		
				
			if (currFileSize > MAX_FILE_SIZE) {
				writeCacheToFile();				
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void writeCacheToFile() throws IOException {
		File outFile;
		String timestamp;
		do {
			// whole-number-division, because second-precision is enough
			timestamp = ""+(System.currentTimeMillis()/1000);
			currFileName = Preferences.getFileDirectory().toString()+"/"+timestamp+"_"+mFileNameExtension;
			outFile = new File(currFileName);

		} while(outFile.exists());  // try more names fore the case that there exists already an old file with that name
		
//		fos_data = new FileOutputStream(outFile);
//
		SensorMessage fullMessage = mSensorBuilder.build();
//
//		fullMessage.writeTo(fos_data);
//
//		fos_data.flush();
//		fos_data.close();

        fos = new FileOutputStream(outFile);
        //byte[] bytes = fullMessage;
        GZIPOutputStream zos = new GZIPOutputStream(fos);
        try {
            zos.write(fullMessage.toByteArray());
        } finally {
            zos.close();
        }
        fos.flush();
		fos.close();

		//sendFileToServer(currFileName);
		if (mQueueManager == null) {
			mQueueManager = QueueManager.getInstance();
		}
		
		mQueueManager.enqueueFiletoSend(currFileName);
		
		fos = null;
		currFileSize = 0;
//		sensorBuilder = null;
		
		// try out if clear does the job
		mSensorBuilder.clear();
	}
	

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	
	//static int i = 0;
	public static void logLocationToSepeateFile(Location loc)  {

		//i++;
		
		String locationFileName = Preferences.getFileDirectory().toString()+"/locationFile.txt";
		
		try {
			
			//boolean append = (i==1?false:true);
			
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(locationFileName, true)));
			
			
			//out.println("Provider;Timestamp(absolute);Latitude;Longitude;Altitude;AccuracyX;AccuracyY;Bearing;Speed;");
			
			out.println(loc.getProvider()+";"+
						((double)loc.getTime())/1000.0+";"+
						loc.getLatitude()+";"+
						loc.getLongitude()+";"+
						loc.getAltitude()+";"+
						loc.getAccuracy()+";"+
						loc.getAccuracy()+";"+
						loc.getBearing()+";"+
						loc.getSpeed()+";"
					   );

			
			
		    out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //opens file
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
