package de.tum.sun2car.feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.helper.GMailSender;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class FeedbackWrite extends Activity
{
	ImageButton btn_send;
	EditText et_feedback;
	EditText et_to_cc;
	
    private SharedPreferences settings;
	Boolean success;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_feedback_write);
		
		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		
		et_feedback = (EditText) findViewById(R.id.feedback_write_text);
		et_to_cc = (EditText) findViewById(R.id.feedback_write_cc);
		btn_send = (ImageButton) findViewById(R.id.feedback_voice_record);
	}
	
	
	public void onSend(View v)
	{
		String feedback_text = et_feedback.getText().toString();
		String feedback_cc = et_to_cc.getText().toString();
		String username = settings.getString(Preferences.cUsername,null);
		
		if ( !feedback_text.isEmpty() )
		{
			try {
				success = new SendWriteFeebackTask().execute(username, feedback_text, feedback_cc).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (success) {
                Toast.makeText(FeedbackWrite.this,
                        "Feedback erfolgreich gesendet.",
                        Toast.LENGTH_LONG).show();
                et_feedback.setText("");
                et_to_cc.setText("");
                
            } else {
                Toast.makeText(FeedbackWrite.this, "Feedback wurde nicht gesendet.",
                        Toast.LENGTH_LONG).show();
            }
		}
		else
		{
			AlertDialog alert;
			String errorMsg = "Kein Feedback vorhanden";
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setMessage(errorMsg)
    			   .setTitle("Mitteilung")
    			   .setCancelable(true);
			alert = builder.create();
    		alert.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.feedback_write, menu);
		return true;
	}


}

class SendWriteFeebackTask extends AsyncTask<String, Void, Boolean> {

    private Exception exception;
	boolean success;
	List<String> a = new ArrayList<String>();

    protected Boolean doInBackground(String... param) {
        try
        {
//        	android.os.Debug.waitForDebugger();
        	
        	GMailSender mailsender = new GMailSender("probanden.sun2car@gmail.com", "sun2car@GAP");

        	a.add("sun2car@ftm.mw.tum.de");	//TODO: set project-mail-address!

        	if ( !param[2].isEmpty() )
        	{
        		a.add(param[2]);
        	}
        	
        	String[] toArr = new String[a.size()];
        	a.toArray(toArr);
        	
            mailsender.set_to(toArr);
            mailsender.set_from("probanden.sun2car@gmail.com");
            mailsender.set_subject("Text Feedback: " + param[0]);
            mailsender.setBody("Absender: " + param[0] + "\nFeedback:\n" + param[1]);

            try {
                //mailsender.addAttachment("/sdcard/filelocation");

       
                if (mailsender.send()) {
                    success = true;
                } else {
                    success = false;
                }
            } catch (Exception e) {
                
                Log.e("MailApp", "Could not send email", e);
            }
            
    		return success;
        } catch (Exception e) {
            this.exception = e;
            return false;
        }
    }

    protected void onPostExecute() {
        // TODO: check this.exception 
        // TODO: do something with the feed
    }
}
