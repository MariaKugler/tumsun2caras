package de.tum.sun2car.classes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Two segments can be merged using {@code segment1.addAll(segment2) } 
 * 
 */
public class Segment extends ArrayList<MyLocation> 
{
	public boolean isMissing = false;
	/*public boolean tramStationAtBegin = false;
	public boolean busStationAtBegin = false;*/
	
	public String wayType = "";

	/**
	 * See http://stackoverflow.com/questions/2734208/what-is-the-best-way-to-
	 * store-three-attributes-entry-sets-in-java
	 * 
	 * @param <U>
	 * @param <V>
	 */
	private class Pair<U, V> {

		public U first;

		public V second;

		public Pair(U first, V second)
		{
			this.first = first;
			this.second = second;
		}
	}

	/*
	 * public static <U, V> Pair<U, V> newInstance(U first, V second) { return
	 * new Pair<U, V>(first, second); }
	 */

	/**
	 * @param segment
	 */
	public Segment(List<MyLocation> segment)
	{
		super(segment);
	}

	public Segment()
	{
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void setWayType(String way_type)
	{
		wayType = way_type;
	}

	// private TransportationMode mode;
	public String transportationMode = "";

	/*
	 * public TransportationMode getMode() { return mode; }
	 */

	/**
	 * Splits this Segment into two Segments at a given index. A new Segment,
	 * which contains all MyLocations from range [0,i), is returned.
	 * 
	 * 
	 * @param i index
	 * @param remove If
	 * @return new Segment
	 */
	public Segment splitAtIndex(int i)
	{
		List<MyLocation> subList = this.subList(0, i);
		Segment newSegment = new Segment(subList);
		subList.clear();

		return newSegment;
	}

	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public Segment subSegment(MyLocation start, MyLocation end)
	{
		return new Segment(this.subList(this.indexOf(start), this.indexOf(end)));
	}

	/**
	 * 
	 * @param MyLocation
	 * @return
	 */
	public Segment splitAtMyLocation(MyLocation MyLocation)
	{
		int index = this.indexOf(MyLocation);
		return this.splitAtIndex(index);
	}

	/**
	 * 
	 * @return 
	 */
	public MyLocation getLast()
	{
		if (this.size() == 0)
		{
			return null;
		}
		return this.get(this.size() - 1);
	}


	/**
	 * 
	 * @return
	 */
	public MyLocation getFirst()
	{
		if (this.size() == 0)
		{
			return null;
		}
		return this.get(0);
	}

	//Insert for Calculation of the MyLocation in the middle of the segment
	/**
	 * 
	 * @return
	 */
	public MyLocation getMiddle()
	{
		if (this.size() == 0)
		{
			return null;
		}
		else
		{
			int SegmentElements = this.size();
			return this.get(SegmentElements/2);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("dd.MM - HH:mm:ss");
		return "Segment [" + sdf.format(this.getFirst().getDate()) + " -> " + sdf.format(this.getLast().getDate()) + "; transportationMode=" + transportationMode + ", MyLocations=" + super.toString() + "]";
	}
	
	
	public double getMaxSpeed(Segment s)
	{
		double maxSpeed = 0.0;
		double speed;
		
		if (s.size() > 0)
		{ 
            for (MyLocation i : s)
            {
                speed = i.getSpeed();
                if (speed > maxSpeed)
                	maxSpeed = speed;
            }
        }
		
        return maxSpeed;
		
	}
	
	//Start David
	//sumHeadingChange() und averageHeadingCHange()
	
	/**
	 * Summiert die Richtungswechsel vom 2.Punkt(=1.Punkt von Segment segment) bis zum letzten Punkt des Segmentes auf.
	 * @param Segment
	 * @return Sum Heading Change
	 *  
	 */
	
	public static double sumHeadingChange (Segment segment){
        if (segment.size() > 0) {
            double sum = 0;
 
            for (int i=1; i<segment.size();i++) {
                sum += MyLocation.courseChangeBetween(segment.get(i),segment.get(i-1));
            }
            return sum;
        }
        return 0;
    }
	
	/**
	 * Berechnet die durchschnittliche Richtungsänderung des Segmentes.
	 * @param Segment
	 * @return Average Heading Change Rate
	 */
	 public static double averageHeadingChange (Segment segment){
	        double sum = sumHeadingChange(segment);
	        double mean = 0;
	        mean = sum / segment.size();
	        return mean;
	    }


	
	
	//***********************************************************************************************************//
	// Helfer-Funktionen
	//***********************************************************************************************************//
	/**
	 * 
	 * @param list
	 * @param which:
	 * 		0: values < threshold
	 * 		1: values > threshold
	 * 		2: values <= threshold
	 * 		3: values >= threshold
	 * 		4: values == threshold
	 * 		5: values != threshold
	 * @param threshold
	 * @return 
	 * @return indices
	 */
	public List<Integer> find(List<Double> list, int which, double threshold)
	{
		int count = 0;
		List<Integer> indices = new ArrayList<Integer>();
		
		switch (which)
		{
			case 0:
				for (Double d : list)
				{
					if ( d < threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
				
			case 1:
				for (Double d : list)
				{
					if ( d > threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
				
			case 2:
				for (Double d : list)
				{
					if ( d <= threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
				
			case 3:
				for (Double d : list)
				{
					if ( d >= threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
				
			case 4:
				for (Double d : list)
				{
					if ( d == threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
				
			case 5:
				for (Double d : list)
				{
					if ( d != threshold )
					{
						indices.add(count);
					}
					count++;
				}
				break;
		}
		
		
		return indices;
		
	}
	
	public List<Integer> findInCluster(double value, double[] cluster)
	{
		int count = 0;
		List<Integer> indices = new ArrayList<Integer>();
		
		for (double d : cluster)
		{
			if ( value > d )
			{
				indices.add(count);
			}
			count++;
		}
		
		return indices;
	}
	
	
	public double getMeanValue(List<Double> list)
	{
		double sum = 0;
		for (Double d : list)
		{
			sum = sum + d;
		}
		
		return (sum / list.size());
		
	}
	
	public double getMinValue(double[] array)
	{
		double min = 0;
		
		for (double d : array)
		{
			if ( d < min )
			{
				min = d;
			}
		}
		return min;
	}
	
	public double getMaxValue(double[] array)
	{
		double max = 0;
		
		for (double d : array)
		{
			if ( d > max )
			{
				max = d;
			}
		}
		return max;
	}
	
	
	public double getStdValue(List<Double> list)
	{
		int no = list.size();
		
		double sum = 0.0;
		double sumOfSquares = 0.0;
		for (Double d : list)
		{
			sum = sum + d;
			sumOfSquares = sumOfSquares + d*d;
		}
	    
	    return Math.sqrt((no * sumOfSquares - sum * sum) / (no * (no - 1)));
	}

}
