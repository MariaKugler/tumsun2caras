package de.tum.sun2car.logbook;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.footprint.FootprintCalc;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.GMailSender;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class LogbookDrivesCorrect extends Activity {
  private LocationDB locsSource;
  private DrivesDB drivesSource;
  Double trackID;
  String trackMOT;
  Parcelable state;
  Context con;
  String[] namesMOT = {"foot", "bike", "car", "bus", "train", "tram", "subway"};

  List<String> motsPossible = new ArrayList<String>();

  private SharedPreferences settings;
  private ServerHelper ServerHelper;
  private String ServerUrl = "http://129.187.64.247/";

  ListView motsListView;
  private ProgressDialog dialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_logbook_drives_correct);

    con = this;

    locsSource = new LocationDB(this);
    locsSource.open();

    drivesSource = new DrivesDB(this);
    drivesSource.open();

    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
    String storedUsername = settings.getString(Preferences.cUsername, null);
    String storedToken = settings.getString(Preferences.cToken, null);
    if(storedUsername != null && !storedUsername.isEmpty()) {
      ServerHelper = new ServerHelper(ServerUrl, storedUsername, storedToken);              // initialize connection
    }
    else {
      Toast.makeText(con, "Fehler in den Nutzerdaten", Toast.LENGTH_LONG).show();
      return;
    }

    dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setTitle("Korrektur senden...");
    dialog.setMessage("Korrektur wird an Server übertragen.");

    Bundle b = new Bundle();
    b = getIntent().getExtras();

    trackID = b.getDouble("trackID");
    trackMOT = b.getString("trackMOT");
    state = b.getParcelable("lv_state");

    // get possible MOTs
    for (String mot : namesMOT) {
      if (!mot.equalsIgnoreCase(trackMOT))
        motsPossible.add(mot);
    }

    motsListView = (ListView) findViewById(R.id.drivescorrect_listview);
    motsListView.setFastScrollEnabled(true);

    if (motsListView.getAdapter() == null)
      motsListView.setAdapter(new MOTsAdapter(con, R.layout.item_logbook_mots, motsPossible));

    motsListView.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        final String newMOT = motsPossible.get(position);
        dialog.show();
        new Thread(new Runnable() {
          public void run() {
            drivesSource.setCorrectedMOT(newMOT, trackID);
            Double TrackLength = drivesSource.getTrackLength(trackID);

            List<MyLocation> locs = new ArrayList<MyLocation>();
            locs = locsSource.getAllLocationsByTrack(trackID);
            List<List<Double>> trackdata = FootprintCalc.getCalcList(locs);
            Double co2Trip = FootprintCalc.calculateCO2Trip(trackdata.get(1), trackdata.get(2), trackdata.get(3), newMOT, TrackLength);
            drivesSource.updateDriveCO2(trackID, co2Trip);

            // Temporär geändert, da Passwort entfernt
            String successMOT = ServerHelper.setCorrectedMOT(newMOT, trackID);
            String successCO2 = ServerHelper.setCO2Emission(co2Trip, trackID);

//            String successMOT = serverHelper.setCorrectedMOT(pw, newMOT, trackID);
//            String successCO2 = serverHelper.setCO2Emission(pw, co2Trip, trackID);

            if (successMOT.equals("1")) {
              Handler handler = new Handler(Looper.getMainLooper());
              handler.post(new Runnable() {
                @Override
                public void run() {
                  Toast.makeText(LogbookDrivesCorrect.this, "MOT erfolgreich geändert.",
                          Toast.LENGTH_LONG).show();
                  try {
                    Boolean success = new SendLogbookChangedTask().execute(String.valueOf(trackID), trackMOT, newMOT).get();
                  } catch (InterruptedException | ExecutionException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                  }

                  dialog.dismiss();

                  Intent intent = new Intent(con, Logbook.class);
                  Bundle b = new Bundle();
                  b.putParcelable("lv_state", state);
                  intent.putExtras(b);
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                }
              });
            } else {
              Handler handler = new Handler(Looper.getMainLooper());
              handler.post(new Runnable() {
                @Override
                public void run() {
                  Toast.makeText(LogbookDrivesCorrect.this, "MOT ändern derzeit nicht möglich, bitte später nochmal versuchen.",
                          Toast.LENGTH_LONG).show();
                  dialog.dismiss();
                  Intent intent = new Intent(con, Logbook.class);
                  Bundle b = new Bundle();
                  b.putParcelable("lv_state", state);
                  intent.putExtras(b);
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                }
              });
            }
          }
        }
      ).
      start();
    }
  });
  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logbook_drives_correct, menu);
		return true;
	}
	

}

// TODO: das später über DB lösen
class SendLogbookChangedTask extends AsyncTask<String, Void, Boolean> {

  private Exception exception;
  boolean success;
  List<String> a = new ArrayList<String>();

  protected Boolean doInBackground(String... param) {
    try {
      GMailSender mailsender = new GMailSender("probanden.sun2car@gmail.com", "sun2car@GAP");

      a.add("sun2car@ftm.mw.tum.de");  //TODO: set project-mail-address!

      String[] toArr = new String[a.size()];
      a.toArray(toArr);

      mailsender.set_to(toArr);
      mailsender.set_from("probanden.sun2car@gmail.com");
      mailsender.set_subject("Geänderter Track: " + param[0]);
      mailsender.setBody("AusgangsMOT: " + param[1] + "\nKorrigiertes MOT: " + param[2]);

      try {
        if (mailsender.send()) {
          success = true;
        } else {
          success = false;
        }
      } catch (Exception e) {
        Log.e("MailApp", "Could not send email", e);
      }

      return success;
    } catch (Exception e) {
      this.exception = e;
      return false;
    }
  }

  protected void onPostExecute() {
    // TODO: check this.exception
    // TODO: do something with the feed
  }

}
