package de.tum.sun2car.balance;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.tum.sun2car.MainPage;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.DrivesDetails;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class BalancePurposes extends Activity
{
	ServerHelper serverHelper;
	String url = "http://129.187.64.247/";
	
	List<DrivesDetails> driveDetails;
	Context con;
	String userName;
	String password;
    private ProgressDialog dialog;
    private SharedPreferences settings;
	private SharedPreferences prefs;
	
	List<DrivesDetails> dd_business;
	List<DrivesDetails> dd_shopping;
	List<DrivesDetails> dd_leisure;
	List<DrivesDetails> dd_family;
	List<DrivesDetails> dd_holiday;
	List<DrivesDetails> dd_fun;

	private static int[] COLORS_CHART_PLANNING = new int[] { Color.parseColor("#006ab3"), Color.parseColor("#cc0033")};
	private static int[] COLORS_CHART_ELECTRIC = new int[] { Color.parseColor("#006ab3"), Color.parseColor("#cc0033"), Color.parseColor("#1d741d")};

	
	private GraphicalView graph_business_planning;
	private GraphicalView graph_shopping_planning;
	private GraphicalView graph_leisure_planning;
	private GraphicalView graph_family_planning;
	private GraphicalView graph_holiday_planning;
	private GraphicalView graph_fun_planning;	
	
	LinearLayout ll_business_planning;
	LinearLayout ll_shopping_planning;
	LinearLayout ll_leisure_planning;
	LinearLayout ll_family_planning;
	LinearLayout ll_holiday_planning;
	LinearLayout ll_fun_planning;
	
	private GraphicalView graph_business_electric;
	private GraphicalView graph_shopping_electric;
	private GraphicalView graph_leisure_electric;
	private GraphicalView graph_family_electric;
	private GraphicalView graph_holiday_electric;
	private GraphicalView graph_fun_electric;	
	
	LinearLayout ll_business_electric;
	LinearLayout ll_shopping_electric;
	LinearLayout ll_leisure_electric;
	LinearLayout ll_family_electric;
	LinearLayout ll_holiday_electric;
	LinearLayout ll_fun_electric;
	
	
	private GraphicalView graph_business_duration;
	private GraphicalView graph_shopping_duration;
	private GraphicalView graph_leisure_duration;
	private GraphicalView graph_family_duration;
	private GraphicalView graph_holiday_duration;
	private GraphicalView graph_fun_duration;
	
	LinearLayout ll_business_duration;
	LinearLayout ll_shopping_duration;
	LinearLayout ll_leisure_duration;
	LinearLayout ll_family_duration;
	LinearLayout ll_holiday_duration;
	LinearLayout ll_fun_duration;
	
	
	private GraphicalView graph_business_distance;
	private GraphicalView graph_shopping_distance;
	private GraphicalView graph_leisure_distance;
	private GraphicalView graph_family_distance;
	private GraphicalView graph_holiday_distance;
	private GraphicalView graph_fun_distance;
	
	
	LinearLayout ll_business_distance;
	LinearLayout ll_shopping_distance;
	LinearLayout ll_leisure_distance;
	LinearLayout ll_family_distance;
	LinearLayout ll_holiday_distance;
	LinearLayout ll_fun_distance;
	
	TextView tv_business_duration_min;
	TextView tv_business_duration_mean;
	TextView tv_business_duration_max;
	
	TextView tv_business_distance_min;
	TextView tv_business_distance_mean;
	TextView tv_business_distance_max;
	
	TextView tv_shopping_duration_min;
	TextView tv_shopping_duration_mean;
	TextView tv_shopping_duration_max;
	
	TextView tv_shopping_distance_min;
	TextView tv_shopping_distance_mean;
	TextView tv_shopping_distance_max;
	
	TextView tv_leisure_duration_min;
	TextView tv_leisure_duration_mean;
	TextView tv_leisure_duration_max;
	
	TextView tv_leisure_distance_min;
	TextView tv_leisure_distance_mean;
	TextView tv_leisure_distance_max;
	
	TextView tv_family_duration_min;
	TextView tv_family_duration_mean;
	TextView tv_family_duration_max;
	
	TextView tv_family_distance_min;
	TextView tv_family_distance_mean;
	TextView tv_family_distance_max;
	
	TextView tv_holiday_duration_min;
	TextView tv_holiday_duration_mean;
	TextView tv_holiday_duration_max;
	
	TextView tv_holiday_distance_min;
	TextView tv_holiday_distance_mean;
	TextView tv_holiday_distance_max;
	
	TextView tv_fun_duration_min;
	TextView tv_fun_duration_mean;
	TextView tv_fun_duration_max;
	
	TextView tv_fun_distance_min;
	TextView tv_fun_distance_mean;
	TextView tv_fun_distance_max;
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance_purposes);
		
		setLayouts();
		
		con = this;

		driveDetails = new ArrayList<DrivesDetails>();
		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Infos laden...");
    	dialog.setMessage("Infos werden geladen.");
    	

		dd_business = new ArrayList<DrivesDetails>();
		dd_family = new ArrayList<DrivesDetails>();
		dd_fun = new ArrayList<DrivesDetails>();
		dd_holiday = new ArrayList<DrivesDetails>();
		dd_leisure = new ArrayList<DrivesDetails>();
		dd_shopping = new ArrayList<DrivesDetails>();
	}


	public void onResume()
	{
    	super.onResume();
    	
    	
		dialog.show();
		
		new Thread(new Runnable() {

			public void run()
			{
				//android.os.Debug.waitForDebugger();
		        
		    	userName = settings.getString(Preferences.cUsername,null);
		    	password = settings.getString(Preferences.cPass, null);
				
		    	
		    	serverHelper = ServerHelper.getInstance(con);
				serverHelper = new ServerHelper(url, userName, null);

				
				if( isOnline() )
				{
					driveDetails = serverHelper.getDriveInfos(prefs.getString("UserID", ""));
					
					
					for (DrivesDetails dd : driveDetails)
					{
						if ( (dd.getDistance() > 0) && (dd.getDuration_rec() < 1440*60*1000) ) // only tracks with actual distance and shorter than 24 hours
						{
							switch (dd.getPurpose())
							{
								case 0:
									dd_business.add(dd);
									break;
								case 1:
									dd_shopping.add(dd);
									break;
								case 2:
									dd_leisure.add(dd);
									break;
								case 3:
									dd_family.add(dd);
									break;
								case 4:
									dd_holiday.add(dd);
									break;
								case 5:
									dd_fun.add(dd);
									break;
								default:
									break;
							}
						}						
					}

			    	dialog.dismiss();
			    	
			    	Handler handler = new Handler(Looper.getMainLooper());
			    	handler.post(new Runnable()
			    	{
			    		@Override
						public void run()
						{
			    			if ( !dd_business.isEmpty() )
			    			{
			    				drawPieChart(graph_business_planning, dd_business, ll_business_planning, 0);
			    				drawPieChart(graph_business_electric, dd_business, ll_business_electric, 1);
				    			setMMMValues(dd_business, tv_business_duration_min, tv_business_duration_mean, tv_business_duration_max, 0);
				    			setMMMValues(dd_business, tv_business_distance_min, tv_business_distance_mean, tv_business_distance_max, 1);
			    			}
			    			if ( !dd_family.isEmpty() )
			    			{
			    				drawPieChart(graph_family_planning, dd_family, ll_family_planning, 0);
			    				drawPieChart(graph_family_electric, dd_family, ll_family_electric, 1);
				    			setMMMValues(dd_family, tv_family_duration_min, tv_family_duration_mean, tv_family_duration_max, 0);
				    			setMMMValues(dd_family, tv_family_distance_min, tv_family_distance_mean, tv_family_distance_max, 1);
			    			}
			    			if ( !dd_fun.isEmpty() )
			    			{
			    				drawPieChart(graph_fun_planning, dd_fun, ll_fun_planning, 0);
			    				drawPieChart(graph_fun_electric, dd_fun, ll_fun_electric, 1);
				    			setMMMValues(dd_fun, tv_fun_duration_min, tv_fun_duration_mean, tv_fun_duration_max, 0);
				    			setMMMValues(dd_fun, tv_fun_distance_min, tv_fun_distance_mean, tv_fun_distance_max, 1);
			    			}
			    			if ( !dd_holiday.isEmpty() )
			    			{
			    				drawPieChart(graph_holiday_planning, dd_holiday, ll_holiday_planning, 0);
			    				drawPieChart(graph_holiday_electric, dd_holiday, ll_holiday_electric, 1);
				    			setMMMValues(dd_holiday, tv_holiday_duration_min, tv_holiday_duration_mean, tv_holiday_duration_max, 0);
				    			setMMMValues(dd_holiday, tv_holiday_distance_min, tv_holiday_distance_mean, tv_holiday_distance_max, 1);
			    			}
			    			if ( !dd_leisure.isEmpty() )
			    			{
			    				drawPieChart(graph_leisure_planning, dd_leisure, ll_leisure_planning, 0);
			    				drawPieChart(graph_leisure_electric, dd_leisure, ll_leisure_electric, 1);
				    			setMMMValues(dd_leisure, tv_leisure_duration_min, tv_leisure_duration_mean, tv_leisure_duration_max, 0);
				    			setMMMValues(dd_leisure, tv_leisure_distance_min, tv_leisure_distance_mean, tv_leisure_distance_max, 1);
			    			}
			    			if ( !dd_shopping.isEmpty() )
			    			{
			    				drawPieChart(graph_shopping_planning, dd_shopping, ll_shopping_planning, 0);
			    				drawPieChart(graph_shopping_electric, dd_shopping, ll_shopping_electric, 1);
				    			setMMMValues(dd_shopping, tv_shopping_duration_min, tv_shopping_duration_mean, tv_shopping_duration_max, 0);
				    			setMMMValues(dd_shopping, tv_shopping_distance_min, tv_shopping_distance_mean, tv_shopping_distance_max, 1);
			    			}
			    			
			    			
						}

						 
			    	});
			    	
			    	
				}}}).start();
	}
	
	public void setMMMValues(List<DrivesDetails> list, TextView min, TextView mean, TextView max, int which)
	{
		String min_val = "";
		String max_val = "";
		String mean_val = "";

		DecimalFormat df = new DecimalFormat("#.##");      // 2 Stellen
		
		switch (which)
		{
		case 0:	// duration
			long min_dur_temp = 0;
			long max_dur_temp = 0;
			long mean_dur_temp = 0;
			
			for (DrivesDetails pd : list)
			{
				mean_dur_temp = mean_dur_temp + pd.getDuration_rec();
				
				if( pd.getDuration_rec() < min_dur_temp )
				{
					min_dur_temp = pd.getDuration_rec();
				}
				else if( pd.getDuration_rec() > max_dur_temp )
				{
					max_dur_temp = pd.getDuration_rec();
				}
			}
    		
			mean_val = String.valueOf(df.format(mean_dur_temp / list.size() / 1000 / 60));
			min_val = String.valueOf(df.format(min_dur_temp / 1000 / 60));
			max_val = String.valueOf(df.format(max_dur_temp / 1000 / 60));

			min.setText(min_val + " min");
			mean.setText(mean_val + " min");
			max.setText(max_val + " min");
			break;
		case 1:	// distance
			Double min_dis_temp = 0.0;
			Double max_dis_temp = 0.0;
			Double mean_dis_temp = 0.0;
			
			for (DrivesDetails pd : list)
			{
				mean_dis_temp = mean_dis_temp + pd.getDistance();
				
				if( pd.getDistance() < min_dis_temp )
				{
					min_dis_temp = pd.getDistance();
				}
				else if( pd.getDistance() > max_dis_temp )
				{
					max_dis_temp = pd.getDistance();
				}
			}
			mean_val = String.valueOf(df.format(mean_dis_temp / list.size() / 1000));
			min_val = String.valueOf(df.format(min_dis_temp / 1000));
			max_val = String.valueOf(df.format(max_dis_temp / 1000));

			min.setText(min_val + " km");
			mean.setText(mean_val + " km");
			max.setText(max_val + " km");
			break;
		case 2:	// start_time
		default:
			break;
		}
		
	}
	
	public void drawPieChart(GraphicalView graph, List<DrivesDetails> list, LinearLayout ll, int which)
	{
		if (graph == null)
		{
			graph = ChartFactory.getPieChartView(con, getCatSeries(list, which), getDefaultRenderer(which));
			ll.addView(graph);
		}
		else
		{
			graph.repaint(); // use this whenever data has changed and you want to redraw
		}
	}
	
	public void drawLineChart(GraphicalView graph, List<DrivesDetails> list, LinearLayout ll, int which)
	{
    	
		switch (which)
		{
		case 0:	// duration
			Collections.sort(list,new Comparator<DrivesDetails>(){
	    		  public int compare(DrivesDetails pd1, DrivesDetails pd2) {
	    			  
	    			    return pd1.getDuration_rec().compareTo(pd2.getDuration_rec());
	    			  }
	    			});
		case 1:	// distance
			Collections.sort(list,new Comparator<DrivesDetails>(){
	    		  public int compare(DrivesDetails pd1, DrivesDetails pd2) {
	    			  
	    			    return pd1.getDistance().compareTo(pd2.getDistance());
	    			  }
	    			});
		case 2:	// start_time
			Collections.sort(list,new Comparator<DrivesDetails>(){
	    		  public int compare(DrivesDetails pd1, DrivesDetails pd2) {
	    			  
	    			    return pd1.getStart_rec().compareTo(pd2.getStart_rec());	// TODO: Tageszeit u.ä. auslesen
	    			  }
	    			});
		default:
			break;
		}
		
		
		if (graph == null)
		{
			graph = ChartFactory.getLineChartView(con, getXYMultData(list, which), getXYMultRenderer());
			ll.addView(graph);
		}
		else
		{
			graph.repaint(); // use this whenever data has changed and you want to redraw
		}
	}
	
	
	// Helper for Graphics
	public XYMultipleSeriesDataset getXYMultData(List<DrivesDetails> pd_list, int which)
	{    
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
	    XYSeries dataSeries = new XYSeries("series");
	    
	    int i = 0;
		for (DrivesDetails pd : pd_list)
		{
			switch (which)
			{
			case 0:	// Duration
				dataSeries.add(i, pd.getDuration_rec());	// min
				break;
			case 1: // Distance
				dataSeries.add(i, pd.getDistance() / 1000);	// km
				break;
			default:
				break;
			}
			i = i + 1;
		}
	    
		dataset.addSeries(dataSeries);
		
	    return dataset;
	}
	
	public XYMultipleSeriesRenderer getXYMultRenderer()
	{    
	    XYSeriesRenderer r = new XYSeriesRenderer();
	    r.setColor(Color.parseColor("#006ab3"));
        r.setLineWidth(2);        
        
//        r.setPointStyle(PointStyle.SQUARE); // CIRCLE, DIAMOND , POINT, TRIANGLE, X
//        r.setFillPoints(true); // not for point or x
        // don't know how to set point size or point color
//        r.setFillBelowLine(true);
//        r.setFillBelowLineColor(Color.WHITE);

    	XYMultipleSeriesRenderer renderer_speed = new XYMultipleSeriesRenderer(); 
    	renderer_speed.addSeriesRenderer(r);

		// set some properties on the main renderer
		renderer_speed.setApplyBackgroundColor(false);
		renderer_speed.setBackgroundColor(Color.WHITE);
//		renderer_speed.setAxisTitleTextSize(16);
//		renderer_speed.setChartTitleTextSize(20);
//		renderer_speed.setLabelsTextSize(15);
//		renderer_speed.setLegendTextSize(15);
		renderer_speed.setMargins(new int[] { 0, 0, 0, 0 });
//		renderer_speed.setZoomButtonsVisible(true);
		renderer_speed.setPointSize(5);
		renderer_speed.setShowLabels(false);
		renderer_speed.setShowLegend(false);
		renderer_speed.setPanEnabled(false);
		renderer_speed.setZoomEnabled(false);
		
    	
        return renderer_speed;
	}

	
	public CategorySeries getCatSeries(List<DrivesDetails> pd_list, int which)
	{    
		CategorySeries dataSeries = new CategorySeries("");
	    
		switch(which)
		{
			case 0:	// planning
				int i = 0;
				int planned = 0;
				int spontaneous = 0;
				for (DrivesDetails dd : pd_list)
				{			
					if (dd.getPlanning() == 1)
					{
						planned = planned + 1;
					}
					else if (dd.getPlanning() == 0)
					{
						spontaneous = spontaneous + 1;
					}
					i = i + 1;
				}
				dataSeries.add("geplant", planned);
				dataSeries.add("spontan", spontaneous);
				break;
				
			case 1: // electric
				int ii = 0;
				int electric = 0;
				int conventional = 0;
				int gas = 0;
				for (DrivesDetails dd : pd_list)
				{			
					if (dd.getElectric() == 0)
					{
						electric = electric + 1;
					}
					else if (dd.getElectric() == 1)
					{
						conventional = conventional + 1;
					}
					else if (dd.getElectric() == 2)
					{
						gas = gas + 1;
					}
					ii = ii + 1;
				}
				dataSeries.add("elektrisch", electric);
				dataSeries.add("konventionell", conventional);
				dataSeries.add("gas-betrieben", gas);
				break;
			default:
				break;
		}
		
	    return dataSeries;
	}
	
	public DefaultRenderer getDefaultRenderer(int which)
	{            
		DefaultRenderer defaultRenderer = new DefaultRenderer();
		
		switch(which)
		{
			case 0:	// planning
				for (int color : COLORS_CHART_PLANNING)
				{
					SimpleSeriesRenderer simpleRenderer = new SimpleSeriesRenderer();
					simpleRenderer.setColor(color);
					defaultRenderer.addSeriesRenderer(simpleRenderer);
				}
				break;
				
			case 1: // electric
				for (int color : COLORS_CHART_ELECTRIC)
				{
					SimpleSeriesRenderer simpleRenderer = new SimpleSeriesRenderer();
					simpleRenderer.setColor(color);
					defaultRenderer.addSeriesRenderer(simpleRenderer);
				}
				break;
				
			default:
				break;
		}

		
		defaultRenderer.setStartAngle(180);
		defaultRenderer.setApplyBackgroundColor(false);
		defaultRenderer.setBackgroundColor(Color.WHITE);
		defaultRenderer.setMargins(new int[] { 0, 0, 0, 0 });
		defaultRenderer.setShowLabels(false);
		defaultRenderer.setShowLegend(false);
		defaultRenderer.setPanEnabled(false);
		defaultRenderer.setZoomEnabled(false);
		
		return defaultRenderer;
	}

	
	
	public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

	    if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	
	
	private void setLayouts()
	{
		ll_business_planning = (LinearLayout) findViewById(R.id.chart_business_1);
		ll_family_planning = (LinearLayout) findViewById(R.id.chart_family_1);
		ll_fun_planning = (LinearLayout) findViewById(R.id.chart_fun_1);
		ll_holiday_planning = (LinearLayout) findViewById(R.id.chart_holiday_1);
		ll_leisure_planning = (LinearLayout) findViewById(R.id.chart_leisure_1);
		ll_shopping_planning = (LinearLayout) findViewById(R.id.chart_shopping_1);
		
		ll_business_electric = (LinearLayout) findViewById(R.id.chart_business_2);
		ll_family_electric = (LinearLayout) findViewById(R.id.chart_family_2);
		ll_fun_electric = (LinearLayout) findViewById(R.id.chart_fun_2);
		ll_holiday_electric = (LinearLayout) findViewById(R.id.chart_holiday_2);
		ll_leisure_electric = (LinearLayout) findViewById(R.id.chart_leisure_2);
		ll_shopping_electric = (LinearLayout) findViewById(R.id.chart_shopping_2);
		
//		ll_business_distance = (LinearLayout) findViewById(R.id.chart_business_3);
//		ll_family_distance = (LinearLayout) findViewById(R.id.chart_family_3);
//		ll_fun_distance = (LinearLayout) findViewById(R.id.chart_fun_3);
//		ll_holiday_distance = (LinearLayout) findViewById(R.id.chart_holiday_3);
//		ll_leisure_distance = (LinearLayout) findViewById(R.id.chart_leisure_3);
//		ll_shopping_distance = (LinearLayout) findViewById(R.id.chart_shopping_3);
		
		tv_business_duration_max = (TextView) findViewById(R.id.duration_max_business);
		tv_business_distance_max = (TextView) findViewById(R.id.distance_max_business);
		tv_business_duration_min = (TextView) findViewById(R.id.duration_min_business);
		tv_business_distance_min = (TextView) findViewById(R.id.distance_min_business);
		tv_business_duration_mean = (TextView) findViewById(R.id.duration_mean_business);
		tv_business_distance_mean = (TextView) findViewById(R.id.distance_mean_business);
		
		tv_shopping_duration_max = (TextView) findViewById(R.id.duration_max_shopping);
		tv_shopping_distance_max = (TextView) findViewById(R.id.distance_max_shopping);
		tv_shopping_duration_min = (TextView) findViewById(R.id.duration_min_shopping);
		tv_shopping_distance_min = (TextView) findViewById(R.id.distance_min_shopping);
		tv_shopping_duration_mean = (TextView) findViewById(R.id.duration_mean_shopping);
		tv_shopping_distance_mean = (TextView) findViewById(R.id.distance_mean_shopping);
		
		tv_leisure_duration_max = (TextView) findViewById(R.id.duration_max_leisure);
		tv_leisure_distance_max = (TextView) findViewById(R.id.distance_max_leisure);
		tv_leisure_duration_min = (TextView) findViewById(R.id.duration_min_leisure);
		tv_leisure_distance_min = (TextView) findViewById(R.id.distance_min_leisure);
		tv_leisure_duration_mean = (TextView) findViewById(R.id.duration_mean_leisure);
		tv_leisure_distance_mean = (TextView) findViewById(R.id.distance_mean_leisure);
		
		tv_family_duration_max = (TextView) findViewById(R.id.duration_max_family);
		tv_family_distance_max = (TextView) findViewById(R.id.distance_max_family);
		tv_family_duration_min = (TextView) findViewById(R.id.duration_min_family);
		tv_family_distance_min = (TextView) findViewById(R.id.distance_min_family);
		tv_family_duration_mean = (TextView) findViewById(R.id.duration_mean_family);
		tv_family_distance_mean = (TextView) findViewById(R.id.distance_mean_family);
		
		tv_holiday_duration_max = (TextView) findViewById(R.id.duration_max_holiday);
		tv_holiday_distance_max = (TextView) findViewById(R.id.distance_max_holiday);
		tv_holiday_duration_min = (TextView) findViewById(R.id.duration_min_holiday);
		tv_holiday_distance_min = (TextView) findViewById(R.id.distance_min_holiday);
		tv_holiday_duration_mean = (TextView) findViewById(R.id.duration_mean_holiday);
		tv_holiday_distance_mean = (TextView) findViewById(R.id.distance_mean_holiday);
		
		tv_fun_duration_max = (TextView) findViewById(R.id.duration_max_fun);
		tv_fun_distance_max = (TextView) findViewById(R.id.distance_max_fun);
		tv_fun_duration_min = (TextView) findViewById(R.id.duration_min_fun);
		tv_fun_distance_min = (TextView) findViewById(R.id.distance_min_fun);
		tv_fun_duration_mean = (TextView) findViewById(R.id.duration_mean_fun);
		tv_fun_distance_mean = (TextView) findViewById(R.id.distance_mean_fun);
	}
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance_purposes, menu);
		return true;
	}
	
	@Override
    public void onBackPressed()
    {
    	Intent intent = new Intent(con, MainPage.class);

		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		startActivity(intent);
    }

}
