package de.tum.sun2car.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.footprint.FootprintCalc;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;
import de.tum.sun2car.tumsensorrecorderlib.SensorService;
import de.tum.sun2car.tumsensorrecorderlib.UploadService;

public class SyncSun2CarService extends Service {

  private static final String TAG = "TstSnsrServiceActivity";

	private LocationDB locsSource;
	private DrivesDB drivesSource;
	private FamilyDB familySource;
	
	List<Drives> drivesServer;
	List<Drives> drivesDB;
	List<String> members;
	List<MyLocation> locs;

  private String userName;
  private String token;
  private ServerHelper ServerHelper;
  private String ServerUrl = "http://129.187.64.247/";

	Context con;
	
	private SharedPreferences settings;
	private SharedPreferences prefs;
	
	private final IBinder mBinder = new SyncBinder();	
	public static Boolean mIsRunning = false;
	public static Boolean mIsStarted = false;
	//private FeedbackServerManager mServerManager;	

  /**
   * Class used for the client Binder.  Because we know this service always
   * runs in the same process as its clients, we don't need to deal with IPC.
   */
  public class SyncBinder extends Binder {
    public SyncSun2CarService getService() {
      return SyncSun2CarService.this;
    }
  }

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onCreate() {
		//mServerManager = FeedbackServerManager.getInstance(this);		
		mIsRunning = false;
		mIsStarted = true;
		
		drivesServer = new ArrayList<Drives>();
		drivesDB = new ArrayList<Drives>();
		locs = new ArrayList<MyLocation>();
		members = new ArrayList<String>();

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);

		
		con = this;
	}

	@Override
  public void onDestroy() {
    super.onDestroy();
    Toast.makeText(this, "Synchronisierung gestoppt", Toast.LENGTH_LONG).show();
    mIsRunning = false;
    mIsStarted = false;
  }


  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
//		android.os.Debug.waitForDebugger();
    Toast.makeText(this, "Synchronisierung gestartet", Toast.LENGTH_LONG).show();
    Vibrator v = (Vibrator) con.getSystemService(Context.VIBRATOR_SERVICE);
    // Vibrate for 500 milliseconds
    v.vibrate(100);
    mIsRunning = true;

    new Thread(new Runnable() {
      public void run() {
        checkSyncData();
      }
    }).start();
    return START_STICKY;
  }

  public void syncDB() {
    while (mIsRunning) {
      try {

      } catch (Exception e) {
        this.stopSelf();
      }
    }
  }

  public boolean checkSyncData() {

    locsSource = new LocationDB(this);
    locsSource.open();

    drivesSource = new DrivesDB(this);
    drivesSource.open();

    familySource = new FamilyDB(this);
    familySource.open();

    userName = settings.getString(Preferences.cUsername, null);
    token = settings.getString(Preferences.cToken, null);
    ServerHelper = new ServerHelper(ServerUrl, userName, token);

    if (isOnline()) {
      /**
       * Tracks-Sync
       */
      members = ServerHelper.getFamilyMembers(prefs.getString("FamilyID", ""));

      for (int i = 0; i < members.size(); i++) {
        if (!familySource.checkForMember(members.get(i))) {
          int driver = ServerHelper.getDriver(members.get(i));
          familySource.addMember(members.get(i), driver, prefs.getString("FamilyID", ""));
        }
      }
      drivesServer = ServerHelper.getFamilyTracks(prefs.getString("FamilyID", ""));
			Collections.sort(drivesServer, Collections.reverseOrder());
			List<Double> tracksServer = new ArrayList<Double>();

      for (int i = 0; i < drivesServer.size(); i++) {
        Double trackID = drivesServer.get(i).getTrackID();
        tracksServer.add(trackID);
        Double userID = drivesServer.get(i).getUserID();

        if (!drivesSource.checkForDrive(userID, trackID)) {
          drivesSource.addDrive(drivesServer.get(i));
        }
      }

      for (int i = 0; i < members.size(); i++) {
        drivesDB.addAll(drivesSource.getAllDrivesByUser(members.get(i)));
      }

      for (int i = 0; i < drivesDB.size(); i++) {
        Double track_DB = drivesDB.get(i).getTrackID();
        if (!tracksServer.contains(track_DB)) {
          drivesSource.removeDriveWithTrackID(con, track_DB);
        }
      }


      for (int i = 0; i < drivesServer.size(); i++) {
        Double trackID = drivesServer.get(i).getTrackID();

        if (locsSource.getAllLocationsByTrack(trackID).isEmpty()) {
          locs = ServerHelper.getLocations(trackID);
          Collections.sort(locs);
          for (int j = 0; j < locs.size(); j++) {
            // write new Locations to DB
            locsSource.addLocation(locs.get(j));
          }
        }
      }

			/*
			 * calc occupation ratio
			 * calc co2 emissions
			 */
      for (int i = 0; i < drivesServer.size(); i++) {
        Double trackID = drivesServer.get(i).getTrackID();
        locs = locsSource.getAllLocationsByTrack(trackID);

        List<Drives> similarDrives = drivesSource.getSimilarTimeDrives(drivesServer.get(i));

        if (similarDrives.size() > 1) {
          List<List<MyLocation>> locations = new ArrayList<List<MyLocation>>();
//					locations.add(locs);

          for (int j = 0; j < similarDrives.size(); j++) {
            List<MyLocation> track = locsSource.getAllLocationsByTrack(similarDrives.get(j).getTrackID());

            locations.add(track);
          }
          int[] occupation = FootprintCalc.getOccupationRatio(locations);
          for (int j = 0; j < similarDrives.size(); j++) {
            drivesSource.updateDriveOccupation(similarDrives.get(j), occupation[j]);
          }
        }

        List<List<Double>> trackdata = FootprintCalc.getCalcList(locs);

        Double co2Trip = FootprintCalc.calculateCO2Trip(trackdata.get(1), trackdata.get(2), trackdata.get(3), drivesServer.get(i).getTrip_mot(), 0.0);

        drivesSource.updateDriveCO2(drivesServer.get(i).getTrackID(), co2Trip);
        drivesSource.updateDriveDistance(drivesServer.get(i), trackdata.get(3));

        // update emissions in server DB
        // Änderung des aufrufs, da das Passwort entfernt wurde
        String successCO2 = ServerHelper.setCO2Emission(co2Trip, trackID);
        // String successCO2 = serverHelper.setCO2Emission(password, co2Trip, trackID);
      }

      /**
			 * SmartMeter-Sync
			 */
			String version = null;
			
			File sdcard = Environment.getExternalStorageDirectory();
			File file = new File(sdcard + "/sun2car/NewCSV/version.txt");

      if (file.exists()) {
        try {
          BufferedReader br = new BufferedReader(new FileReader(file));
          String line;

          while ((line = br.readLine()) != null) {
            version = line;
          }
        } catch (IOException e) {
          //You'll need to add proper error handling here
        }
      } else {
        version = "0";
      }

      String newVersion = null;
			try {
				newVersion = new checkForCSVUpdateTask().execute(ServerHelper).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    		

//			smarthomeSource.removeValues();
//			smartloadingSource.removeValues();
//			smartpvSource.removeValues();

      Double d_current = Double.parseDouble(version);
      Double d_new = Double.parseDouble(newVersion);
      if (d_new > d_current)  // !newVersion.equals(version)
      {
        fetchData(newVersion);
      }

      Handler handler = new Handler(Looper.getMainLooper());
	    	handler.post(new Runnable()
	    	{

				@Override
				public void run()
				{
					Toast.makeText(con, "Synchronisierung erfolgreich abgeschlossen", Toast.LENGTH_LONG).show();
					Vibrator v = (Vibrator) con.getSystemService(Context.VIBRATOR_SERVICE);
					// Vibrate for 500 milliseconds
					v.vibrate(500);
					mIsRunning = false;
					
					/**
					 * check whether SensorRecording is running and then start UploadManager
					 */
					// check whether sync-service is running at the moment
			        boolean running = SensorService.getIsRunning();
			        
			    	boolean serviceRunning = false;
			    	ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
			        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
			        {
			            if (SensorService.class.getName().equals(service.service.getClassName()))
			            {
			            	serviceRunning =  true;
			            }
			        }
          if (running) {
            // upload
            // TODO: Auswahl synchroner Upload oder Upload erst später!?
            Log.i(TAG, "onClick: starting UploadService");
            startService(new Intent(con, UploadService.class));
          }

//					File direct = new File(Environment.getExternalStorageDirectory() + "/sun2car");
//					
//			        if(!direct.exists())
//			         {
//			             if(direct.mkdir()) 
//			               {
//			                //directory is created;
//			               }
//			
//			         }
//			        
//					try {
//			            File sd = Environment.getExternalStorageDirectory();
//			            File data = Environment.getDataDirectory();
//			
//			            if (sd.canWrite()) {
//			                String  currentDBPath= "//data//" + "de.tum.sun2car"
//			                        + "//databases//" + "sun2car.db";
//			                String backupDBPath  = "/sun2car/sun2car.db";
//			                File currentDB = new File(data, currentDBPath);
//			                File backupDB = new File(sd, backupDBPath);
//			
//			                FileChannel src = new FileInputStream(currentDB).getChannel();
//			                FileChannel dst = new FileOutputStream(backupDB).getChannel();
//			                dst.transferFrom(src, 0, src.size());
//			                src.close();
//			                dst.close();
//			                Toast.makeText(getBaseContext(), backupDB.toString(),
//			                        Toast.LENGTH_LONG).show();
//			
//			            }
//			        } catch (Exception e) {
//			
//			            Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
//			                    .show();
//			
//			        }
				} 
	    	});
		} else {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {

        @Override
        public void run() {
          Toast.makeText(con, "Keine Verbindung zum Server", Toast.LENGTH_LONG).show();
          mIsRunning = false;
        }
      });
    }
    return false;
	}


  String[] smvFiles = {"Home", "Loading", "PV"};
  private void fetchData(final String newVersion) {
    new Thread(new Runnable() {
      public void run() {
        //3x Durchlauf für 3 CSV
        for (int i = 0; i < smvFiles.length; i++) {
          settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
          String storedUsername = settings.getString(Preferences.cUsername, null);
          String storedToken = settings.getString(Preferences.cToken, null);
          ServerHelper = new ServerHelper(ServerUrl, storedUsername, storedToken);
          String familyID = prefs.getString("FamilyID", "");
          ServerHelper.getNewConsumption(con, familyID, i);
        }
        writeDownloadedVersion(newVersion);
      }
    }).start();
  }

  protected void writeDownloadedVersion(String newVersion) {
    File sdcard = Environment.getExternalStorageDirectory();
    File file = new File(sdcard + "/sun2car/NewCSV/version.txt");
    try {
      BufferedWriter buf_writer = new BufferedWriter(new FileWriter(file));
      buf_writer.write(newVersion);
      buf_writer.flush();
      buf_writer.close();
      toString();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  public boolean isOnline() {
    ConnectivityManager connectivityManager
            = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
  }

}

class checkForCSVUpdateTask extends AsyncTask<ServerHelper, Void, String> {
  private Exception exception;
  boolean success;
  ServerHelper serverHelper;

  protected String doInBackground(ServerHelper... param) {
    serverHelper = param[0];
    return serverHelper.getCSVVersion();
  }
}

/**
 * Start Service in Activity
 */
//public void startServices(){
//	//startService(new Intent(this, FeedbackService.class));			
//	//startService(new Intent(this,TrackAnalyzerService.class));
//	Intent iDBSyncService = new Intent(this, DBSyncService.class);
//	PendingIntent piDBSyncService = PendingIntent.getService(this, 0, iDBSyncService, PendingIntent.FLAG_UPDATE_CURRENT);
//	AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//	alarmManager.cancel(piDBSyncService);
//	alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 30*60*1000, piDBSyncService);
//}
