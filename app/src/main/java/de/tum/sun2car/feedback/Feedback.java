package de.tum.sun2car.feedback;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;
import de.tum.sun2car.R;
import de.tum.sun2car.info.InfoAccount;
import de.tum.sun2car.info.InfoProject;

@SuppressWarnings("deprecation")
public class Feedback extends TabActivity
{

	private TabHost tabHost;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_feedback);
		
		
		tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    /**
	     * Tab Voice
	     */
	    // set Tab Text
		final TextView tvVoice = new TextView(this);
		tvVoice.setText(R.string.feedback_voice);
		tvVoice.setTextColor(Color.WHITE);
		tvVoice.setTextSize(15);
		tvVoice.setPadding(2, 20, 2, 20);
		tvVoice.setGravity(25);

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, FeedbackVoice.class);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("voice").setIndicator(tvVoice)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    /**
	     * Tab Write
	     */
	    // set Tab Text
	    final TextView tvWrite = new TextView(this);
	    tvWrite.setText(R.string.feedback_write);
	    tvWrite.setTextColor(Color.parseColor("#006ab3"));
	    tvWrite.setTextSize(15);
	    tvWrite.setPadding(2, 20, 2, 20);
	    tvWrite.setGravity(25);
		
	    intent = new Intent().setClass(this, FeedbackWrite.class);
	    spec = tabHost.newTabSpec("write").setIndicator(tvWrite)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(0);
	    
	    // current tab: blue, other: white
	    for(int j = 0; j < tabHost.getTabWidget().getChildCount(); j++)
	    {
	        tabHost.getTabWidget().getChildAt(j).setBackgroundColor(Color.WHITE);
	    }
	    tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
    
	    
	    
	    tabHost.setOnTabChangedListener(new OnTabChangeListener()
			{
			  public void onTabChanged(String arg0)
			   	{
				   for(int i = 0; i < tabHost.getTabWidget().getChildCount(); i++)
				    {
				        tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
				    }
		        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
		        if (tabHost.getCurrentTab() == 0)
		        	{
		        		tvVoice.setTextColor(Color.WHITE);
		        		tvWrite.setTextColor(Color.parseColor("#006ab3"));
		        	}
		        else
			        {
		        		tvVoice.setTextColor(Color.parseColor("#006ab3"));
		        		tvWrite.setTextColor(Color.WHITE);
		        	}
		        
			   	}     
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.feedback, menu);
		return true;
	}

}
