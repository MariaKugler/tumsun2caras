package de.tum.sun2car.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
 
public class SqliteDB extends SQLiteOpenHelper {
 
	private static final String TABLE_DRIVES = "table_drives";
	public static final String COL_DRIVES_TRACKID = "TRACK_ID";
	public static final String COL_DRIVES_USERID = "USER_ID";
	public static final String COL_DRIVES_START_LOC = "START_LOC";
	public static final String COL_DRIVES_STOP_LOC = "STOP_LOC";
	public static final String COL_DRIVES_START_TIME = "START_TIME";
	public static final String COL_DRIVES_STOP_TIME = "STOP_TIME";
	public static final String COL_DRIVES_LENGTH = "LENGTH";
	public static final String COL_DRIVES_MOT = "MOT";
	public static final String COL_DRIVES_OCCUPATION = "OCCUPATION";
	public static final String COL_DRIVES_EMISSION = "EMISSION";
	
	private static final String TABLE_LOCATION = "table_location";
	public static final String COL_LOCATION_LOCID = "LOC_ID";
	public static final String COL_LOCATION_TRACKID = "TRACK_ID";
	public static final String COL_LOCATION_LAT = "LAT";
	public static final String COL_LOCATION_LON = "LON";
	public static final String COL_LOCATION_ALT = "ALT";
	public static final String COL_LOCATION_TIME = "TIME";
	public static final String COL_LOCATION_SPEED = "SPEED";
	public static final String COL_LOCATION_HDOP = "HDOP";
	public static final String COL_LOCATION_COURSE = "COURSE";
	
	private static final String TABLE_FAMILY = "table_familymembers";
	public static final String COL_FAMILY_ID = "FAMILY_ID";
	public static final String COL_FAMILY_USERID = "USER_ID";
	public static final String COL_FAMILY_DRIVER = "DRIVER";
	
	private static final String TABLE_SMARTHOME = "table_smarthome";
	public static final String COL_SMARTHOME_ID = "SMARTHOME_ID";
	public static final String COL_SMARTHOME_TIMESTAMP = "TIMESTAMP";
	public static final String COL_SMARTHOME_ENERGY = "ENERGY";
	
	private static final String TABLE_SMARTLOADING= "table_smartloading";
	public static final String COL_SMARTLOADING_ID = "SMARTLOADING_ID";
	public static final String COL_SMARTLOADING_TIMESTAMP = "TIMESTAMP";
	public static final String COL_SMARTLOADING_ENERGY = "ENERGY";
	
	private static final String TABLE_SMARTPV = "table_smartpv";
	public static final String COL_SMARTPV_ID = "SMARTPV_ID";
	public static final String COL_SMARTPV_TIMESTAMP = "TIMESTAMP";
	public static final String COL_SMARTPV_ENERGY = "ENERGY";

 
	// Authors-Table
	private static final String CREATE_DRIVES_TABLE = "CREATE TABLE " + TABLE_DRIVES + " ("
			+ COL_DRIVES_TRACKID + " DOUBLE PRIMARY KEY, " 
			+ COL_DRIVES_USERID + " DOUBLE," 
			+ COL_DRIVES_START_LOC + " TEXT NOT NULL," 
			+ COL_DRIVES_STOP_LOC + " TEXT NOT NULL,"  
			+ COL_DRIVES_START_TIME + " TEXT NOT NULL,"  
			+ COL_DRIVES_STOP_TIME + " TEXT NOT NULL,"  
			+ COL_DRIVES_LENGTH + " TEXT NOT NULL,"  
			+ COL_DRIVES_MOT + " TEXT NOT NULL,"  
			+ COL_DRIVES_OCCUPATION + " NUMBER," 
			+ COL_DRIVES_EMISSION + " NUMBER" 
			+ ")";
	
	// Series-Table
	private static final String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "(" 
			+ COL_LOCATION_LOCID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COL_LOCATION_TRACKID + " DOUBLE,"
			+ COL_LOCATION_LAT + " DOUBLE," 
			+ COL_LOCATION_LON + " DOUBLE,"
			+ COL_LOCATION_ALT + " DOUBLE,"
			+ COL_LOCATION_TIME + " DATETIME,"
			+ COL_LOCATION_SPEED + " DOUBLE,"
			+ COL_LOCATION_HDOP + " DOUBLE,"
			+ COL_LOCATION_COURSE + " DOUBLE" 
			+ ")";
	
	
	// Family-Table
	private static final String CREATE_FAMILY_TABLE = "CREATE TABLE " + TABLE_FAMILY + "(" 
			+ COL_FAMILY_ID + " INTEGER,"
			+ COL_FAMILY_USERID + " DOUBLE PRIMARY KEY,"
			+ COL_FAMILY_DRIVER + " INTEGER"
			+ ")";
		
	// SmartHome-Table
	private static final String CREATE_SMARTHOME_TABLE = "CREATE TABLE " + TABLE_SMARTHOME + "(" 
			+ COL_SMARTHOME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COL_SMARTHOME_TIMESTAMP + " DATETIME,"
			+ COL_SMARTHOME_ENERGY + " DOUBLE"
			+ ")";
		
	// SmartLoading-Table
	private static final String CREATE_SMARTLOADING_TABLE = "CREATE TABLE " + TABLE_SMARTLOADING + "(" 
			+ COL_SMARTLOADING_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COL_SMARTLOADING_TIMESTAMP + " DATETIME,"
			+ COL_SMARTLOADING_ENERGY + " DOUBLE"
			+ ")";
		
	// SmartPV-Table
	private static final String CREATE_SMARTPV_TABLE = "CREATE TABLE " + TABLE_SMARTPV + "(" 
			+ COL_SMARTPV_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COL_SMARTPV_TIMESTAMP + " DATETIME,"
			+ COL_SMARTPV_ENERGY + " DOUBLE"
			+ ")";
		
		
 
	public SqliteDB(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
 
	@Override
	public void onCreate(SQLiteDatabase db)
	{		
		//Tabelle neu anlegen
		db.execSQL(CREATE_DRIVES_TABLE);
		db.execSQL(CREATE_LOCATION_TABLE);
		db.execSQL(CREATE_FAMILY_TABLE);

		db.execSQL(CREATE_SMARTHOME_TABLE);
		db.execSQL(CREATE_SMARTLOADING_TABLE);
		db.execSQL(CREATE_SMARTPV_TABLE);
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Hier sind beliebige Anfragen möglich (Hier Tabelle löschen und wieder neu erstellen)
		
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DRIVES);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAMILY);
		

		db.execSQL(CREATE_SMARTHOME_TABLE);
		db.execSQL(CREATE_SMARTLOADING_TABLE);
		db.execSQL(CREATE_SMARTPV_TABLE);
		
	}
 
}
