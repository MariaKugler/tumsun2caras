package de.tum.sun2car.newdrive;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import de.tum.sun2car.LoginActivity;
import de.tum.sun2car.MainPage;
import de.tum.sun2car.R;
import de.tum.sun2car.helper.NotificationHandler;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.helper.SyncSun2CarService;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;
import de.tum.sun2car.tumsensorrecorderlib.SensorService;
import de.tum.sun2car.tumsensorrecorderlib.UploadService;

public class NewDrive extends FragmentActivity implements LocationListener{

  private ToggleButton btn_recording;
  private ImageButton btn_changeUser;
  boolean mSensorIsStarted = false;
  private TextView tv_login;

  private NotificationHandler mNotificatonHandler;
  private static final String TAG = "TstSnsrServiceActivity";
  private SharedPreferences settings;

  public Context con;
  Bundle b;
  String pw;

  int purpose;
  int planning;
  int electric;

  GoogleMap map;
  Criteria criteria;
  Location curr_loc;
  Double longitude;
  Double latitude;
  LocationManager locationManager;
  String provider;

  PolylineOptions poly;
  //	LatLngBounds.Builder bc;
  List<LatLng> latLngs;

  long end_rec;
  long start_rec;
  Timestamp ts_start;
  long duration_rec;
  Double distance = 0.0;

  boolean wasRecording;

  private ServerHelper ServerHelper;
  private String ServerUrl = "http://129.187.64.247/";

  File[] matchingFiles_uploading;
  File dir;

	
	@Override
  protected void onCreate(Bundle savedInstanceState) {
    try {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.layout_newdrive);

      con = this;
      latLngs = new ArrayList<LatLng>();

      // get purpose-bundle
      b = getIntent().getExtras();
      purpose = b.getInt("drive_purpose");
      planning = b.getInt("drive_planning");
      electric = b.getInt("drive_electric");

      if (mSensorIsStarted) {
        start_rec = b.getLong("start_rec");
        wasRecording = true;
      } else {
        wasRecording = false;
      }

      // reading settings from shared preferences
      // creating new ServerHelper
      settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
      String storedUsername = settings.getString(Preferences.cUsername, null);
      String storedToken = settings.getString(Preferences.cToken, null);
      ServerHelper = new ServerHelper(ServerUrl, storedUsername, storedToken);              // initialize connection

      btn_recording = (ToggleButton) findViewById(R.id.btn_sensorRecorder);
      btn_changeUser = (ImageButton) findViewById(R.id.btn_changeAccount);
      tv_login = (TextView) findViewById(R.id.tv_user);

      // Getting Google Play availability status
      int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        /*MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

      map = ((SupportMapFragment) getSupportFragmentManager()
              .findFragmentById(R.id.map)).getMap();



      // Getting LocationManager object from System Service LOCATION_SERVICE
      locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

      //if GPS is disabled launch GPS settings...
      if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(myIntent);
      }

      // Creating a criteria object to retrieve provider
      criteria = new Criteria();

      // Getting the name of the best provider
      provider = locationManager.getBestProvider(criteria, true);

      // Getting Current Location
      curr_loc = locationManager.getLastKnownLocation(provider);

      if (curr_loc != null) {
        onLocationChanged(curr_loc);
      }

      locationManager.requestLocationUpdates(provider, 0, 0, this);


    } catch (Exception ex) {
      Log.i("cought unexpected ex", ex.toString());
    }
  }

	@Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.new_drive, menu);
    return true;
  }
	
	public void onResume() {
		try {
      super.onResume();
      con = this;
      settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
      String storedUsername = settings.getString(Preferences.cUsername, null);
      String storedToken = settings.getString(Preferences.cToken, null);
      ServerHelper = new ServerHelper(ServerUrl, storedUsername, storedToken);
      mNotificatonHandler = NotificationHandler.getInstance(this);
      pw = settings.getString(Preferences.cPass, null);

      if(storedUsername != null && !storedUsername.isEmpty()) {
        ServerHelper = ServerHelper.getInstance(this);
				ServerHelper = new ServerHelper(ServerUrl, storedUsername, storedToken);
        tv_login.setText(storedUsername);
        mSensorIsStarted = SensorService.getIsRunning();
        btn_recording.setChecked(mSensorIsStarted);

				// get purpose-bundle
				b = getIntent().getExtras();
				purpose = b.getInt("drive_purpose");
				planning = b.getInt("drive_planning");
				electric = b.getInt("drive_electric");

        if (mSensorIsStarted) {
          start_rec = b.getLong("start_rec");
          wasRecording = true;
        } else {
          wasRecording = false;
        }

        // handle drawable Toggle
        if (mSensorIsStarted)
          btn_recording.setBackgroundResource(R.drawable.end_newdrive);
        else
          btn_recording.setBackgroundResource(R.drawable.start_newdrive);

        // Creating a criteria object to retrieve provider
        criteria = new Criteria();
        // Getting the name of the best provider
        provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        curr_loc = locationManager.getLastKnownLocation(provider);

        if (curr_loc != null) {
          onLocationChanged(curr_loc);
        }
        locationManager.requestLocationUpdates(provider, 0, 0, this);

      } else {
        logout();
      }

		} catch(Exception ex) {
			Log.i("cought unexpected exc",ex.toString());
		}
	}

  public synchronized void onRecorder(View v) {
    if (!btn_recording.isChecked()) {
      // recording
      Log.i(TAG, "onClick: stopping SensorService");
      stopService(new Intent(this, SensorService.class));
      // upload
      Log.i(TAG, "onClick: stopping UploadService");
      stopService(new Intent(this, UploadService.class));
//    		mNotificatonHandler.NotificationStop();
    } else {
      // recording
      Log.i(TAG, "onClick: starting SensorService");
      startService(new Intent(this, SensorService.class));

      // check whether sync-service is running at the moment
      SyncSun2CarService mService = new SyncSun2CarService();
      boolean running = mService.mIsRunning;

      boolean serviceRunning = false;
      ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
      for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
        if (SyncSun2CarService.class.getName().equals(service.service.getClassName())) {
          serviceRunning = true;
        }
      }
      if (running) {
        Toast.makeText(con, "Synchronisierung läuft derzeit.", Toast.LENGTH_LONG).show();
      } else {
        // upload
        // TODO: Auswahl synchroner Upload oder Upload erst später!?
        Log.i(TAG, "onClick: starting UploadService");
        startService(new Intent(this, UploadService.class));
//        		mNotificatonHandler.NotificationStart();
      }


      start_rec = System.currentTimeMillis();

      while (start_rec == 0.0) {
        start_rec = System.currentTimeMillis();
      }
      writeDriveInfosFileRecording(pw, purpose, planning, electric, start_rec);
    }


    mSensorIsStarted = !mSensorIsStarted;

    // handle drawable Toggle
    if (mSensorIsStarted)
      btn_recording.setBackgroundResource(R.drawable.end_newdrive);
    else
      btn_recording.setBackgroundResource(R.drawable.start_newdrive);


    // handle DrivesInfos
    if (!mSensorIsStarted && wasRecording) {
      // check for recording file
      dir = new File(getResources().getString(R.string.downloadPath));

      matchingFiles_uploading = dir.listFiles(new FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().contains("TUM_DriveInfos_RECORD.json"));
        }
      });

      if (!(matchingFiles_uploading.length == 0)) {
        for (int i = 0; i < matchingFiles_uploading.length; i++) {
          File file = matchingFiles_uploading[i];

          JSONObject json = null;
          try {
            json = new JSONObject(loadJSONFromFile(file));
          } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
          }

          pw = json.optString("password", "");
          purpose = json.optInt("drivePurpose", 0);
          planning = json.optInt("drivePlanning", 0);
          electric = json.optInt("driveElectric", 0);
          start_rec = json.optLong("driveStartTime", 0L);
        }
      }

      for (File aMatchingFiles_uploading : matchingFiles_uploading) {
        aMatchingFiles_uploading.delete();
      }

      end_rec = System.currentTimeMillis();
      while (end_rec == 0.0) {
        end_rec = System.currentTimeMillis();
      }
      duration_rec = end_rec - start_rec;

      for (int i = 0; i < (latLngs.size() - 1); i++) {
        distance = distance + distance(latLngs.get(i), latLngs.get(i + 1));
      }

      DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
//    		symbols.setDecimalSeparator('.');
      DecimalFormat df = new DecimalFormat("#" + symbols.getDecimalSeparator() + "############", symbols);      // 12 Stellen
      distance = Double.valueOf(df.format(distance));
      ts_start = new Timestamp(start_rec);

      if (isOnline()) {
        new Thread(new Runnable() {
          public void run() {
            if (start_rec == 0.0) {
              start_rec = 0;
            }
            // Temporär geändert, da Passwort entfernt
            // String success = serverHelper.setDriveInfos(pw, purpose, planning, electric, duration_rec, distance, start_rec);
            String success = ServerHelper.setDriveInfos(purpose, planning, electric, duration_rec, distance, start_rec);
            if (success.equals("1")) {
              Handler handler = new Handler(Looper.getMainLooper());
              handler.post(new Runnable() {
                @Override
                public void run() {
                  Toast.makeText(NewDrive.this, "Fahrtinfos erfolgreich gesendet.",
                          Toast.LENGTH_LONG).show();
                }
              });
            } else {
              Handler handler = new Handler(Looper.getMainLooper());
              handler.post(new Runnable() {
                @Override
                public void run() {
                  writeDriveInfosFile(pw, purpose, planning, electric, duration_rec, distance, start_rec);
                }
              });
            }

            handleOldDriveInfosFiles();
          }
        }).start();
      } else {
        if (start_rec == 0.0)
          start_rec = 0;

        writeDriveInfosFile(pw, purpose, planning, electric, duration_rec, distance, start_rec);
      }
      wasRecording = !wasRecording;
    }
  }


  public void onChangeAccount(View v) {
    logout();
  }

  private void logout() {
    clearSharedPreferences();

    if (SensorService.getIsRunning()) {
      stopService(new Intent(this, SensorService.class));
    }

    if (UploadService.getIsRunning()) {
      stopService(new Intent(this, UploadService.class));
    }

    Intent intent = new Intent(this, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    startActivity(intent);
    this.finish();
  }

  @Override
  public void onBackPressed() {

    try {
      if (SensorService.getIsRunning()) {
        new AlertDialog.Builder(con)
                .setTitle(R.string.newDrive_titleBack)
                .setMessage(R.string.newDrive_msgBack)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialoginterface, int i) {
                    Intent intent = new Intent(con, MainPage.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    b = new Bundle();
                    b.putInt("drive_purpose", purpose);
                    b.putInt("drive_planning", planning);
                    b.putInt("drive_electric", electric);
                    b.putLong("start_rec", start_rec);
                    intent.putExtras(b);
                    startActivity(intent);

                    ((Activity) con).finish();
                  }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialoginterface, int i) {
                  }
                })
                .show();
      } else {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

          new AlertDialog.Builder(con)
                  .setTitle(R.string.newDrive_GPSenabled)
                  .setMessage(R.string.newDrive_GPSenabled_msg)
                  .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Log.i("gps","ja");
                        //if GPS is enabled cancel GPS settings...
                      Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                      myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      con.startActivity(myIntent);
                    }
                  })
                  .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Log.i("gps","nein");
                      Intent intent = new Intent(con, MainPage.class);
                      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                      startActivity(intent);

                      ((Activity) con).finish();
                    }
                  })
                  .show();
        } else {

            Log.i("gps","vllt");
          Intent intent = new Intent(con, MainPage.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          startActivity(intent);

          ((Activity) con).finish();
        }

      }


    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }



  }

  public void clearSharedPreferences(){
    SharedPreferences.Editor editor = settings.edit();
    editor.remove(Preferences.cLastLoginStatus);
    editor.remove(Preferences.cPass);
    editor.remove(Preferences.cToken);
    editor.apply();
	}
	
	
	@Override
  public void onStart() {
    try {
      super.onStart();
    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }
  }

  @Override
  public void onRestart() {
    try {
      super.onRestart();
    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }
  }
	
	@Override
  public void onPause() {
    try {
      super.onPause();
    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }
  }

  @Override
  public void onStop() {
    try {
      super.onStop();
    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }
  }

  @Override
  public void onDestroy() {
    try {
      super.onDestroy();
    } catch (Exception ex) {
      Log.i("cought unexpected exc", ex.toString());
    }
  }

  //	AlertDialog alertDialog;
  @Override
  public void onLocationChanged(Location location) {
    // Getting latitude of the current location
    double latitude = location.getLatitude();

    // Getting longitude of the current location
    double longitude = location.getLongitude();

    // Creating a LatLng object for the current location
    LatLng latLng = new LatLng(latitude, longitude);

    // Showing the current location in Google Map
    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

    // Zoom in the Google Map
    map.animateCamera(CameraUpdateFactory.zoomTo(18));

    latLngs.add(latLng);

    poly = new PolylineOptions()
            .color(Color.parseColor("#006ab3"));

//        bc = new LatLngBounds.Builder();

//        poly.add(latLng);

    //Add LatLngs to a polyline
    for (LatLng ll : latLngs) {
      poly.add(ll);
//        	bc.include(ll);
    }

    map.clear();
    map.addPolyline(poly);
//        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
  }

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

  public static double distance(LatLng StartP, LatLng EndP) {
    double lat1 = StartP.latitude;
    double lat2 = EndP.latitude;
    double lon1 = StartP.longitude;
    double lon2 = EndP.longitude;
    double dLat = Math.toRadians(lat2 - lat1);
    double dLon = Math.toRadians(lon2 - lon1);
    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                    Math.sin(dLon / 2) * Math.sin(dLon / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return 6371000 * c;    // [m]
  }

  public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
  }

  public void writeDriveInfosFile(String pw_write, int purpose_write, int planning_write, int electric_write, long duration_rec_write, Double distance_write, long start_rec_write) {
    String filename = "TUM_DriveInfos_" + start_rec_write + ".json";
    // Infos in JSON-Format
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("password", pw_write);
      jsonObject.put("drivePurpose", purpose_write);
      jsonObject.put("drivePlanning", planning_write);
      jsonObject.put("driveElectric", electric_write);
      jsonObject.put("driveDuration", duration_rec_write);
      jsonObject.put("driveDistance", distance_write);
      jsonObject.put("driveStartTime", start_rec_write);
    } catch (JSONException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    try {
      FileWriter jsonFileWriter = new FileWriter(getResources().getString(R.string.downloadPath) + filename);
      jsonFileWriter.write(jsonObject.toString());
      jsonFileWriter.flush();
      jsonFileWriter.close();
      System.out.print(jsonObject);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void writeDriveInfosFileRecording(String pw_write, int purpose_write, int planning_write, int electric_write, long start_rec_write) {
    String filename = "TUM_DriveInfos_RECORD.json";
    // Infos in JSON-Format
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("password", pw_write);
      jsonObject.put("drivePurpose", purpose_write);
      jsonObject.put("drivePlanning", planning_write);
      jsonObject.put("driveElectric", electric_write);
      jsonObject.put("driveStartTime", start_rec_write);
    } catch (JSONException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    try {
      FileWriter jsonFileWriter = new FileWriter(getResources().getString(R.string.downloadPath) + filename);
      jsonFileWriter.write(jsonObject.toString());
      jsonFileWriter.flush();
      jsonFileWriter.close();

      System.out.print(jsonObject);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

	int count_success = 0;

  public void handleOldDriveInfosFiles() {
    // check for older files
    dir = new File(getResources().getString(R.string.downloadPath));

    matchingFiles_uploading = dir.listFiles(new FileFilter() {
      public boolean accept(File pathname) {
        return (pathname.getName().contains("TUM_DriveInfos_"));
      }
    });

    if (!(matchingFiles_uploading.length == 0)) {
      String[] success_rest = new String[matchingFiles_uploading.length];
      for (int i = 0; i < matchingFiles_uploading.length; i++) {
        File file = matchingFiles_uploading[i];

        JSONObject json = null;
        try {
          json = new JSONObject(loadJSONFromFile(file));
        } catch (JSONException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }

        String pw_rest = json.optString("password", "");
        int purpose_rest = json.optInt("drivePurpose", 0);
        int planning_rest = json.optInt("drivePlanning", 0);
        int electric_rest = json.optInt("driveElectric", 0);
        long duration_rec_rest = json.optLong("driveDuration", 0L);
        Double distance_rest = json.optDouble("driveDistance", 0.0);
        long start_rec_rest = json.optLong("driveStartTime", 0L);

        // Temporär geändert, da Passwort entfernt
        // success_rest[i] = ServerHelper.setDriveInfos(pw_rest, purpose_rest, planning_rest, electric_rest, duration_rec_rest, distance_rest, start_rec_rest);
        success_rest[i] = ServerHelper.setDriveInfos(purpose_rest, planning_rest, electric_rest, duration_rec_rest, distance_rest, start_rec_rest);
      }

      for (int i = 0; i < matchingFiles_uploading.length; i++) {
        if (success_rest[i].equals("1")) {
          matchingFiles_uploading[i].delete();
          count_success = count_success + 1;
        }
      }

      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {
          Toast.makeText(NewDrive.this, count_success + " von " + matchingFiles_uploading.length + " Fahrtinfos erfolgreich gesendet.",
                  Toast.LENGTH_LONG).show();
        }
      });
    }

  }

  public String loadJSONFromFile(File file) {
    String json = null;
    try {
      FileInputStream fis;
      BufferedReader buffered_reader = null;
      StringBuilder total = null;

      if (file.exists() && file.isFile()) {
        fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        buffered_reader = new BufferedReader(isr);

        total = new StringBuilder();
        String line;
        while ((line = buffered_reader.readLine()) != null) {
          total.append(line);
        }
      }

      json = total.toString();
//            json = new String(total, "UTF-8");

    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    return json;

  }

}