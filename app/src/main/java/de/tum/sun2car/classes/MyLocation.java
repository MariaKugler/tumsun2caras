package de.tum.sun2car.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONObject;


public class MyLocation implements Comparable<MyLocation>
{
	private double trackID;
	private double latitude;
	private double longitude;
	private double altitude;
	private String time;	// ROUND((CAST(time AS DATE)-TO_DATE(''2011-01-01 00:00:00'',''YYYY-MM-DD HH24:MI:SS''))*24*3600,0) "TimeSec"
	private Date date;
	private double speed;
	private double hdop;
	private double course;
	
	/**
	 * Earth radius in kilometers
	 */
	private static final double earthRadius = 6371.009; 
	
	public MyLocation(double trackID, double lat, double lon, double alt, String time, double speed, double hdop, double course)
	{
		this.trackID = trackID;
		this.latitude = lat;
		this.longitude = lon;
		this.altitude = alt;
		this.time = time;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        try
        {
        	this.date = simpleDateFormat.parse(time);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
        this.speed = speed;
        this.hdop = hdop;
        this.course = course;
        
	}
	
	public MyLocation(JSONObject json) throws Exception
	{
		this.trackID = json.optDouble("trackID");
		this.latitude = json.optDouble("latitude");
		this.longitude = json.optDouble("longitude");
		this.altitude = json.optDouble("altitude");
		this.time = json.optString("time");
		
		
		// "03/24/2013 21:54";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        try
        {
        	this.date = simpleDateFormat.parse(time);
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
		
		this.speed = json.optDouble("speed");
		this.hdop = json.optDouble("hdop");
		this.course = json.optDouble("course");
	}


	public double getTrackID() {
		return trackID;
	}


	public void setTrackID(double trackID) {
		this.trackID = trackID;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public double getAltitude() {
		return altitude;
	}


	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}


	public String getTimestamp() {
		return time;
	}


	public void setTimestamp(String timestamp) {
		this.time = timestamp;
	}

	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public double getSpeed() {
		return speed;
	}


	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public double getHdop() {
		return hdop;
	}


	public void setHdop(double hdop) {
		this.hdop = hdop;
	}


	public double getCourse() {
		return course;
	}


	public void setCourse(double course) {
		this.course = course;
	}


	@Override
	public int compareTo(MyLocation another) {
		// TODO Auto-generated method stub
		return date.compareTo(another.date);
	}



	
	
	/**
     * Distance between two points.
     * 
     * @param point1 the first point.
     * @param point2 the second point.
     * @return the distance in kilometers
     */
	public static double distance(MyLocation point1, MyLocation point2)
	{
		return MyLocation.distanceInRadians(point1, point2) * earthRadius;
	}
	
	
	/*David
	 * Neue Berechnung der Distanz. Bisher wurden nur die Anfangs- und Endpunkte betrachtet.
	 * Rundkurse hatten die Distanz 0!
	 * 
	 */
	/**
	 * 
	 * Berechnet die durch eine Liste von TrackPoints aufgespannte Strecke.
	 * @return (real)distance in meters
	 */
	public static double sumDistance(List<MyLocation> locs){
		if (locs.size() > 2) {
            double sum = 0;
 
            for (int i=1; i<locs.size();i++) {
            	double distance=distance(locs.get(i),locs.get(i-1));
            		sum += distance;
            }
            return sum;
        }
        return 0;
		
			
	}


    /**
     * <p>This "distance" function is mostly for internal use.
     * 
     * <p>Yields the internal angle for an arc between two points on the surface of a sphere
     * in radians. This angle is in the plane of the great circle connecting the two points
     * measured from an axis through one of the points and the center of the Earth.
     * Multiply this value by the sphere's radius to get the length of the arc.</p>
     * 
     * @return the internal angle for the arc connecting the two points in radians.
     */
    public static double distanceInRadians(MyLocation loc1, MyLocation loc2) 
    {
            double lat1R = Math.toRadians(loc1.getLatitude());
            double lat2R = Math.toRadians(loc2.getLatitude());
            double dLatR = Math.abs(lat2R - lat1R);
            double dLngR = Math.abs(Math.toRadians(loc2.getLongitude()
                            - loc1.getLongitude()));
            double a = Math.sin(dLatR / 2) * Math.sin(dLatR / 2) + Math.cos(lat1R)
                            * Math.cos(lat2R) * Math.sin(dLngR / 2) * Math.sin(dLngR / 2);
            return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    }
    
    /**
	 * Calculates the mean speed between two TrackPoints
	 * @param start first TrackPoint
	 * @param end second TrackPoint
	 * @return mean speed between the two points in m/s
	 */
	public static double meanSpeedBetween(MyLocation start, MyLocation end)
	{
		return distance(start, end) * 1000 / (timeDiffBetween(start,end) / 1000);
	}
	
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @return the time difference between the two points in milliseconds
	 */
	public static double timeDiffBetween(MyLocation start, MyLocation end)
	{
		return (end.getDate().getTime() - start.getDate().getTime());
	}
	
	
	
	/**
	 * sums all speed-values for a certain list of trackpoints
	 * @param tps
	 * @return
	 */
	public static double sumSpeed (List<MyLocation> tps){
        if (tps.size() > 0) {
            double sum = 0;
 
            for (MyLocation i : tps) {
            	if ( i.getSpeed() != -1.0 )
            		sum += i.getSpeed();
            }
            return sum;
        }
        return 0;
    }
	
	/**
	 * calculates real mean speed for a certain list of trackpoints
	 * @param locs
	 * @return
	 */
    public static double meanSpeed (List<MyLocation> locs){

        for (ListIterator<MyLocation> iter = locs.listIterator(); iter.hasNext(); ) {
            MyLocation loc = iter.next();
            if (loc.getSpeed() < (1 / 3.6))
            {
                iter.remove();
            }
        }

    	//locs = filter(having(on(MyLocation.class).getSpeed(), greaterThan(1 / 3.6)), locs);
        double sum = sumSpeed(locs);
        double mean = 0;
        mean = sum / (locs.size() * 1.0);
        return mean;
    }
    
  //David
  //accelerationBetween
    
    /**
     * 
     * @param loc2
     * @param loc2
     * @return acceleration between TrackPoint1 and TrackPoint2
     */
    
    public static double accelerationBetween(MyLocation loc1, MyLocation loc2)
    {
    	double speedDiff = loc1.getSpeed() - loc2.getSpeed();
	    double timeDiff = (loc1.getDate().getTime() - loc2.getDate().getTime()) / 1000;
	    
	    return Math.abs(speedDiff/timeDiff);
    	
    }
    
    
    //David
    //headingChangeBetween()
    /**
     * 
     * @param loc1
     * @param loc2
     * @return course change between TrackPoint1 and TrackPoint2
     */
    
    public static double courseChangeBetween(MyLocation loc1, MyLocation loc2)
    {
    	double courseDiff = loc2.getCourse() - loc1.getCourse();
    	return Math.abs(courseDiff);
    }


}
