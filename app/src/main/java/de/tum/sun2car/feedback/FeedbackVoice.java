package de.tum.sun2car.feedback;

import java.io.File;
import java.io.FileFilter;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import de.tum.sun2car.R;
import de.tum.sun2car.helper.GMailSender;
import de.tum.sun2car.helper.RecordingControl;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class FeedbackVoice extends Activity {

  String userName;
  private SharedPreferences settings;
  Boolean success;

  ImageButton btn_recording;
  ImageButton btn_send;
  TextView tv_recording;
  Chronometer chrono;
  private String recordedTime = null;

  private RecordingControl recorder = null;
  private File file = null;
  File[] matchingFiles_uploading;
  File dir;

  Drawable drw_btn_recording;
  Drawable drw_btn_send;

  private State states = State.STOP;

  private enum State {
    STOP, RECORD
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_feedback_voice);


    dir = new File(getResources().getString(R.string.downloadPath));

    // Delete old recording files
    File[] matchingFiles_record = dir.listFiles(new FileFilter() {
      public boolean accept(File pathname) {
        return (pathname.getName().contains("TUM_recording_"));
      }
    });

    for (int i = 0; i < matchingFiles_record.length; i++) {
      matchingFiles_record[i].delete();
    }

    // check for files to upload
    matchingFiles_uploading = dir.listFiles(new FileFilter() {
      public boolean accept(File pathname) {
        return (pathname.getName().contains("TUM_uploading_"));
      }
    });


    settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);

    userName = settings.getString(Preferences.cUsername, null);

    btn_recording = (ImageButton) findViewById(R.id.feedback_voice_record);
    tv_recording = (TextView) findViewById(R.id.feedback_voice_text);
    btn_send = (ImageButton) findViewById(R.id.feedback_voice_send);
    chrono = (Chronometer) findViewById(R.id.feedback_chronometer);
    chrono.setFormat("%s");
    chrono.setBase(SystemClock.elapsedRealtime());

    String filepath = getResources().getString(R.string.RecordTempPath);
    Timestamp timestamp = new Timestamp(new Date(System.currentTimeMillis()).getTime());
    filepath = filepath + timestamp.toString() + "_" + userName + ".wav";
    file = new File(filepath);
    recorder = new RecordingControl(file);


    if (matchingFiles_uploading.length != 0) {
      btn_send.setBackground(null);
      drw_btn_send = getResources().getDrawable(R.drawable.btn_feedback_voice_send);
      btn_send.setBackground(drw_btn_send);
    }

  }

  public void onRecording(View v) {
    if (states == State.RECORD) {
      recorder.endRecord();
      tv_recording.setText(getResources().getString(R.string.feedback_voice_start));

      btn_recording.setBackground(null);
      drw_btn_recording = getResources().getDrawable(R.drawable.feedback_play);
      btn_recording.setBackground(drw_btn_recording);

      FeedbackVoice.this.states = State.STOP;
      chrono.stop();


      recordedTime = new StringBuilder(chrono.getText()).toString();

      String filepath = getResources().getString(R.string.UploadTempPath);
      Timestamp timestamp = new Timestamp(new Date(System.currentTimeMillis()).getTime());
      filepath = filepath + timestamp.toString() + "_" + userName + ".wav";
      File TestFile = new File(filepath);
      if (TestFile.exists()) {
        TestFile.delete();
      }

      file.renameTo(TestFile);


      try {
        success = new SendVoiceFeebackTask().execute(userName, filepath).get();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (ExecutionException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      if (success) {
        Toast.makeText(FeedbackVoice.this,
                "Feedback erfolgreich gesendet.",
                Toast.LENGTH_LONG).show();
        new File(filepath).delete();
      } else {
        Toast.makeText(FeedbackVoice.this, "Feedback wurde nicht gesendet.",
                Toast.LENGTH_LONG).show();
        btn_send.setBackground(null);
        drw_btn_send = getResources().getDrawable(R.drawable.btn_feedback_voice_send);
        btn_send.setBackground(drw_btn_send);
      }
      chrono.setBase(SystemClock.elapsedRealtime());

//			new CountDownTimer(3000,1000){
//
//	            @Override
//	            public void onTick(long miliseconds)
//	            {
//	            }
//
//	            @Override
//	            public void onFinish(){
//	            }
//	        }.start();

    } else {
      recorder = new RecordingControl(file);
      recorder.startRecord();
      tv_recording.setText(getResources().getString(R.string.feedback_voice_end));

      btn_recording.setBackground(null);
      drw_btn_recording = getResources().getDrawable(R.drawable.feedback_stop);
      btn_recording.setBackground(drw_btn_recording);

      FeedbackVoice.this.states = State.RECORD;
      chrono.setBase(SystemClock.elapsedRealtime());
      chrono.start();

      //TODO: Upload muss noch implementiert werden
    }

  }

  public void onSendRemaining(View v) {
    boolean success_rest[] = new boolean[matchingFiles_uploading.length];
    for (int i = 0; i < matchingFiles_uploading.length; i++) {
      try {
        success_rest[i] = new SendVoiceFeebackTask().execute(userName, matchingFiles_uploading[i].getPath()).get();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (ExecutionException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    int count_success = 0;
    for (int i = 0; i < matchingFiles_uploading.length; i++) {
      if (success_rest[i]) {
        matchingFiles_uploading[i].delete();
        count_success = count_success + 1;
      }
    }
    Toast.makeText(FeedbackVoice.this, count_success + " von " + matchingFiles_uploading.length + " Feedbacks erfolgreich gesendet.",
            Toast.LENGTH_LONG).show();

    if (count_success == matchingFiles_uploading.length) {
      btn_send.setBackground(null);
      drw_btn_send = getResources().getDrawable(R.drawable.feedback_voice_send_inactive);
      btn_send.setBackground(drw_btn_send);
    } else if (count_success > 0 && count_success > matchingFiles_uploading.length) {
      matchingFiles_uploading = dir.listFiles(new FileFilter() {
        public boolean accept(File pathname) {
          return (pathname.getName().contains("TUM_uploading_"));
        }
      });
    }

  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.feedback_voice, menu);
    return true;
  }

}

class SendVoiceFeebackTask extends AsyncTask<String, Void, Boolean> {

  private Exception exception;
  boolean success;
  List<String> a = new ArrayList<String>();

  protected Boolean doInBackground(String... param) {
    try {
//        	android.os.Debug.waitForDebugger();

      GMailSender mailsender = new GMailSender("probanden.sun2car@gmail.com", "sun2car@GAP");

      a.add("sun2car@ftm.mw.tum.de");  //TODO: set project-mail-address!

      String[] toArr = new String[a.size()];
      a.toArray(toArr);

      mailsender.set_to(toArr);
      mailsender.set_from("probanden.sun2car@gmail.com");
      mailsender.set_subject("Voice Feedback: " + param[0]);
      mailsender.setBody("Absender: " + param[0]);

      try {
//                mailsender.addAttachment("/sdcard/filelocation");
        mailsender.addAttachment(param[1]);


        if (mailsender.send()) {
          success = true;
        } else {
          success = false;
        }
      } catch (Exception e) {

        Log.e("MailApp", "Could not send email", e);
      }

      return success;
    } catch (Exception e) {
      this.exception = e;
      return false;
    }
  }

  protected void onPostExecute() {
    // TODO: check this.exception
    // TODO: do something with the feed
  }
}
