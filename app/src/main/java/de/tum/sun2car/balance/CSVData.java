package de.tum.sun2car.balance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CSVData {
	
	List<CSVBase> CSVdataFileTotal = new ArrayList<CSVBase>();
	List<CSVBase> CSVdataFileHour = new ArrayList<CSVBase>();
	List<CSVBase> CSVdataFileDay = new ArrayList<CSVBase>();
	
	

	public void setCSVTotal(GregorianCalendar date, float energy, String csvName) {	
		CSVBase base = new CSVBase(date, energy, csvName);
		CSVdataFileTotal.add(base);
	}	
	public void setCSVHour(GregorianCalendar date, float energy, String csvName) {
		CSVBase base = new CSVBase(date, energy, csvName);
		CSVdataFileHour.add(base);
	}
	public void setCSVDay(GregorianCalendar date, float energy, String csvName) {
		CSVBase base = new CSVBase(date, energy, csvName);
		CSVdataFileDay.add(base);
	}

	public List<CSVBase> getCSVHour(){	
		return CSVdataFileHour;
	}
	public List<CSVBase> getCSVDay(){	
		return CSVdataFileDay;
	}
	public List<CSVBase> getCSVTotal(){	
		return CSVdataFileTotal;
	}
	
	
	public float[] getCSVHourEnergy(GregorianCalendar timestamp){
		float[] hour_count = new float[24];
		int count = 0;
		for (int i = 0; i < hour_count.length; i++) {
			hour_count[i] = 0;
		}
		int year = timestamp.get(Calendar.YEAR);
		int month = timestamp.get(Calendar.MONTH);
		int day = timestamp.get(Calendar.DAY_OF_MONTH);
		for (CSVBase currentData : CSVdataFileHour) {
			int currentYear = currentData.getDate().get(Calendar.YEAR);
			int currentMonth = currentData.getDate().get(Calendar.MONTH);
			int currentDay = currentData.getDate().get(Calendar.DAY_OF_MONTH);
			int currentHour = currentData.getDate().get(Calendar.HOUR_OF_DAY);
			if(currentYear  > year)
				return hour_count;
			
			if(year == currentYear && month ==  currentMonth && day == currentDay){
				hour_count[currentHour] += currentData.getEnergy();
				if(currentHour == 23){
					System.arraycopy(hour_count, 1, hour_count, 0, 23);
					hour_count[23] = CSVdataFileHour.get(count+1).getEnergy();
				}				
			}

			count += 1;
		}
		return hour_count;
		
		
	}

	
	public boolean containsCSVHour(GregorianCalendar timestamp){
		for (CSVBase base : CSVdataFileHour) {
			if(base.date.equals(timestamp)){
				return true;
			}
		}
		return false;	
	}
	
	public GregorianCalendar lasttimestamp(String key){
		if(key.equalsIgnoreCase("hour")){
			if (!CSVdataFileHour.isEmpty()) {
				int lastindex = CSVdataFileHour.size();
				CSVBase csvLaststamp = CSVdataFileHour.get(lastindex);
				return csvLaststamp.getDate();
				}
		}if(key.equalsIgnoreCase("day")){
			if (!CSVdataFileDay.isEmpty()) {
				int lastindex = CSVdataFileDay.size();
				CSVBase csvLaststamp = CSVdataFileDay.get(lastindex);
				return csvLaststamp.getDate();
				}
		}if(key.equalsIgnoreCase("total")){
			if (!CSVdataFileTotal.isEmpty()) {
				int lastindex = CSVdataFileTotal.size();
				CSVBase csvLaststamp = CSVdataFileTotal.get(lastindex);
				return csvLaststamp.getDate();
				}
			
		}	
		return null;
	}
	
	
	
	
	
	
}

