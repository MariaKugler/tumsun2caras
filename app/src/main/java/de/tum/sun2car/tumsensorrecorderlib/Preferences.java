package de.tum.sun2car.tumsensorrecorderlib;

import java.io.File;

import android.os.Environment;

public class Preferences {
  private static File mOutDataDirectory = null;

  public static final String PREF_FILE_NAME = "SensorDataUploader";
  public static final String cUsername = "user";
  public static final String cPass = "pw";
  public static final String cToken = "";
  public static final String cLastLoginStatus = "lastlogin";


  public static File getFileDirectory() {

    if (mOutDataDirectory == null) {
      mOutDataDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      mOutDataDirectory.mkdirs();
    }

    return mOutDataDirectory;
  }
}
