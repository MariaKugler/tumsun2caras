package de.tum.sun2car.logbook;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;

public class DrivesAdapter extends ArrayAdapter<Drives> {
  List<Drives> drives;
  Context context;

  public DrivesAdapter(Context context,
                       int textViewResourceId, List<Drives> drives) {
    super(context, textViewResourceId, drives);
    this.context = context;
    this.drives = drives;

  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View v = convertView;
    if (v == null) {
      LayoutInflater vi = (LayoutInflater) this.context
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      v = vi.inflate(R.layout.item_logbook_drives, null);
    }

    ImageView iv_mot = (ImageView) v.findViewById(R.id.logbook_mot);
    TextView tv_trip_length = (TextView) v.findViewById(R.id.logbook_trip_length);
//			TextView tv_trip_duration = (TextView) v.findViewById(R.id.logbook_trip_duration);

    TextView tv_start_loc = (TextView) v.findViewById(R.id.logbook_start_location);
    TextView tv_stop_loc = (TextView) v.findViewById(R.id.logbook_stop_location);

    TextView tv_start_time = (TextView) v.findViewById(R.id.logbook_start_time);
    TextView tv_stop_time = (TextView) v.findViewById(R.id.logbook_stop_time);

    Drives drive = drives.get(position);

    Double length = drive.getTrip_length() / 1000; // in km
    DecimalFormat df = new DecimalFormat("#.##");      // 2 Stellen
    tv_trip_length.setText(new String(df.format(length).replace(".", ",")) + " km");

//			tv_trip_duration.setText(drive.getTrip_duration());
    tv_start_loc.setText(drive.getStart_location());
    tv_stop_loc.setText(drive.getStop_location());
    tv_start_time.setText(drive.getStart_time());
    tv_stop_time.setText(drive.getStop_time());


//			bike x
//			Bus x
//			car x
//			citytrain 
//			foot x
//			other 
//			SBahn x
//			subway x
//			train x
//			Tram x
//			Walk x

    if (drive.getTrip_mot().equals("bike"))
      iv_mot.setBackgroundResource(R.drawable.mot_bike);
    else if (drive.getTrip_mot().toLowerCase().equals("bus"))
      iv_mot.setBackgroundResource(R.drawable.mot_bus);
    else if (drive.getTrip_mot().toLowerCase().equals("car"))
      iv_mot.setBackgroundResource(R.drawable.mot_car);
    else if (drive.getTrip_mot().toLowerCase().equals("sbahn") || drive.getTrip_mot().toLowerCase().equals("citytrain"))
      iv_mot.setBackgroundResource(R.drawable.mot_sbahn);
    else if (drive.getTrip_mot().toLowerCase().equals("subway"))
      iv_mot.setBackgroundResource(R.drawable.mot_subway);
    else if (drive.getTrip_mot().toLowerCase().equals("train"))
      iv_mot.setBackgroundResource(R.drawable.mot_train);
    else if (drive.getTrip_mot().toLowerCase().equals("tram"))
      iv_mot.setBackgroundResource(R.drawable.mot_tram);
    else if (drive.getTrip_mot().toLowerCase().equals("walk") || drive.getTrip_mot().toLowerCase().equals("foot"))
      iv_mot.setBackgroundResource(R.drawable.mot_walk);
    else
      iv_mot.setBackgroundResource(R.drawable.mot_other);

    return v;
  }

  public void reload(List<Drives> d_list) {
    drives.clear();
    drives.addAll(d_list);
    notifyDataSetChanged();
  }
}



