package de.tum.sun2car.balance;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import de.tum.sun2car.R;

public class Balance extends TabActivity {

	private TabHost tabHost;
	int currentTab = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_balance);
		
		tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    /**
	     * Tab BalanceTracks
	     */
	    // set Tab Text
		final TextView tvTracks = new TextView(this);
		tvTracks.setText(R.string.balance_tracks);
		tvTracks.setTextColor(Color.WHITE);
		tvTracks.setTextSize(18);
		tvTracks.setPadding(2, 20, 2, 20);
		tvTracks.setGravity(25);

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, de.tum.sun2car.balance.BalanceTracks.class);

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("tracks").setIndicator(tvTracks)
	                  .setContent(intent);
	    tabHost.addTab(spec);
	    
	    /**
	     * Tab BalanceSmartMeter
	     */
	    // set Tab Text
		final TextView tvSmartMetering = new TextView(this);
		tvSmartMetering.setText(R.string.balance_pv);
		tvSmartMetering.setTextColor(Color.parseColor("#006ab3"));
		tvSmartMetering.setTextSize(18);
		tvSmartMetering.setPadding(2, 20, 2, 20);
		tvSmartMetering.setGravity(25);

	    // Create an Intent to launch an Activity for the tab (to be reused)
	    intent = new Intent().setClass(this, de.tum.sun2car.balance.BalanceSmartMeter.class);

		// pass savedCalView, if existing
    	if ( getIntent().hasExtra("savedCal") )
    	{
    		intent.putExtras(getIntent());
    		currentTab = 1;
    	}

	    // Initialize a TabSpec for each tab and add it to the TabHost
	    spec = tabHost.newTabSpec("smartmetering").setIndicator(tvSmartMetering)
	                  .setContent(intent);
	    tabHost.addTab(spec);


	    /**
	     * Tab BalancePurposes
	     */
	    // set Tab Text
	    final TextView tvPurposes = new TextView(this);
	    tvPurposes.setText(R.string.balance_drivepurpose);
	    tvPurposes.setTextColor(Color.parseColor("#006ab3"));
	    tvPurposes.setTextSize(18);
	    tvPurposes.setPadding(2, 20, 2, 20);
	    tvPurposes.setGravity(25);
		
	    intent = new Intent().setClass(this, de.tum.sun2car.balance.BalancePurposes.class);
	    spec = tabHost.newTabSpec("driveDetails").setIndicator(tvPurposes)
	                  .setContent(intent);
	    tabHost.addTab(spec);

	    tabHost.setCurrentTab(currentTab);
	    
	    // current tab: blue, other: white
	    for(int i = 0; i < tabHost.getTabWidget().getChildCount(); i++)
	    {
	        tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
	    }
    	
    	tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));

        if (tabHost.getCurrentTab() == 0)
    	{
    		tvTracks.setTextColor(Color.WHITE);
    		tvSmartMetering.setTextColor(Color.parseColor("#006ab3"));
    		tvPurposes.setTextColor(Color.parseColor("#006ab3"));
    	}
        else if (tabHost.getCurrentTab() == 1)
    	{
    		tvTracks.setTextColor(Color.parseColor("#006ab3"));
    		tvSmartMetering.setTextColor(Color.WHITE);
    		tvPurposes.setTextColor(Color.parseColor("#006ab3"));
    	}
        else
        {
    		tvTracks.setTextColor(Color.parseColor("#006ab3"));
    		tvSmartMetering.setTextColor(Color.parseColor("#006ab3"));
    		tvPurposes.setTextColor(Color.WHITE);
    	}
    
	    
	    
	    tabHost.setOnTabChangedListener(new OnTabChangeListener()
			{
	    		public void onTabChanged(String arg0)
			   	{
			    	for(int i = 0; i < tabHost.getTabWidget().getChildCount(); i++)
				    {
				        tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
				    }
			    	
			    	tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
	        
			        if (tabHost.getCurrentTab() == 0)
		        	{
		        		tvTracks.setTextColor(Color.WHITE);
		        		tvSmartMetering.setTextColor(Color.parseColor("#006ab3"));
		        		tvPurposes.setTextColor(Color.parseColor("#006ab3"));
		        	}
			        else if (tabHost.getCurrentTab() == 1)
		        	{
		        		tvTracks.setTextColor(Color.parseColor("#006ab3"));
		        		tvSmartMetering.setTextColor(Color.WHITE);
		        		tvPurposes.setTextColor(Color.parseColor("#006ab3"));
		        	}
			        else
			        {
		        		tvTracks.setTextColor(Color.parseColor("#006ab3"));
		        		tvSmartMetering.setTextColor(Color.parseColor("#006ab3"));
		        		tvPurposes.setTextColor(Color.WHITE);
		        	}
			   	}     
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance, menu);
		return true;
	}

}
