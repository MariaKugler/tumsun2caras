package de.tum.sun2car.helper;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import de.tum.sun2car.classes.MyLocation;

public class LocationDB
{
	private static final int VERSION_LocationDB = 2;
	private static final String NOM_LocationDB = "sun2car.db";
 
	private static final String TABLE_LOCATION = "table_location";

	public static final String COL_LOCATION_LOCID = "LOC_ID";
	private static final int NUM_COL_LOCID = 0;
	public static final String COL_LOCATION_TRACKID = "TRACK_ID";
	private static final int NUM_COL_TRACKID = 1;
	public static final String COL_LOCATION_LAT = "LAT";
	private static final int NUM_COL_LAT = 2;
	public static final String COL_LOCATION_LON = "LON";
	private static final int NUM_COL_LON = 3;
	public static final String COL_LOCATION_ALT = "ALT";
	private static final int NUM_COL_ALT = 4;
	public static final String COL_LOCATION_TIME = "TIME";
	private static final int NUM_COL_TIME = 5;
	public static final String COL_LOCATION_SPEED = "SPEED";
	private static final int NUM_COL_SPEED = 6;
	public static final String COL_LOCATION_HDOP = "HDOP";
	private static final int NUM_COL_HDOP = 7;
	public static final String COL_LOCATION_COURSE = "COURSE";
	private static final int NUM_COL_COURSE = 8;

	private String[] allColumns = {SqliteDB.COL_LOCATION_LOCID, 
									SqliteDB.COL_LOCATION_TRACKID, 
									SqliteDB.COL_LOCATION_LAT,
									SqliteDB.COL_LOCATION_LON,
									SqliteDB.COL_LOCATION_ALT, 
									SqliteDB.COL_LOCATION_TIME,
									SqliteDB.COL_LOCATION_SPEED,
									SqliteDB.COL_LOCATION_HDOP,
									SqliteDB.COL_LOCATION_COURSE};
							
	private SQLiteDatabase locationdb;
 
	private SqliteDB myBaseSQLite;
 
	public LocationDB(Context context){
		//Erzeugung der DB inkl. Tabellen
		myBaseSQLite = new SqliteDB(context, NOM_LocationDB, null, VERSION_LocationDB);
	}
 
	public void open(){
		//DB im Schreib-Modus öffnen
		locationdb = myBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//Datenbank schliessen
		locationdb.close();
	}
 
	public SQLiteDatabase getLocationDB(){
		return locationdb;
	}

  public long addLocation(MyLocation loc) {
    //Erzeugung einer ContentValues
    ContentValues values = new ContentValues();

    //Einfügen eines Element als key-value-Paar (Spalte-Wert-Paar)
    values.put(COL_LOCATION_TRACKID, loc.getTrackID());
    values.put(COL_LOCATION_LAT, loc.getLatitude());
    values.put(COL_LOCATION_LON, loc.getLongitude());
    values.put(COL_LOCATION_ALT, loc.getAltitude());
    values.put(COL_LOCATION_TIME, loc.getTimestamp());
    values.put(COL_LOCATION_SPEED, loc.getSpeed());
    values.put(COL_LOCATION_HDOP, loc.getHdop());
    values.put(COL_LOCATION_COURSE, loc.getCourse());
//		values.put(COL_COUNT, s.getCount());

    //Ein Objekt wird in bdd via ContentValues eingefügt
    return locationdb.insert(TABLE_LOCATION, null, values);
  }


  public int updateLocation(MyLocation d) {
    double trackid = d.getTrackID();

    ContentValues values = new ContentValues();
//		values.put(COL_TITLE, s.getTitle());
    return locationdb.update(TABLE_LOCATION, values, COL_LOCATION_TRACKID + " = " + trackid, null);
  }

  public int removeDriveWithTrackID(Double trackid) {
    return locationdb.delete(TABLE_LOCATION, COL_LOCATION_TRACKID + " = \"" + trackid + "\"", null);
  }

  private MyLocation cursorToLocation(Cursor c) {
    if (c.getCount() == 0)
      return null;

    MyLocation loc = new MyLocation(c.getDouble(NUM_COL_TRACKID), c.getDouble(NUM_COL_LAT), c.getDouble(NUM_COL_LON), c.getDouble(NUM_COL_ALT),
            c.getString(NUM_COL_TIME), c.getDouble(NUM_COL_SPEED), c.getDouble(NUM_COL_HDOP), c.getDouble(NUM_COL_COURSE));

    return loc;
  }

  public List<MyLocation> getAllLocationsByTrack(Double trackid) {
    List<MyLocation> locs = new ArrayList<MyLocation>();

    Cursor c = locationdb.rawQuery("SELECT * FROM " + TABLE_LOCATION + " WHERE " + COL_LOCATION_TRACKID + " = " + String.valueOf(trackid), null);
    if(c.moveToFirst()){
      do{
        MyLocation loc = cursorToLocation(c);
        locs.add(loc);
      }while(c.moveToNext());
    }

    c.close();
    return locs;
  }




}
