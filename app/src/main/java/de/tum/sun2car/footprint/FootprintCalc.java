package de.tum.sun2car.footprint;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tum.sun2car.classes.MyLocation;

public class FootprintCalc {
  public static Double calculateCO2Trip(List<Double> speed, List<Double> accel, List<Double> distance, String mot, Double TrackLength) {
    Double co2TOTAL = 0.0;

    // TODO
    // Die Distance-Liste wird leer übergeben ???
    // Daher Workaround mit "Double TrackLength"
    // Daher abfrage ob Liste leer => Liste mit Tracklength verändern
    if (distance.size() == 0) {
      distance.add(TrackLength);
    }

    // TODO: later these values are from mots
    float consumption = 11.5f;
    double speedFactor = 1.0;
    double co2gLiter = 0;

    if (mot.equalsIgnoreCase("bike") || mot.equalsIgnoreCase("walk") || mot.equalsIgnoreCase("foot")) {
      co2TOTAL = 0.0;
    } else if (mot.equalsIgnoreCase("bus")) {
      // 90 gCO2/PersonenKM
      double co2_bus = 90;
      co2TOTAL = co2_bus * distance.get(distance.size() - 1) / 1000;
    } else if (mot.equalsIgnoreCase("sbahn") || mot.equalsIgnoreCase("citytrain")) {
      // 42 gCO2/PersonenKM
      double co2_sbahn = 42;
      co2TOTAL = co2_sbahn * distance.get(distance.size() - 1) / 1000;
    } else if (mot.equalsIgnoreCase("train")) {
      // 77 gCO2/PersonenKM
      double co2_train = 77;
      co2TOTAL = co2_train * distance.get(distance.size() - 1) / 1000;
    } else if (mot.equalsIgnoreCase("tram") || mot.equalsIgnoreCase("subway")) {
      // 22 gCO2/PersonenKM
      double co2_tramsubway = 22;
      co2TOTAL = co2_tramsubway * distance.get(distance.size() - 1) / 1000;
    }
    // Calculation Car
    else if (mot.equalsIgnoreCase("car")) {
      // Annahme: 18 kWh / 100km (inkl. NV)
      /**
       * Technikgespräch 20140129
       * mittlerer Verbrauch:
       * - 14 kWh/100km
       * - 6.4 l/100km --> 148,48 gCO2/km
       */
      //
      double energykWh100km = 14;

      // Annahme konventioneller Antrieb: 150 g CO2/km
      // Annahme gasbetriebener Antrieb: 80% * konventionell (http://www.erdgasautos.org/erdgasautos/Emissionen-Erdgas-Benzin-43.htm)
      double co2_car_konv = 150;
      double co2_car_gas = 0.78 * co2_car_konv;
      double co2_car_el_kWh = 550;  // http://de.statista.com/statistik/daten/studie/38897/umfrage/co2-emissionsfaktor-fuer-den-strommix-in-deutschland-seit-1990/
      double co2_car_el = co2_car_el_kWh * energykWh100km / 100;
      // 1 l Diesel etwa 2,62 kg CO2
      // 1 l Benzin etwa 2,32 kg CO2
      // Umrechnungsfaktoren (UF):
      // für Diesel etwa 26,2 kg/10l
      // für Benzin etwa 23,2 kg/10l
      // --> g CO2/km = x l/100km * UF

      // TODO: switch konv-el-gas

      int count_speed_lt30 = 0;
      int count_speed_30120 = 0;
      int count_speed_gt120 = 0;

      int count_stop = 0;
      int stop_length = 0;
      boolean newStop = true;
      ArrayList<Double> stop_duration = new ArrayList<Double>();

      int count_accelstrong = 0;
      int count_accelrecup = 0;
      int count_acceldynamic = 0;
      ArrayList<Integer> accelstrong_duration = new ArrayList<Integer>();


      for (int i = 0; i < speed.size(); i++) {
        double currentAcceleration = accel.get(i);
        double currentSpeed = speed.get(i);

        /**
         *  analyse speed-niveau
         */
        if (currentSpeed <= 30 / 3.6 && currentSpeed >= 0) {
          count_speed_lt30++;
        } else if (currentSpeed > 30 / 3.6 && currentSpeed <= 120 / 3.6) {
          count_speed_30120++;
        } else if (currentSpeed > 120 / 3.6) {
          count_speed_gt120++;
        }

        /**
         * analyse stopp occurence
         */
        if (currentSpeed < 10 / 3.6 && currentSpeed >= 0) {
          count_stop++;
        }

        /**
         * analyse stopp duration
         */
        // start counter for new Stop
        if (currentSpeed < 10 / 3.6 && newStop && currentSpeed >= 0) {
          newStop = false;
        }

        // increase counter
        if (currentSpeed < 10 / 3.6 && !newStop &&
                i > 0 && speed.get(i - 1) < 10 / 3.6 && currentSpeed >= 0) {
          stop_length++;
        }

        // add stoppduration and reset counter
        if (currentSpeed > 10 / 3.6 && !newStop & stop_length > 1) {
          stop_duration.add(new Double(stop_length));
          newStop = true;
          stop_length = 1;
        }


        /**
         *  analyse acceleration
         */
        if (currentAcceleration > 3)  // fast acceleration
        {
          count_accelstrong++;
        } else if (currentAcceleration < 0) {
          count_accelrecup++;
        }

        /**
         * analyse dynamic accelerations
         */
        if (Math.abs(currentAcceleration) > 2.5) {
          count_acceldynamic++;
        }

      }


      /**
       * analyse dynamic behaviour
       */
      double speedStd = getRealStdValue(speed);
      double speedMean = getRealMeanValue(speed);
      double accelStd = getStdValue(accel);
      double accelMean = getMeanValue(accel);


      /**
       * SET FACTORS
       */
      double fac_speed_lt30 = 1.0;
      double fac_speed_30120 = 1.0;
      double fac_speed_gt120 = 1.0;
      double fac_speed_behaviour = 1.0;

      double fac_stop_occurence = 1.0;
      double fac_stop_duration = 1.0;

      double fac_accel_strong = 1.0;
      double fac_accel_recup = 1.0;
      double fac_accel_dynamic = 1.0;
      double fac_accel_behaviour = 1.0;


      /**
       * CALC FACTORS
       */

      // Error: divide by zero
      // kommt vor wenn speed.size == 0
      // => Prüfen ob null, wenn null, dann Rechnen wie wenn Ergebnis > 0.xxx
      if(speed.size() != 0) {

        if (count_speed_lt30 / speed.size() > 0.2) {
          fac_speed_lt30 = fac_speed_lt30 + 0.2;
        } else {
          fac_speed_lt30 = fac_speed_lt30 - 0.1;
        }

        if (count_speed_30120 / speed.size() > 0.4) {
          fac_speed_30120 = fac_speed_30120 - 0.20;
        } else {
          fac_speed_30120 = fac_speed_30120 + 0.10;
        }

        if (count_speed_gt120 / speed.size() > 0.3) {
          fac_speed_gt120 = fac_speed_gt120 + 0.15;
        } else {
          fac_speed_gt120 = fac_speed_gt120 - 0.05;
        }

      } else {
        fac_speed_lt30 = fac_speed_lt30 + 0.2;
        fac_speed_30120 = fac_speed_30120 - 0.20;
        fac_speed_gt120 = fac_speed_gt120 + 0.15;
      }



      if (speedStd / speedMean > 0.3)  // breites Geschwindigkeitsband
      {
        fac_speed_behaviour = fac_speed_behaviour + 0.15;
      } else {
        fac_speed_behaviour = fac_speed_behaviour - 0.05;
      }

      // Error: divide by zero
      // kommt vor wenn speed.size == 0
      // => Prüfen ob null, wenn null, dann Rechnen wie wenn Ergebnis > 0.xxx
      if(speed.size() != 0) {
        if (count_stop / speed.size() > 0.3) {
          fac_stop_occurence = fac_stop_occurence + 0.3;
        } else {
          fac_stop_occurence = fac_stop_occurence - 0.15;
        }
      } else {
        fac_stop_occurence = fac_stop_occurence + 0.3;
      }

      if (stop_duration.size() > 0) {
        double stop_duration_mean = getMeanValue(stop_duration);
        if (stop_duration_mean / speed.size() > 0.15) {
          fac_stop_duration = fac_stop_duration + 0.3;
        } else {
          fac_stop_duration = fac_stop_duration + 0.1;
        }
      } else {
        fac_stop_duration = fac_stop_duration - 0.2;
      }

      // Error: divide by zero
      // kommt vor wenn speed.size == 0
      // => Prüfen ob null, wenn null, dann Rechnen wie wenn Ergebnis > 0.xxx
      if(speed.size() != 0) {

        if (count_accelstrong / speed.size() > 0.3) {
          fac_accel_strong = fac_accel_strong + 0.25;
        } else {
          fac_accel_strong = fac_accel_strong - 0.1;
        }

        if (count_accelrecup / speed.size() > 0.3) {
          fac_accel_recup = fac_accel_recup - 0.25;
        } else {
          fac_accel_recup = fac_accel_recup + 0.1;
        }

        if (count_acceldynamic / speed.size() > 0.3) {
          fac_accel_dynamic = fac_accel_dynamic + 0.25;
        } else {
          fac_accel_dynamic = fac_accel_dynamic - 0.1;
        }

      } else {
        fac_accel_strong = fac_accel_strong + 0.25;
        fac_accel_recup = fac_accel_recup - 0.25;
        fac_accel_dynamic = fac_accel_dynamic + 0.25;
      }

      if (accelStd / accelMean > 0.3) {
        fac_accel_behaviour = fac_accel_behaviour + 0.2;
      } else {
        fac_accel_behaviour = fac_accel_behaviour - 0.1;
      }


      double factor = (fac_speed_lt30 + fac_speed_30120 + fac_speed_gt120 + fac_speed_behaviour + fac_stop_occurence + fac_stop_duration +
              fac_accel_strong + fac_accel_recup + fac_accel_dynamic + fac_accel_behaviour) / 10;

      co2TOTAL = co2_car_konv * factor * distance.get(distance.size() - 1) / 1000;

    } else {
      co2TOTAL = 0.0;
    }

    return co2TOTAL;
  }

  public static List<List<Double>> getCalcList(List<MyLocation> locs)
	{			
		List<Double> timeData = new ArrayList<Double>();
		List<Double> speedData = new ArrayList<Double>();
		List<Double> accelerationData = new ArrayList<Double>();
		List<Double> speedQualityData = new ArrayList<Double>();
		List<Double> distanceData = new ArrayList<Double>();
		List<Double> latitudeData = new ArrayList<Double>();
		List<Double> longitudeData = new ArrayList<Double>();
//		List<Double> hdopData = new ArrayList<Double>();
		

		if ( !locs.isEmpty() )
		{
			// get all Trackpoints
			MyLocation loc_last = locs.get(0);
			timeData.add( Double.valueOf(loc_last.getDate().getTime() / 1000) );
			speedData.add( (loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : 0.0 );
			latitudeData.add( loc_last.getLatitude() );
			longitudeData.add( loc_last.getLongitude() );
			distanceData.add(0.0);
			//accelerationData.add(0.0);
			
			speedQualityData.add( (loc_last.getSpeed() >= 0) ? 1.0 : 0.0 );
			
			double speedLastOK = (loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : 0.0;
			
			for ( int i = 1; i < locs.size(); i++ )
			{
				MyLocation loc_current = locs.get(i);
				
				double deltaTime = MyLocation.timeDiffBetween(loc_last, loc_current);
				speedLastOK = (loc_current.getSpeed() >= 0) ? loc_current.getSpeed() : speedLastOK;
				
				double currentFixedSpeed = (loc_current.getSpeed() >= 0) ? loc_current.getSpeed() : speedLastOK;
				
				if ( deltaTime == 1000 )
				{
					timeData.add( Double.valueOf(loc_current.getDate().getTime() / 1000) );
					speedData.add( loc_current.getSpeed() );
					latitudeData.add( loc_current.getLatitude() );
					longitudeData.add( loc_current.getLongitude() );
					
					// SpeedQuality auf 1 setzen fuer existierende Punkte
					speedQualityData.add(1.0);
					loc_last = loc_current;
				}
				// if there are only 5 seconds of no tracking --> ignore and interpolate
				else if ( deltaTime <= 5000 )
				{
					double timeDelta = deltaTime / 1000;
	
					// calc deltas of single attributes
					double latDelta = ( loc_current.getLatitude() - loc_last.getLatitude() ) / timeDelta;
					double longDelta = ( loc_current.getLongitude() - loc_last.getLongitude() ) / timeDelta;
					double speedDelta = ( currentFixedSpeed - ((loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : speedLastOK )) / timeDelta;
					
					// interpoliere einzelnen/wenige SpeedWert(e)
					for (int j = 1; j < timeDelta; j++)
					{
						// SpeedQuality auf 1 setzen
						speedQualityData.add(1.0);
						timeData.add( Double.valueOf(loc_last.getDate().getTime() / 1000) + j );
						latitudeData.add( loc_last.getLatitude() + ( j * latDelta) );
						longitudeData.add( loc_last.getLongitude() + ( j * longDelta) );
						
						speedData.add( ((loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : speedLastOK ) + ( j * speedDelta) );
					}
					
					// set current trackpoint
					timeData.add( Double.valueOf(loc_current.getDate().getTime() / 1000) );
					speedData.add( loc_current.getSpeed() );
					latitudeData.add( loc_current.getLatitude() );
					longitudeData.add( loc_current.getLongitude() );
					
					// SpeedQuality auf 1 setzen fuer existierende Punkte
					speedQualityData.add(1.0);
					
					loc_last = loc_current;
				}
				else
				{
					double timeDelta = deltaTime / 1000;
	
					// calc deltas of single attributes
					double latDelta = ( loc_current.getLatitude() - loc_last.getLatitude() ) / timeDelta;
					double longDelta = ( loc_current.getLongitude() - loc_last.getLongitude() ) / timeDelta;
					double speedDelta = ( currentFixedSpeed - ((loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : speedLastOK )) / timeDelta;
					
					// alle Werte interpolieren und SpeedQuality auf 0 setzen
					for (int j = 1; j < timeDelta; j++)
					{
						// SpeedQuality auf 0 setzen
						speedQualityData.add(0.0);
						timeData.add( Double.valueOf(loc_last.getDate().getTime() / 1000) + j );
						latitudeData.add( loc_last.getLatitude() + ( j * latDelta) );
						longitudeData.add( loc_last.getLongitude() + ( j * longDelta) );
						
						speedData.add( ((loc_last.getSpeed() >= 0) ? loc_last.getSpeed() : speedLastOK ) + ( j * speedDelta) );
					}
					
					// set current trackpoint
					timeData.add( Double.valueOf(loc_current.getDate().getTime() / 1000) );
					speedData.add( loc_current.getSpeed() );
					latitudeData.add( loc_current.getLatitude() );
					longitudeData.add( loc_current.getLongitude() );
					
					// SpeedQuality auf 1 setzen fuer existierende Punkte
					speedQualityData.add(1.0);
					
					loc_last = loc_current;
				}
			}
			
			
			/*
			 *  nach Interpolation accel und dist ausrechnen
			 */
			speedLastOK = (speedData.get(0) >= 0) ? speedData.get(0) : 0.0;
	
			// Gradient: dx = 1(sec)
	//		grad[0] = (vals[1] - vals[0]) / dx;
	//		grad[i] = (vals[i+1] - vals[i-1]) / (2*dx);  // for i in [1,N-2]
	//		grad[N-1] = (vals[N-1] - vals[N-2]) / dx;
			
			// First point
			accelerationData.add( speedData.get(1) - speedData.get(0) );
			
			for (int i = 1; i < timeData.size()-1; i++)
			{
				speedLastOK = (speedData.get(i) >= 0) ? speedData.get(i) : speedLastOK;
				
				double currentFixedSpeed = (speedData.get(i) >= 0) ? speedData.get(i) : speedLastOK;
	
				
				distanceData.add(distanceRadKM(latitudeData.get(i), longitudeData.get(i),
									latitudeData.get(i-1), longitudeData.get(i-1) ) * 1000 + distanceData.get(i-1) );
				
				
				accelerationData.add( ( speedData.get(i+1) - speedData.get(i-1) ) / 2 );
				
			}
			
			// Last Point
			distanceData.add(distanceRadKM(latitudeData.get(latitudeData.size()-1), longitudeData.get(longitudeData.size()-1),
					latitudeData.get(latitudeData.size()-2), longitudeData.get(longitudeData.size()-2) ) * 1000 + distanceData.get(distanceData.size()-1) );
			accelerationData.add( ( speedData.get(speedData.size()-1) - speedData.get(speedData.size()-2) ) / 1 );
		}
		
		List<List<Double>> result = new ArrayList<List<Double>>();
		result.add(timeData);
		result.add(speedData);
		result.add(accelerationData);
		result.add(distanceData);
		
		return result;
	}


  public static int[] getOccupationRatio(List<List<MyLocation>> locations) {
    int[] ratio = new int[locations.size()];
    Arrays.fill(ratio, 1);

    int firstSimilarity = -1;

    for (int k = 1; k < locations.size(); k++) {
      List<MyLocation> locs_current = locations.get(k);
      List<MyLocation> locs_previous = locations.get(k - 1);

      if (Math.abs(locs_current.get(0).getDate().getTime() - locs_previous.get(0).getDate().getTime()) < 5 * 60 * 1000) {
        boolean checkNow = checkSimilarity(locs_current, locs_previous);
        if (checkNow && firstSimilarity >= 0) {
          ratio[firstSimilarity]++;
          for (int j = k; j > firstSimilarity; j--) {
            ratio[j] = ratio[firstSimilarity];
          }

        } else if (checkNow) {
          firstSimilarity = k - 1;
          ratio[firstSimilarity]++;

          for (int j = k; j > firstSimilarity; j--) {
            ratio[j] = ratio[firstSimilarity];
          }
        } else {
          firstSimilarity = -1;
        }
      } else {

        firstSimilarity = -1;
      }
    }

    return ratio;
  }

  private static boolean checkSimilarity(List<MyLocation> locs_1, List<MyLocation> locs_2) {
    int count = 0;

    //get all time-points of reference list locs_2
    ArrayList<Long> times = new ArrayList<Long>();
    for (MyLocation ml : locs_2) {
      times.add(ml.getDate().getTime());
    }

    for (int i = 0; i < locs_1.size(); i++) {
      //if ( times.contains( locs_1.get(i).getDate().getTime() ) )
      int idx = times.indexOf(locs_1.get(i).getDate().getTime());
      if (idx != -1) {
        if (distanceRadKM(locs_1.get(i).getLatitude(), locs_1.get(i).getLongitude(), locs_2.get(idx).getLatitude(), locs_2.get(idx).getLongitude()) * 1000 < 10)
          count++;
      }

    }

    if (locs_1.size() > locs_2.size()) {
      if (count > (0.9 * locs_2.size()) && (locs_2.size() / locs_1.size()) > 0.9)
        return true;
      else
        return false;
    } else {
      if (count > (0.9 * locs_1.size()) && (locs_1.size() / locs_2.size()) > 0.9)
        return true;
      else
        return false;
    }

  }

  public static double getTotalDistance(List<List<List<Double>>> loc_details) {
    double sumDistance = 0.0;
    for (int i = 0; i < loc_details.size(); i++) {
      sumDistance = sumDistance + loc_details.get(i).get(3).get(loc_details.get(i).get(3).size() - 1);
    }
    return sumDistance / 1000;
  }


  /**
	 *  distance in kilometers
	 */
  public static double distanceRadKM(double lat1, double long1, double lat2, double long2) {
    double earthRadius = 6371.009;

    double lat1R = Math.toRadians(lat1);
    double lat2R = Math.toRadians(lat2);
    double dLatR = Math.abs(lat2R - lat1R);
    double dLngR = Math.abs(Math.toRadians(long2
            - long1));
    double a = Math.sin(dLatR / 2) * Math.sin(dLatR / 2) + Math.cos(lat1R)
            * Math.cos(lat2R) * Math.sin(dLngR / 2) * Math.sin(dLngR / 2);
    return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * earthRadius;
  }

  public static double getRealStdValue(List<Double> list) {
    double sum = 0.0;
    double sumOfSquares = 0.0;
    int count = 0;
    for (Double d : list) {
      if (d >= 0) {
        sum = sum + d;
        sumOfSquares = sumOfSquares + d * d;
        count++;
      }
    }

    return Math.sqrt((count * sumOfSquares - sum * sum) / (count * (count - 1)));
  }

  public static double getRealMeanValue(List<Double> list) {
    double sum = 0;
    int count = 0;
    for (Double d : list) {
      if (d >= 0) {
        sum = sum + d;
        count++;
      }
    }

    return (sum / count);

  }

  public static double getStdValue(List<Double> list) {
    int no = list.size();

    double sum = 0.0;
    double sumOfSquares = 0.0;
    for (Double d : list) {
      sum = sum + Math.abs(d);
      sumOfSquares = sumOfSquares + d * d;
    }

    return Math.sqrt((no * sumOfSquares - sum * sum) / (no * (no - 1)));
  }

  public static double getMeanValue(List<Double> list) {
    double sum = 0;
    for (Double d : list) {
      sum = sum + Math.abs(d);
    }
    return (sum / list.size());
  }

}
