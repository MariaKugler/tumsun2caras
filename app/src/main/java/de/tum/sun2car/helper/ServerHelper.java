package de.tum.sun2car.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.classes.DrivesDetails;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.classes.SmartmeterValue;

public class ServerHelper extends AsyncTask<String, Void, List<Drives>> {
	String serverURL;
	String username;
	String token;
	
	List<Drives> track_list = new ArrayList<Drives>();

	private static ServerHelper serverHelper;
	private Context mContext;
	private HttpClient httpclient;
	
	// https://www.mobiletrack.ftm.mw.tum.de/includes/v2/api.php?action=trackstime&token=<dasBeiLoginErhalteneTOKEN>

	public ServerHelper(String server, String username, String token) {
		this.serverURL = server;
		this.username = username;
		this.token = token;
	}

  // Test
  public void SetUserName(String username) {
    this.username = username;
  }
  public String getUsername() {
    return username;
  }
  public void SetToken(String token) {
    this.token = token;
  }
  public String getToken() {
    return token;
  }



  private ServerHelper(Context con) {
    mContext = con;
    httpclient = new DefaultHttpClient();
  }

  public static ServerHelper getInstance(Context con) {
    if (serverHelper == null) {
      serverHelper = new ServerHelper(con);
    }
    return serverHelper;
  }

  protected List<Drives> doInBackground(String... params) {
//		android.os.Debug.waitForDebugger();

    token = createNewToken(params[0]);
    ArrayList<NameValuePair> tracksPair = new ArrayList<NameValuePair>();
    tracksPair.add(new BasicNameValuePair("action", "trackstime"));
    tracksPair.add(new BasicNameValuePair("time", "0"));
    tracksPair.add(new BasicNameValuePair("offset", "0"));
    tracksPair.add(new BasicNameValuePair("token", token));

    try {
//			JSONObject json_data_tracksslist = getJSONfromServer("apitrack.php", tracksPair);
      JSONArray json_data_tracksslist = getJSONArrayfromServer("php_sun2car/api.php", tracksPair);
//			JSONArray tracksJson = json_data_tracksslist.getJSONArray("trackid");
      for (int i = 0; i < json_data_tracksslist.length(); i++) {
        JSONObject jsonDrive = json_data_tracksslist.getJSONObject(i);
        track_list.add(new Drives(jsonDrive));
      }
      return track_list;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized String createNewToken(String password) {
		
		try {
      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
			tokenPair.add(new BasicNameValuePair("action", "token"));
			tokenPair.add(new BasicNameValuePair("username", username));
			tokenPair.add(new BasicNameValuePair("password", password));
			JSONObject json_data_token = getJSONfromServer("php_sun2car/api.php", tokenPair);

      if(json_data_token.has("error_message")) {
        String error = json_data_token.getString("error_message");
        return error;
        //it has it, do appropriate processing
      }
      else {
        token = json_data_token.getString("token");
        return token;
      }
		}
		catch(Exception e)
		{
			e.printStackTrace();
        	return null;
		}
	}

  public synchronized String[] getIDs(String password) {
    String IDs[] = new String[2];  //0: userID, 1: familyID

    try {
      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "IDs"));
      tokenPair.add(new BasicNameValuePair("username", username));
      tokenPair.add(new BasicNameValuePair("password", password));
      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);
      IDs[0] = json_data_token.getString("userID");
      IDs[1] = json_data_token.optString("familyID");
      return IDs;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized int getDriver(String userid) {
    int driver;
    try {

      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "isDriver"));
      tokenPair.add(new BasicNameValuePair("userID", userid));
      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);
      driver = json_data_token.optInt("isDriver");
      return driver;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public synchronized String getSurname(String familyid) {
    String surname;
    try {
      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "getSurname"));
      tokenPair.add(new BasicNameValuePair("familyID", familyid));
      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);
      surname = json_data_token.optString("familySurname");
      return surname;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized String[] getPVSpecs(String familyid) {
    String pvSpecs[] = new String[6];
    // 0: pv_tiltangle
    // 1: pv_area
    // 2: pv_orientation
    // 3: pv_kwp
    // 4: pv_output
    // 5: pv_output_spec
    try {

      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "getPVspecs"));
      tokenPair.add(new BasicNameValuePair("familyID", familyid));

      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);

      pvSpecs[0] = json_data_token.optString("pv_tiltangle");
      pvSpecs[1] = json_data_token.optString("pv_area");
      pvSpecs[2] = json_data_token.optString("pv_orientation");
      pvSpecs[3] = json_data_token.optString("pv_kwp");
      pvSpecs[4] = json_data_token.optString("pv_output");
      pvSpecs[5] = json_data_token.optString("pv_output_spec");

      return pvSpecs;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized String getNewAPK(Context context) {

    String downloadUrl = serverURL + "php_sun2car/sun2car.php?action=getNewAPK";
    InputStream inputStream = null;
    OutputStream outputStream = null;
    File file = new File(context.getResources().getString(R.string.downloadPath) + "/TUMsun2car.apk");

    try {
      URL url = new URL(downloadUrl);
      inputStream = url.openStream();

      if (file.exists())
        file.delete();
      file.createNewFile();

      outputStream = new FileOutputStream(file);

      byte data[] = new byte[1024];
      int count;
      while ((count = inputStream.read(data)) != -1) {
        outputStream.write(data, 0, count);
      }

      outputStream.flush();
      outputStream.close();
      inputStream.close();

      //Scanner input = new Scanner(new File(context.getResources().getString(R.string.apkPath) + "/TUMsun2car.apk"));

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (inputStream != null) {
        try {
          inputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return null;
  }

  public synchronized String getAPKVersion() {
    String version;
    try {

      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "isNewAPK"));

      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);

      version = json_data_token.optString("currentVersion");

      return version;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized List<MyLocation> getLocations(Double trackID) {
    NumberFormat formatter = new DecimalFormat("###.#####");
    String s_trackID = formatter.format(trackID);
    List<MyLocation> locs = new ArrayList<MyLocation>();
    ArrayList<NameValuePair> locsPair = new ArrayList<NameValuePair>();
    locsPair.add(new BasicNameValuePair("action", "getLocation"));
    locsPair.add(new BasicNameValuePair("trackid", s_trackID));

    try {
      JSONArray json_data_locslist = getJSONArrayfromServer("php_sun2car/sun2car.php", locsPair);

      for (int i = 0; i < json_data_locslist.length(); i++) {
        JSONObject jsonLocs = json_data_locslist.getJSONObject(i);

        locs.add(new MyLocation(jsonLocs));
      }
      return locs;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  // Entfernung des Passworts, da eh über den Token kommuniziert wird.
  // => Generierung von einem neuen Token nicht notwendig
  public synchronized String setDriveInfos(int purpose, int planned, int electric, Long duration, Double distance, Long ts) {
    ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
    args.add(new BasicNameValuePair("action", "setDriveInfo"));
    args.add(new BasicNameValuePair("token", token));
    args.add(new BasicNameValuePair("drivePurpose", String.valueOf(purpose)));
    args.add(new BasicNameValuePair("drivePlanned", String.valueOf(planned)));
    args.add(new BasicNameValuePair("driveElectric", String.valueOf(electric)));
    args.add(new BasicNameValuePair("driveDuration", String.valueOf(duration)));
    args.add(new BasicNameValuePair("driveDistance", distance.toString()));
    args.add(new BasicNameValuePair("driveStartTime", String.valueOf(ts)));

    try {
      JSONObject json = getJSONfromServer("php_sun2car/sun2car.php", args);
      return json.optString("success", "0");
    } catch (ServerCommunicationException e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized List<DrivesDetails> getDriveInfos(String userID) {
    List<DrivesDetails> infos_list = new ArrayList<DrivesDetails>();

    ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
    args.add(new BasicNameValuePair("action", "getDriveInfo"));
    args.add(new BasicNameValuePair("userID", userID));

    try {
      JSONArray json_data_infoslist = getJSONArrayfromServer("php_sun2car/sun2car.php", args);

      for (int i = 0; i < json_data_infoslist.length(); i++) {
        JSONObject jsonInfos = json_data_infoslist.getJSONObject(i);

        infos_list.add(new DrivesDetails(jsonInfos));
      }

      return infos_list;

    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  // Entfernung des Passworts, da eh über den Token kommuniziert wird.
  // => Generierung von einem neuen Token nicht notwendig
  public synchronized String setCorrectedMOT(String newMOT, Double trackID) {
    ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
    args.add(new BasicNameValuePair("action", "setCorrectedMOT"));
    args.add(new BasicNameValuePair("token", token));
    args.add(new BasicNameValuePair("newMOT", newMOT));
    args.add(new BasicNameValuePair("trackID", String.valueOf(trackID)));

    try {
      JSONObject json = getJSONfromServer("php_sun2car/sun2car.php", args);
      return json.optString("success", "0");

    } catch (ServerCommunicationException e) {
      e.printStackTrace();
      return null;
    }
  }

  // Entfernung des Passworts, da eh über den Token kommuniziert wird.
  // => Generierung von einem neuen Token nicht notwendig
  public synchronized String setCO2Emission(Double co2trip, Double trackID) {
    ArrayList<NameValuePair> args = new ArrayList<NameValuePair>();
    args.add(new BasicNameValuePair("action", "setEmission"));
    args.add(new BasicNameValuePair("token", token));
    args.add(new BasicNameValuePair("trackID", String.valueOf(trackID)));
    args.add(new BasicNameValuePair("emissionTrip", String.valueOf(co2trip)));

    try {
      JSONObject json = getJSONfromServer("php_sun2car/sun2car.php", args);
      // Log.v("TrackID: ", String.valueOf(trackID));
      return json.optString("success", "0");

    } catch (ServerCommunicationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  public synchronized List<Drives> getFamilyTracks(String familyID)
	{
		List<Drives> familyDrives = new ArrayList<Drives>();
		
		ArrayList<NameValuePair> tracksPair = new ArrayList<NameValuePair>();
		tracksPair.add(new BasicNameValuePair("action", "getFamilyTracks"));
		tracksPair.add(new BasicNameValuePair("familyID", familyID));
		
		try
		{
			JSONArray json_data_tracksslist = getJSONArrayfromServer("php_sun2car/sun2car.php", tracksPair);
			
//			JSONArray tracksJson = json_data_tracksslist.getJSONArray("trackid");

			for (int i = 0; i < json_data_tracksslist.length(); i++)
			{
				JSONObject jsonDrive = json_data_tracksslist.getJSONObject(i);
		
				familyDrives.add(new Drives(jsonDrive));	
			}
                 
			return familyDrives;
	

		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
	}

  public synchronized List<String> getFamilyMembers(String familyID) {
    List<String> familyMembers = new ArrayList<String>();

    ArrayList<NameValuePair> tracksPair = new ArrayList<NameValuePair>();
    tracksPair.add(new BasicNameValuePair("action", "getFamilyMembers"));
    tracksPair.add(new BasicNameValuePair("familyID", familyID));

    try {
      JSONArray json_data_tracksslist = getJSONArrayfromServer("php_sun2car/sun2car.php", tracksPair);

//			JSONArray tracksJson = json_data_tracksslist.getJSONArray("trackid");

      for (int i = 0; i < json_data_tracksslist.length(); i++) {
        JSONObject jsonMember = json_data_tracksslist.getJSONObject(i);

        familyMembers.add(jsonMember.optString("userid"));
      }

      return familyMembers;


    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

  }

  public synchronized String getCSVVersion() {
    String version;
    try {

      ArrayList<NameValuePair> tokenPair = new ArrayList<NameValuePair>();
      tokenPair.add(new BasicNameValuePair("action", "isNewCSV"));

      JSONObject json_data_token = getJSONfromServer("php_sun2car/sun2car.php", tokenPair);

      version = json_data_token.optString("currentVersion");

      return version;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public synchronized void getNewConsumption(Context con, String familyid, int which) {
    SmartHomeDB smarthomeSource = new SmartHomeDB(con);
    smarthomeSource.open();

    SmartLoadingDB smartloadingSource = new SmartLoadingDB(con);
    smartloadingSource.open();

    SmartPVDB smartpvSource = new SmartPVDB(con);
    smartpvSource.open();

    String[] smvFiles = {"Home", "Loading", "PV"};


    ArrayList<NameValuePair> smartmeteringPair = new ArrayList<NameValuePair>();
    smartmeteringPair.add(new BasicNameValuePair("action", "getSmartMetering"));
    smartmeteringPair.add(new BasicNameValuePair("familyID", familyid));
    smartmeteringPair.add(new BasicNameValuePair("switch", smvFiles[which]));

    try {
      JSONArray json_data_smlist = getJSONArrayfromServer("php_sun2car/sun2car.php", smartmeteringPair);

//			JSONArray tracksJson = json_data_tracksslist.getJSONArray("trackid");

      for (int i = 0; i < json_data_smlist.length(); i++) {
        JSONObject jsonSMV = json_data_smlist.getJSONObject(i);

        SmartmeterValue smv = new SmartmeterValue();
        smv.setEnergy(jsonSMV.optDouble("energy"));

        String time = jsonSMV.optString("time");
        Date date = new Date();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss,sssss"); // YYYY.MM.DD HH24:MI.SS
        try {
          date = simpleDateFormat.parse(time);
        } catch (ParseException ex) {
          System.out.println("Exception " + ex);
        }

        smv.setDate(date);

        switch (which) {
          case 0:
            smarthomeSource.addValues(smv);
            break;
          case 1:
            smartloadingSource.addValues(smv);
            break;
          case 2:
            smartpvSource.addValues(smv);
            break;
          default:
            break;
        }

      }


    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public List<Drives> onPostExecute(Boolean result) {
        // The results of the above method
        // Processing the results here
        return track_list;
    }



	/**
	 * von sun2car/FTM-Server auslesen
	 */
	public JSONArray getJSONArrayfromServer(String page, List<NameValuePair> args) throws ServerCommunicationException {
		try{
			String json = getStringFromServer(page, args);
			
//			int start = json.indexOf("{");
//			int end = json.lastIndexOf("}");
//			
//			if (json.length() == 0) return null;
//			
//			if (start>0){
//				json = json.substring(start, end+1);
//			}
			return new JSONArray(json);
			
		}catch (JSONException e) {
			throw new ServerCommunicationException(e.toString());
		}

	}

  public JSONObject getJSONfromServer(String page, List<NameValuePair> args) throws ServerCommunicationException {
    try {
      String json = getStringFromServer(page, args);
      int start = json.indexOf("{");
      int end = json.lastIndexOf("}");
      if (json.length() == 0) return null;
      if (start > 0) {
        json = json.substring(start, end + 1);
      }
      return new JSONObject(json);
    } catch (JSONException e) {
      throw new ServerCommunicationException(e.toString());
    }
  }

  public String getStringFromServer(String page, List<NameValuePair> args) throws ServerCommunicationException {
    try {
      InputStream is = this.getContentFromServer(page, args);
      BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
      reader.close();
      return sb.toString();
    } catch (ClientProtocolException e) {
      throw new ServerCommunicationException(e.toString());
    } catch (IOException e) {
      throw new ServerCommunicationException(e.toString());
    }
  }

  // Anpassung an anderen Http-Client
  public InputStream getContentFromServer(String page, List<NameValuePair> options) throws ClientProtocolException, IOException {
    InputStream is = null;
    HttpClient httpclient = createHttpClient();
    String params = "?";
    for (int i = 0; i < options.size(); i++) {
      params = params + options.get(i).getName() + "=" + options.get(i).getValue();
      if (i < (options.size() - 1))
        params = params + "&";
    }
    HttpGet httpget = new HttpGet(this.serverURL + page + params);
    HttpResponse response = httpclient.execute(httpget);
    HttpEntity entity = response.getEntity();
    is = entity.getContent();
    return is;
  }

  // HTTP-Client method to user HTTPS and HTTP
  private HttpClient createHttpClient() {
    HttpParams params = new BasicHttpParams();
    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
    HttpProtocolParams.setUseExpectContinue(params, true);

    SchemeRegistry schReg = new SchemeRegistry();
    schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

    return new DefaultHttpClient(conMgr, params);
  }

}



