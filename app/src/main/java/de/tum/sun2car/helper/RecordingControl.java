package de.tum.sun2car.helper;

import java.io.File;
import java.io.IOException;

import android.media.MediaRecorder;

public class RecordingControl
{
	private File voice = null;
	private MediaRecorder recorder = null;
	private RecordState state = RecordState.STOP;
	public enum RecordState
	{
		RECORDING,STOP
	}
	
	public RecordingControl(File vfile)
	{
		voice = vfile;;
		init();
	}
	
	protected void init()
	{
		if(recorder == null)
		{
			recorder = new MediaRecorder();
			recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
			recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
		}
		
		recorder.setOutputFile(voice.getAbsolutePath());

		try
		{
			recorder.prepare();
		} catch (IllegalStateException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void startRecord()
	{
		recorder.start();
		state = RecordState.RECORDING;
	}

	// stop recording
	public void endRecord()
	{
		recorder.stop();
		recorder.release();
		recorder = null;
		state = RecordState.STOP;
	}
	
	public RecordState getState()
	{
		return state;
	}

}
