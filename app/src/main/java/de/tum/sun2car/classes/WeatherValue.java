package de.tum.sun2car.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WeatherValue implements Comparable<WeatherValue>
{
	private double clouds;
	private double windSpeed;
	private double rain;
	private double temp;
	private double humidity;
	private Date date;
	private double radiation;
	
	public WeatherValue(JSONObject json) throws JSONException
	{
		JSONObject jsO = json.getJSONObject("clouds");
		
		
		this.clouds = json.getJSONObject("clouds").optDouble("all", 0);	// %
		this.windSpeed = json.getJSONObject("wind").optDouble("speed");	// m/sec
        //this.rain = snow or rain
        if(json.has("rain")) this.rain = json.getJSONObject("rain").optDouble("3h", 0) / 3;
        else if(json.has("snow")) this.rain = json.getJSONObject("snow").optDouble("3h", 0) / 3;
        else this.rain = 0.0;
		this.temp = json.getJSONObject("main").optDouble("temp"); // K
		this.humidity = json.getJSONObject("main").optDouble("humidity");
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
        	this.date = simpleDateFormat.parse(json.optString("dt_txt"));
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
        
        /* GlobalStrahlung Wh/m2 abschätzen
		 -2.4535 * hour +
	     -4.1236 * Luftfeuchte [%]+
	     -9.4573 * Niederschlag [mm in 3 Stunden]+
	   7461.9569 * LOG_Temp273_20cm +
	 -17779.3489
	  */
        double param_hour = -2.4535;
        double param_humidity = -4.1236;
        double param_rain = -9.4573;
        double param_logTemp = 7461.9569;
        double param_beta0 = -17779.3489;
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.date);
        
        this.radiation = param_hour * cal.get(Calendar.HOUR_OF_DAY)
        				+ param_humidity * this.humidity
        				+ param_rain * this.rain
        				+ param_logTemp * Math.log10(this.temp)
        				+ param_beta0;        
	}
	
	public double getClouds() {
		return clouds;
	}
	public void setClouds(double clouds) {
		this.clouds = clouds;
	}
	public double getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public double getRain() {
		return rain;
	}
	public void setRain(double rain) {
		this.rain = rain;
	}
	public double getTemp() {
		return temp;
	}
	public void setTemp(double temp) {
		this.temp = temp;
	}
	public double getHumidity() {
		return humidity;
	}
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public double getRadiation() {
		return radiation;
	}

	public void setRadiation(double radiation)
	{
		this.radiation = radiation;
	}
	
	@Override
	public int compareTo(WeatherValue another) {
		// TODO Auto-generated method stub
		return date.compareTo(another.date);
	}


}
