package de.tum.sun2car.footprint;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import de.tum.sun2car.R;
import de.tum.sun2car.classes.Drives;
import de.tum.sun2car.classes.MyLocation;
import de.tum.sun2car.helper.DrivesDB;
import de.tum.sun2car.helper.FamilyDB;
import de.tum.sun2car.helper.LocationDB;
import de.tum.sun2car.helper.ServerHelper;
import de.tum.sun2car.tumsensorrecorderlib.Preferences;

public class FootprintFamily extends FragmentActivity implements OnDateSetListener
{
	List<Drives> drives;
	List<String> members;
	String user_token;
	String userName;
	String password;
	Context con;
	
	private LocationDB locsSource;
	private DrivesDB drivesSource;
	private FamilyDB familySource;
	
	ImageView iv_co2;
	FrameLayout fl_footprint;
	TextView tv_startdate;
	TextView tv_enddate;
	TextView tv_co2_day;
	TextView tv_co2_km;
	ImageButton btn_go;
	Calendar calendarStart;
	Calendar calendarEnd;
	
	List<Double> co2g_total = new ArrayList<Double>();
	List<List<List<Double>>> locs_details = new ArrayList<List<List<Double>>>();
	
	private SharedPreferences settings;
	private SharedPreferences prefs;
    
    private ProgressDialog dialog;

    boolean startClicked = false;
    boolean endClicked = false;

	double persons_avg_family = 3.6;
    //double co2_avg_year = 4 * 1000 * persons_avg_family;	// kg co2 per person per year
	double co2_avg_km = 130;
    double co2_avg_year = co2_avg_km * 30 * 365 * persons_avg_family;	// kg co2 per person per year
	double co2_avg_month = co2_avg_year / 12;
	
	double persons_family;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_footprint_family);

		fl_footprint = (FrameLayout) findViewById(R.id.footprint_layout_family);
		
		iv_co2 = (ImageView) findViewById(R.id.footprint_family_gradient_middle);
		tv_startdate = (TextView) findViewById(R.id.footprint_family_start);
		tv_enddate = (TextView) findViewById(R.id.footprint_family_end);
		tv_co2_day = (TextView) findViewById(R.id.footprint_family_co2_day);
		tv_co2_km = (TextView) findViewById(R.id.footprint_family_co2_km);
		btn_go = (ImageButton) findViewById(R.id.btn_go);
		
		con = this;
		
		drives = new ArrayList<Drives>();
		members = new ArrayList<String>();

		settings = getSharedPreferences(Preferences.PREF_FILE_NAME, MODE_PRIVATE);
		prefs = getSharedPreferences("SCPrefs", MODE_PRIVATE);


        calendarStart = Calendar.getInstance();  
        calendarEnd = Calendar.getInstance();  
		
		dialog = new ProgressDialog(this);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setTitle("Fahrtdaten laden...");
    	dialog.setMessage("Fahrtdaten werden geladen.");
    	dialog.setCanceledOnTouchOutside(false);
	}
	
	public void onResume()
	{
    	super.onResume();
    	if ( drives.size() != 0 )
    	{
    		// get a calendar using the default time zone and locale.  
    		calendarStart.setTime(drives.get(0).getStartDate());
	        
	        calendarEnd.setTime(drives.get(drives.size()-1).getStopDate());
			
			tv_startdate.setText(calendarStart.get(Calendar.DAY_OF_MONTH) + "." +
					(calendarStart.get(Calendar.MONTH)+1) + "." + 
					calendarStart.get(Calendar.YEAR));
			
			tv_enddate.setText(calendarEnd.get(Calendar.DAY_OF_MONTH) + "." +
					(calendarEnd.get(Calendar.MONTH)+1) + "." + 
					calendarEnd.get(Calendar.YEAR));
			
			updateTracksRange(calendarStart, calendarEnd);
    	}
    	else
    	{
    		
			locsSource = new LocationDB(this);
			locsSource.open();
			
			drivesSource = new DrivesDB(this);
			drivesSource.open();
			
			familySource = new FamilyDB(this);
			familySource.open();
	    	
			dialog.show();
			
	    	new Thread(new Runnable() {
				public void run()
				{
					members = familySource.getAllMembers(prefs.getString("FamilyID", ""));
					
			    	userName = settings.getString(Preferences.cUsername,null);
			    	password = settings.getString(Preferences.cPass, null);
					
			
					/**
					 * get Family Tracks
					 */
			    	for ( int m = 0; m < members.size(); m++ )
					{
			    		drives.addAll( drivesSource.getAllDrivesByUser(members.get(m)) );
					}
			    	
			    	if ( drives.isEmpty() )
			    	{
			    		Handler handler = new Handler(Looper.getMainLooper());
				    	handler.post(new Runnable()
				    	{
	
							@Override
							public void run()
							{
					    		Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
								dialog.dismiss();
							} 
				    	});
			    	}
			    	else
			    	{
						Collections.sort(drives, Collections.reverseOrder());
						for (int i = 0; i < drives.size(); i++ )
    					{
    						int occupation = drives.get(i).getOccupation();
    						
    						co2g_total.add(drives.get(i).getCo2gTotal() / occupation);
    					}
						
//						for (int i = 0; i < smvData.size(); i++ )
//						{
//							Double trackID = smvData.get(i).getTrackID();
//							List<MyLocation> locs = locsSource.getAllLocationsByTrack(trackID);
//							if ( !locs.isEmpty() )
//								locations.add(locs);
//				//			locations.add( serverHelper.getLocations(trackID) );
//						}
//						
//						/**
//						 * check for Occupation ratio
//						 */
//						int[] occupation = FootprintCalc.getOccupationRatio(locations);
//						
//						for (int i = 0; i < locations.size(); i++ )
//						{						
//							// interpolate values and calc accel and distance
//				    		List<List<Double>> locs = FootprintCalc.getCalcList(locations.get(i));
//							locs_details.add(locs);
//				    		
//				//	    	0: timeData
//				//			1: speedData
//				//			2: accelerationData
//				//			3: distanceData --> aufsummiert
//				//	    	List<Double> time = locs.get(0);
//				//	    	List<Double> speed = locs.get(1);
//				//	    	List<Double> accel = locs.get(2);
//				//	    	List<Double> distance = locs.get(3);
//					    	Double co2Trip = FootprintCalc.calculateCO2Trip(locs.get(1), locs.get(2), locs.get(3), smvData.get(i).getTrip_mot());
//					    	
//					    	co2g_total.add(co2Trip / occupation[i]);
//					    	
//						}
						
						dialog.dismiss();
						
						
						Handler handler = new Handler(Looper.getMainLooper());
				    	handler.post(new Runnable()
				    	{
		
							@Override
							public void run()
							{
								// get a calendar using the default time zone and locale.  
								calendarStart.setTime(drives.get(0).getStartDate());
        				        
        				        calendarEnd.setTime(drives.get(drives.size()-1).getStopDate());
								
								tv_startdate.setText(calendarStart.get(Calendar.DAY_OF_MONTH) + "." +
										(calendarStart.get(Calendar.MONTH)+1) + "." + 
										calendarStart.get(Calendar.YEAR));
								
								tv_enddate.setText(calendarEnd.get(Calendar.DAY_OF_MONTH) + "." +
										(calendarEnd.get(Calendar.MONTH)+1) + "." + 
										calendarEnd.get(Calendar.YEAR));
								
								

	    						updateTracksRange(calendarStart, calendarEnd);
							} 
				    	});
						
			    	}
					
			}}).start();
    	}
		
		
    	
	}
	
	public void onChangeStart(View v)
	{	
		startClicked = true;
		
		DialogFragment newFragment = new DatePicker();
		newFragment.show(getSupportFragmentManager(), "startDatePicker");	    
	}
	
	

	public void onChangeEnd(View v)
	{
		endClicked = true;
		
		DialogFragment newFragment = new DatePicker();
	    newFragment.show(getSupportFragmentManager(), "endDatePicker");
	}
	
	
	public void onGo(View v)
	{
		if ( drives.isEmpty() )
		{
			dialog.show();
			
			new Thread(new Runnable() {
				public void run()
				{
					members = familySource.getAllMembers(prefs.getString("FamilyID", ""));
					
			    	userName = settings.getString(Preferences.cUsername,null);
			    	password = settings.getString(Preferences.cPass, null);
					
		
					/**
					 * get Family Tracks
					 */
			    	for ( int m = 0; m < members.size(); m++ )
					{
			    		drives.addAll( drivesSource.getAllDrivesByUser(members.get(m)) );
					}
			    	
			    	if ( drives.isEmpty() )
			    	{
			    		Handler handler = new Handler(Looper.getMainLooper());
				    	handler.post(new Runnable()
				    	{
		
							@Override
							public void run()
							{
					    		Toast.makeText(con, "Noch keine Tracks synchronisiert.", Toast.LENGTH_LONG).show();
							} 
				    	});
			    	}
			    	else
			    	{
						Collections.sort(drives, Collections.reverseOrder());

						for (int i = 0; i < drives.size(); i++ )
    					{
    						int occupation = drives.get(i).getOccupation();
    						
    						co2g_total.add(drives.get(i).getCo2gTotal() / occupation);
    					}
					
//						for (int i = 0; i < smvData.size(); i++ )
//						{
//							Double trackID = smvData.get(i).getTrackID();
//							List<MyLocation> locs = locsSource.getAllLocationsByTrack(trackID);
//							if ( !locs.isEmpty() )
//								locations.add(locs);
//				//			locations.add( serverHelper.getLocations(trackID) );
//						}
//						
//						/**
//						 * check for Occupation ratio
//						 */
//						int[] occupation = FootprintCalc.getOccupationRatio(locations);
//						
//						for (int i = 0; i < locations.size(); i++ )
//						{						
//							// interpolate values and calc accel and distance
//				    		List<List<Double>> locs = FootprintCalc.getCalcList(locations.get(i));
//							locs_details.add(locs);
//				    		
//				//	    	0: timeData
//				//			1: speedData
//				//			2: accelerationData
//				//			3: distanceData --> aufsummiert
//				//	    	List<Double> time = locs.get(0);
//				//	    	List<Double> speed = locs.get(1);
//				//	    	List<Double> accel = locs.get(2);
//				//	    	List<Double> distance = locs.get(3);
//					    	Double co2Trip = FootprintCalc.calculateCO2Trip(locs.get(1), locs.get(2), locs.get(3), smvData.get(i).getTrip_mot());
//					    	
//					    	co2g_total.add(co2Trip / occupation[i]);
//					    	
//						}
			    	}
					
					dialog.dismiss();


					Handler handler = new Handler(Looper.getMainLooper());
			    	handler.post(new Runnable()
			    	{

						@Override
						public void run()
						{
							updateTracksRange(calendarStart, calendarEnd);
						} 
			    	});

			}}).start();
			
				    	
		}
		else
		{
			updateTracksRange(calendarStart, calendarEnd);
		}
	}
	
	
	@Override
	public void onDateSet(android.widget.DatePicker view, int year,
			int monthOfYear, int dayOfMonth)
	{
		FragmentManager fragmanager = getSupportFragmentManager();

	    if (fragmanager.findFragmentByTag("startDatePicker") != null)
	    {
	    	tv_startdate.setText(dayOfMonth + "." +
					(monthOfYear+1) + "." + 
					year);
			calendarStart.set(year, monthOfYear, dayOfMonth);
//			calendarStart.set(year, monthOfYear, dayOfMonth, 0, 0);
			
//			updateTracksRange(calendarStart, calendarEnd);
	    }
	    
	    if (fragmanager.findFragmentByTag("endDatePicker") != null)
	    {
	    	tv_enddate.setText(dayOfMonth + "." +
					(monthOfYear+1) + "." + 
					year);
			calendarEnd.set(year, monthOfYear, dayOfMonth);
//			calendarEnd.set(year, monthOfYear, dayOfMonth, 23, 59);
			
//			updateTracksRange(calendarStart, calendarEnd);
	    }
		
		
	}

	
	private void updateTracksRange(Calendar start, Calendar end)
	{
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		end.set(Calendar.HOUR_OF_DAY, 23);
		end.set(Calendar.MINUTE, 59);
		
		List<Integer> idx = new ArrayList<Integer>();	
//		
//		if ( start.get(Calendar.YEAR) == end.get(Calendar.YEAR) &&
//                start.get(Calendar.DAY_OF_YEAR) == end.get(Calendar.DAY_OF_YEAR) )
//		{
//			end.add(Calendar.DATE, 1);
//		}
		
//		this.compareTo(that)
//		returns
//		a negative int if this < that
//		0 if this == that
//		a positive int if this > that

		// find tracks between StartDate and EndDate
		for ( int i = 0; i < drives.size(); i++ )
		{
			Date startDate = drives.get(i).getStartDate();
			Date stopDate = drives.get(i).getStopDate();
 
	        
			if ( startDate.after(start.getTime()) && stopDate.before(end.getTime()) )
			{
				idx.add(i);
			}
		}
		
		if (idx.size() > 0)
		{
			refreshFootprint(idx);
		}
		else
		{
			Toast.makeText(FootprintFamily.this, "Keine Fahrten im angegebenen Zeitraum.",
	                Toast.LENGTH_LONG).show();
		}
	}


	private void refreshFootprint(List<Integer> idx)
	{
		double sumCO2g = 0.0;
		double sumDistance = 0.0;
		for (int i = 0; i < idx.size(); i++)
		{
			int which = idx.get(i);
			sumCO2g = sumCO2g + co2g_total.get(which);
			sumDistance = sumDistance + ( drives.get(which).getTrip_length() / 1000 );
		}
		
		double sumCO2kg = sumCO2g / 1000;
		
		
		// get timediff of tracks
		int numberOfMonths = (calendarEnd.get(Calendar.YEAR) - calendarStart.get(Calendar.YEAR)) * 12 
				+ (calendarEnd.get(Calendar.MONTH) - calendarStart.get(Calendar.MONTH)) + 1;
		
		int numberOfDays = (calendarEnd.get(Calendar.YEAR) - calendarStart.get(Calendar.YEAR)) * 365 
				+ (calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarStart.get(Calendar.DAY_OF_YEAR)) + 1;							

		double sumCO2kgPerDay = sumCO2kg / numberOfDays;
		double avgCO2gPerKM = sumCO2g / sumDistance;
		
		// CO2-Percentage
		double scaling;
		if  ( avgCO2gPerKM > co2_avg_km )
			scaling = 0;
		else
			scaling = (co2_avg_km - avgCO2gPerKM) / co2_avg_km > 0.9 ? 0.9 : (co2_avg_km - avgCO2gPerKM) / co2_avg_km;
			
		/**
		 * Footprint with gradient color
		 */
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		
		int frame_height = fl_footprint.getHeight();
		
		int height = size.y;
		
//		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, (int) (scaling * frame_height));
//		iv_co2.setLayoutParams(layoutParams);
//		iv_co2.setBackgroundResource(R.drawable.footprint_gradient_middle);
		
		/**
		 * Footprint with single traffic light color
		 */							
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT);
		iv_co2.setLayoutParams(layoutParams);
		
		double ratio = avgCO2gPerKM / co2_avg_km;
		// bad
		if ( ratio > 1 )
		{
			iv_co2.setBackgroundResource(R.drawable.footprint_gradient_red);
		}
		// middle
		else if ( ratio > 0.75 )
		{
			iv_co2.setBackgroundResource(R.drawable.footprint_gradient_yellow);
		}
		// good
		else
		{
			iv_co2.setBackgroundResource(R.drawable.footprint_gradient_green);
		}
		
		/**
		 * set textviews
		 */
		DecimalFormat df = new DecimalFormat("#.##");      // 2 Stellen
		tv_co2_day.setText("" + df.format( sumCO2kgPerDay * 1000 ) );
		tv_co2_km.setText("" +  df.format( avgCO2gPerKM ) );
		
	}

	
	@SuppressLint("ValidFragment")
	public class DatePicker extends DialogFragment{

	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the current date as the default date in the picker
	        final Calendar c = Calendar.getInstance();
	        int year = c.get(Calendar.YEAR);
	        int month = c.get(Calendar.MONTH);
	        int day = c.get(Calendar.DAY_OF_MONTH);

	        DatePickerDialog d = new DatePickerDialog(getActivity(), (FootprintFamily)getActivity(), year, month, day); 
	        android.widget.DatePicker dp = d.getDatePicker(); 
	        
//        	dp.setMinDate(locations.get(0).get(0).getDate().getTime());
//        	dp.setMaxDate(locations.get(locations.size()-1).get(locations.get(locations.size()-1).size()-1).getDate().getTime());
	        
	        if ( startClicked && !drives.isEmpty() )
		    {
	        	dp.setMinDate(drives.get(0).getStartDate().getTime());
	        	dp.setMaxDate(calendarEnd.getTimeInMillis());
	        	startClicked = false;
		    }
	        else if ( startClicked )
	        {
	        	startClicked = false;
	        }
		    
		    if ( endClicked && !drives.isEmpty() )
		    {
		    	dp.setMinDate(calendarStart.getTimeInMillis());
	        	dp.setMaxDate(drives.get(drives.size()-1).getStopDate().getTime());
	        	endClicked = false;
		    }
		    else if ( endClicked )
		    {
		    	endClicked = false;
		    }
	        
	        return d;
	        
	    }
	}
	
	
	public boolean isOnline() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

	    if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.footprint, menu);
		return true;
	}

}
