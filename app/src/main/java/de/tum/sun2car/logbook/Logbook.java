package de.tum.sun2car.logbook;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import de.tum.sun2car.R;

@SuppressWarnings("deprecation")
public class Logbook extends TabActivity {


  private TabHost tabHost;
  Parcelable state;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_logbook);


    tabHost = getTabHost();  // The activity TabHost
    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
    Intent intent;  // Reusable Intent for each tab

    /**
     * Tab LogbookDrives
     */
    // set Tab Text
    final TextView tvDrives = new TextView(this);
    tvDrives.setText(R.string.logbook_drives);
    tvDrives.setTextColor(Color.WHITE);
    tvDrives.setTextSize(18);
    tvDrives.setPadding(2, 20, 2, 20);
    tvDrives.setGravity(25);

    // Create an Intent to launch an Activity for the tab (to be reused)
    intent = new Intent().setClass(this, LogbookDrives.class);

    Bundle b = new Bundle();
    b = getIntent().getExtras();

    if (b != null) {
      state = b.getParcelable("lv_state");
      b.putParcelable("lv_state", state);
      intent.putExtras(b);
    }

    // Initialize a TabSpec for each tab and add it to the TabHost
    spec = tabHost.newTabSpec("smvData").setIndicator(tvDrives)
            .setContent(intent);
    tabHost.addTab(spec);

    /**
     * Tab LogbookStatistics
     */
    // set Tab Text
    final TextView tvStatistics = new TextView(this);
    tvStatistics.setText(R.string.logbook_statistics);
    tvStatistics.setTextColor(Color.parseColor("#006ab3"));
    tvStatistics.setTextSize(18);
    tvStatistics.setPadding(2, 20, 2, 20);
    tvStatistics.setGravity(25);

    intent = new Intent().setClass(this, LogbookStatistics.class);
    spec = tabHost.newTabSpec("statistics").setIndicator(tvStatistics)
            .setContent(intent);
    tabHost.addTab(spec);

    tabHost.setCurrentTab(0);

    // current tab: blue, other: white
    for (int j = 0; j < tabHost.getTabWidget().getChildCount(); j++) {
      tabHost.getTabWidget().getChildAt(j).setBackgroundColor(Color.WHITE);
    }
    tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));


    tabHost.setOnTabChangedListener(new OnTabChangeListener() {
      public void onTabChanged(String arg0) {
        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
          tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
        }
        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#006ab3"));
        if (tabHost.getCurrentTab() == 0) {
          tvDrives.setTextColor(Color.WHITE);
          tvStatistics.setTextColor(Color.parseColor("#006ab3"));
        } else {
          tvDrives.setTextColor(Color.parseColor("#006ab3"));
          tvStatistics.setTextColor(Color.WHITE);
        }
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.logbook, menu);
    return true;
  }

    @Override
    public void onBackPressed() {
        Log.i("hilfe", "nein?");
        super.onBackPressed();
    }
}
