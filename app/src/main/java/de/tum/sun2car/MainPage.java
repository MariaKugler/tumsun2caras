package de.tum.sun2car;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;

import de.tum.sun2car.balance.Balance;
import de.tum.sun2car.feedback.Feedback;
import de.tum.sun2car.footprint.Footprint;
import de.tum.sun2car.helper.SyncSun2CarService;
import de.tum.sun2car.helper.SyncSun2CarService.SyncBinder;
import de.tum.sun2car.info.Info;
import de.tum.sun2car.logbook.Logbook;
import de.tum.sun2car.newdrive.NewDrive;
import de.tum.sun2car.prediction.Prediction;
import de.tum.sun2car.tumsensorrecorderlib.SensorService;

public class MainPage extends Activity {

  ImageButton btn_footprint;
  ImageButton btn_balance;
  ImageButton btn_prediction;
  ImageButton btn_feedback;
  ImageButton btn_logbook;
  ImageButton btn_info;
  ImageButton btn_newDrive;

  ImageView logo_TUM;

  int purpose = 0;
  int planning = 0;
  int electric = 0;
  public Context con;

  Bundle b;
  Long start_rec;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_mainpage);

    File apk = new File(this.getResources().getString(R.string.downloadPath) + "/TUMsun2car.apk");
    if (apk.exists())
      apk.delete();

    con = this;

    btn_footprint = (ImageButton) findViewById(R.id.btn_footprint);
    btn_balance = (ImageButton) findViewById(R.id.btn_balance);
    btn_prediction = (ImageButton) findViewById(R.id.btn_prediction);
    btn_feedback = (ImageButton) findViewById(R.id.btn_feedback);
    btn_logbook = (ImageButton) findViewById(R.id.btn_logbook);
    btn_info = (ImageButton) findViewById(R.id.btn_info);

    btn_newDrive = (ImageButton) findViewById(R.id.btn_newDrive);

    logo_TUM = (ImageView) findViewById(R.id.imageViewHeader1);

    File directCSV = new File(Environment.getExternalStorageDirectory() + "/sun2car/NewCSV");

    if (!directCSV.exists()) {
      if (directCSV.mkdir()) {
        //directory is created;
      }

    }

    if (false) {
      this.deleteDatabase("sun2car.db");
      Toast.makeText(this, "DB gelöscht", Toast.LENGTH_LONG).show();
    } else if (false) {
      File direct = new File(Environment.getExternalStorageDirectory() + "/sun2car");

      if (!direct.exists()) {
        if (direct.mkdir()) {
          //directory is created;
        }
      }


      try {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        if (sd.canWrite()) {
          String currentDBPath = "//data//" + "de.tum.sun2car"
                  + "//databases//" + "sun2car.db";
          String backupDBPath = "/sun2car/sun2car.db";
          File currentDB = new File(data, currentDBPath);
          File backupDB = new File(sd, backupDBPath);

          FileChannel src = new FileInputStream(currentDB).getChannel();
          FileChannel dst = new FileOutputStream(backupDB).getChannel();
          dst.transferFrom(src, 0, src.size());
          src.close();
          dst.close();
          Toast.makeText(getBaseContext(), backupDB.toString(),
                  Toast.LENGTH_LONG).show();

        }
      } catch (Exception e) {

        Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                .show();

      }
    }

    // look for new tracks each 24 hours
    startServices();

    if (SensorService.getIsRunning()) {
      b = getIntent().getExtras();
      purpose = b.getInt("drive_purpose");
      planning = b.getInt("drive_planning");
      electric = b.getInt("drive_electric");
      start_rec = b.getLong("start_rec");

      btn_newDrive.setBackground(null);
      Drawable drw_btn = getResources().getDrawable(R.drawable.running_drive);
      btn_newDrive.setBackground(drw_btn);

      logo_TUM.setBackground(null);
      Drawable drw_logo = getResources().getDrawable(R.drawable.applogo_active);
      logo_TUM.setBackground(drw_logo);
    } else {
      btn_newDrive.setBackground(null);
      Drawable drw_btn = getResources().getDrawable(R.drawable.newdrive);
      btn_newDrive.setBackground(drw_btn);

      logo_TUM.setBackground(null);
      Drawable drw_logo = getResources().getDrawable(R.drawable.applogo);
      logo_TUM.setBackground(drw_logo);
    }
  }

  @Override
  public void onResume() {
    try {
      super.onResume();
    } catch (Exception ex) {
      Log.i("cought unexpected exception", ex.toString());
    }
  }

  // onClick-Events for Overview Buttons
  public void onFootprint(View v) {
    Intent intent = new Intent(this, Footprint.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void onBalance(View v) {
    Intent intent = new Intent(this, Balance.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void onPrediction(View v) {
    Intent intent = new Intent(this, Prediction.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void onFeedback(View v) {
    Intent intent = new Intent(this, Feedback.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void onLogbook(View v) {
    Intent intent = new Intent(this, Logbook.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  public void onInfo(View v) {
    Intent intent = new Intent(this, Info.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
  }

  // Click-Event for new Drive
  public void onNewDrive(View v) {
    if (!SensorService.getIsRunning()) {
      // Questions before newDrive
      new AlertDialog.Builder(this)
              .setTitle(R.string.newDrive_purpose)
              .setItems(R.array.purpose_of_journey, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                  purpose = i;

                  new AlertDialog.Builder(con)
                          .setTitle(R.string.newDrive_planning)
                          .setItems(R.array.planning_of_journey, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                              planning = i;

                              new AlertDialog.Builder(con)
                                      .setTitle(R.string.newDrive_electric)
                                      .setItems(R.array.electric_journey, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialoginterface, int i) {
                                          electric = i;

                                          Intent intent = new Intent(con, NewDrive.class);
                                          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                          b = new Bundle();
                                          b.putInt("drive_purpose", purpose);
                                          b.putInt("drive_planning", planning);
                                          b.putInt("drive_electric", electric);
                                          //b.putLong("start_rec", start_rec);
                                          intent.putExtras(b);
                                          startActivity(intent);
                                        }
                                      })
                                      .show();
                            }
                          })
                          .show();
                }
              })
              .show();
    } else {
      Intent intent = new Intent(con, NewDrive.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      b = new Bundle();
      b.putInt("drive_purpose", purpose);
      b.putInt("drive_planning", planning);
      b.putInt("drive_electric", electric);
      b.putLong("start_rec", start_rec);
      intent.putExtras(b);
      startActivity(intent);
    }

  }


  public void startServices() {
    if (!SyncSun2CarService.mIsStarted) {
      Intent iDBSyncService = new Intent(this, SyncSun2CarService.class);
      PendingIntent piDBSyncService = PendingIntent.getService(this, 0, iDBSyncService, PendingIntent.FLAG_UPDATE_CURRENT);
      AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
      alarmManager.cancel(piDBSyncService);
      alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_DAY, piDBSyncService);

      // Bind to SyncService
      Intent intent = new Intent(this, SyncSun2CarService.class);
      bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }
  }


  @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	private SyncSun2CarService mBoundService;
    boolean mBound = false;

	/** Defines callbacks for service binding, passed to bindService() */
  private ServiceConnection mConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName className, IBinder service) {
      // We've bound to LocalService, cast the IBinder and get LocalService instance
      SyncBinder binder = (SyncBinder) service;
      mBoundService = binder.getService();
      mBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
      mBound = false;
      doUnbindService();
      Toast.makeText(con, "Synchronisierung gestoppt, da keine Internetverbindung", Toast.LENGTH_LONG).show();
    }
  };

  void doBindService() {
    // Establish a connection with the service.  We use an explicit
    // class name because we want a specific service implementation that
    // we know will be running in our own process (and thus won't be
    // supporting component replacement by other applications).
    bindService(new Intent(con,
            SyncSun2CarService.class), mConnection, Context.BIND_AUTO_CREATE);
    mBound = true;
  }

  void doUnbindService() {
    if (mBound) {
      // Detach our existing connection.
      unbindService(mConnection);
      mBound = false;
    }
  }

	@Override
  protected void onDestroy() {
    super.onDestroy();
    doUnbindService();
  }

}